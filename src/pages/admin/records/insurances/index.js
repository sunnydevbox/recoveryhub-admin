import React from 'react'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import BrowseComponent from './components/browse'
import DrawerComponent from './components/drawer'
import { STORE_PREFIX, LABEL_SINGULAR, LABEL_PLURAL } from './config'

// const { TabPane } = Tabs

const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }),
    setRecord: id => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }),
    setContentViewMode: viewMode =>
      dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
  }
}
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const FeatureComponent = ({ toggleDrawer, setRecord, setContentViewMode }) => {
  return (
    <div>
      <Helmet title={LABEL_PLURAL} />

      <DrawerComponent />

      <section className="card">
        <div className="card-header">
          <div className="utils__title">
            <strong>{LABEL_PLURAL}</strong>

            <Button
              type="primary"
              icon={<PlusOutlined />}
              style={{ float: 'right' }}
              onClick={() => {
                setContentViewMode('new')
                setRecord(null)
                toggleDrawer()
              }}
            >
              New {LABEL_SINGULAR}
            </Button>
          </div>
        </div>
        <div className="card-body">
          <BrowseComponent />
        </div>
      </section>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FeatureComponent)
