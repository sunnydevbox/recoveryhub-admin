/* V1.0 */
import Service from 'services/admin/insurances'

export const LABEL_SINGULAR = 'Insurance'
export const LABEL_PLURAL = 'Insurances'
export const DRAWER_WIDTH = '30%'
export const STORE_PREFIX = 'featureInsurance'
export const API_SERVICE = Service
