/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import { Input, Form, Spin, Radio } from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
// import MedicineGenericService from 'services/admin/medicine-generic'
import FormActionButtonsComponent from './action-buttons'
import FormModalExitFormComponent from './modal-exit-form'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const FormItem = Form.Item
const { TextArea } = Input
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    getList: data => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord, drawerState, formTriggerSave },
  commitRecord,
  setFormTouched,
  // toggleDrawer,
}) => {
  const [form] = Form.useForm()
  // const { Option } = Select
  console.log(selectedRecord)
  useEffect(() => {
    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      // populate form
      // console.log(selectedRecord)
      form.setFieldsValue({
        name: selectedRecord.name,
        description: selectedRecord.description,
        status: selectedRecord.status,
      })
    } else {
      form.setFieldsValue({
        name: form.getFieldValue('name'),
        description: form.getFieldValue('description'),
        status: form.getFieldValue('status'),
      })
    }

    // if (formTriggerSave) {
    //   console.log(form.getFieldValue('name'))
    //   form
    //       .validateFields()
    //       .then(values => {
    //         // form.resetFields();
    //         commitRecord(values)
    //       })
    //       .catch(info => {
    //         console.log('Validate Failed:', info);
    //       });
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form, drawerState, selectedRecord, loadingSelectedRecord, formTriggerSave, commitRecord])

  const handleSave = async values => {
    commitRecord(values)
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        // {...formItemLayout}
        form={form}
        // layout="vertical"
        onFinish={handleSave}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 16 }}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="container">
          <FormItem label="Insurance Name" name="name" rules={[{ required: true }]}>
            <Input />
          </FormItem>

          <FormItem label="Description" name="description" rules={[{ required: true }]}>
            <TextArea rows={3} placeholder="Description" />
          </FormItem>

          <FormItem label="Status" name="status" rules={[{ required: true }]}>
            <Radio.Group>
              <Radio value="a">Active</Radio>
              <Radio value="i">Inactive</Radio>
            </Radio.Group>
          </FormItem>
        </div>
        <div className="container footera ant-drawer-footer text-center px-0 py-2">
          <div className="row">
            <div className="col-sm-12">
              <FormActionButtonsComponent />
            </div>
          </div>
        </div>
      </Form>

      <FormModalExitFormComponent />
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
