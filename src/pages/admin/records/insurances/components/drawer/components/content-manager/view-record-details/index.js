import React, { useEffect } from 'react'
import { Skeleton } from 'antd'
import { Table } from 'reactstrap'
import { connect } from 'react-redux'
// import moment from 'moment'

import { STORE_PREFIX } from '../../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    formTriggerSave: () => dispatch({ type: `${STORE_PREFIX}/FORM_TRIGGER_SAVE`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
  }
}

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  // const [cancelConfirmVisible, setCancelConfirmVisible] = useState(false)

  useEffect(() => {
    console.log(selectedRecord)
  }, [selectedRecord])

  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <Table striped>
              <tbody>
                <tr>
                  <td>Insurance Name:</td>
                  <td>{selectedRecord.name}</td>
                </tr>
                <tr>
                  <td>Description:</td>
                  <td>{selectedRecord.description}</td>
                </tr>
                <tr>
                  <td>Status:</td>
                  <td>{selectedRecord.status === 'a' ? 'ACTIVE' : 'INACTIVE'}</td>
                </tr>
              </tbody>
            </Table>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewRecordDetailsComponent)
