import React, { useEffect } from 'react'
import { Drawer, Skeleton, Button } from 'antd'
import { connect } from 'react-redux'
import parser from 'html-react-parser'
import { doctorName } from 'services/helpers'
import DrawerContentManagerComponent from './components/content-manager'
import { LABEL_SINGULAR, DRAWER_WIDTH, STORE_PREFIX } from '../../config'
import './styles.scss'

const mapStateToProps = ({ featureDoctor }) => ({ featureDoctor })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    setContentViewMode: viewMode =>
      dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
  }
}

const DrawerComponent = ({
  featureDoctor: { drawerState, selectedRecord, loadingSelectedRecord, viewMode },
  formCancel,
  setContentViewMode,
}) => {
  useEffect(() => {}, [drawerState, selectedRecord])

  const getDrawerTitle = () => {
    let title = ''

    if (viewMode === 'view') {
      title =
        !loadingSelectedRecord && selectedRecord ? (
          parser(
            `${LABEL_SINGULAR}: <strong>${doctorName(selectedRecord.general_information)}</strong>`,
          )
        ) : (
          <Skeleton.Input />
        )
    } else if (viewMode === 'new') {
      title = `Add New ${LABEL_SINGULAR} Record`
    } else {
      title =
        !loadingSelectedRecord && selectedRecord ? (
          parser(
            `Update ${LABEL_SINGULAR}: <strong>${doctorName(
              selectedRecord.general_information,
            )}</strong>`,
          )
        ) : (
          <Skeleton.Input />
        )

      if (!selectedRecord) {
        title = `Add New ${LABEL_SINGULAR} Info`
      }
    }
    return title
  }

  const drawerFooter = () => {
    if (viewMode === 'view' && selectedRecord && selectedRecord.id) {
      return (
        <div className="text-center">
          <Button
            className="cta-btn-edit"
            onClick={() => {
              setContentViewMode('edit')
            }}
          >
            Edit this record
          </Button>
        </div>
      )
    }

    return null
  }

  return (
    <>
      <Drawer
        title={getDrawerTitle()}
        placement="right"
        width={DRAWER_WIDTH}
        closable={false}
        onClose={() => {
          formCancel()
        }}
        visible={drawerState}
        footer={drawerFooter()}
      >
        <DrawerContentManagerComponent />
      </Drawer>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent)
