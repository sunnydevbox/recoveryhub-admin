/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react'
import {
  Input,
  Form,
  Spin,
  Switch,
  // Tabs,
  Select,
  // DatePicker,
  // Button,
} from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import MedicineGenericService from 'services/admin/medicine-generic'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import FormActionButtonsComponent from './action-buttons'
import FormModalExitFormComponent from './modal-exit-form'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const FormItem = Form.Item
const { TextArea } = Input
const { Option } = Select
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    getList: data => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord, drawerState, formTriggerSave },
  commitRecord,
  setFormTouched,
  // toggleDrawer,
}) => {
  const [form] = Form.useForm()
  const [queryParams, setQueryParams] = useState({ limit: 0 })
  const [generics, setGenerics] = useState([])
  // const { Option } = Select
  console.log(selectedRecord)
  useEffect(() => {
    getGenerics()
    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      // populate form
      // console.log(selectedRecord)
      form.setFieldsValue({
        name: selectedRecord.name,
        dosage: selectedRecord.dosage,
        unit: selectedRecord.unit,
        generic_name_id: selectedRecord.generic_name_id,
        is_active: selectedRecord.is_active,
        is_injectable: selectedRecord.is_injectable,
        is_s2: selectedRecord.age,
        remarks: selectedRecord.remarks,
      })
    } else {
      form.setFieldsValue({
        name: form.getFieldValue('name'),
        dosage: form.getFieldValue('dosage'),
        unit: form.getFieldValue('unit'),
        generic_name_id: form.getFieldValue('generic_name_id'),
        is_active: form.getFieldValue('is_active'),
        is_injectable: form.getFieldValue('is_injectable'),
        age: form.getFieldValue('age'),
        remarks: form.getFieldValue('remarks'),
      })
    }

    // if (formTriggerSave) {
    //   console.log(form.getFieldValue('name'))
    //   form
    //       .validateFields()
    //       .then(values => {
    //         // form.resetFields();
    //         commitRecord(values)
    //       })
    //       .catch(info => {
    //         console.log('Validate Failed:', info);
    //       });
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form, drawerState, selectedRecord, loadingSelectedRecord, formTriggerSave, commitRecord])

  const handleSave = async values => {
    commitRecord(values)
  }

  const getGenerics = async () => {
    await MedicineGenericService.browse(queryParams).then(result => {
      console.log(result)
      const generic = result.data.map(d => ({
        id: d.id,
        name: d.name,
      }))
      setQueryParams(queryParams)
      console.log(generic)
      setGenerics(generic)
    })
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        // {...formItemLayout}
        form={form}
        // layout="vertical"
        onFinish={handleSave}
        labelCol={{ span: 5 }}
        wrapperCol={{ span: 18 }}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="container">
          <FormItem label="Medicine Name" name="name" rules={[{ required: true }]}>
            <Input />
          </FormItem>

          <FormItem label="Generic Name" name="generic_name_id" rules={[{ required: true }]}>
            <Select
              showSearch
              allowClear
              placeholder="Select Generic Name"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {generics.map(({ id, name }) => (
                <Option key={id} value={id}>
                  {name}
                </Option>
              ))}
            </Select>
          </FormItem>

          <FormItem label="Dosage" name="dosage" rules={[{ required: true }]}>
            <Input />
          </FormItem>

          <FormItem label="Unit" name="unit" rules={[{ required: true }]}>
            <Input />
          </FormItem>

          <FormItem name="is_s2" label="Is S2?" valuePropName="checked">
            <Switch checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} />
          </FormItem>

          <FormItem name="is_injectable" label="Is Injectable?" valuePropName="checked">
            <Switch checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} />
          </FormItem>

          <FormItem name="is_active" label="Set to Active?" valuePropName="checked">
            <Switch checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} />
          </FormItem>

          <FormItem label="Remarks" name="remarks">
            <TextArea rows={3} placeholder="Remarks" />
          </FormItem>
        </div>
        <div className="container footera ant-drawer-footer text-center px-0 py-2">
          <div className="row">
            <div className="col-sm-12">
              <FormActionButtonsComponent />
            </div>
          </div>
        </div>
      </Form>

      <FormModalExitFormComponent />
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
