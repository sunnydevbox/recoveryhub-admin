import React, { useCallback, useEffect, useState } from 'react'
import { Space, Table } from 'antd'
import { connect } from 'react-redux'
import { CheckCircleOutlined, ExclamationCircleOutlined } from '@ant-design/icons'
// import RefreshButtonComponent from 'components/btn-refresh'
import SearchbarComponent from './components/searchbar'
import FiltersComponent from './components/filters'
import './styles.scss'
import { STORE_PREFIX } from '../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }),
    getList: () => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST` }),
    setFilter: filters => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, filters }),
    setRecord: id => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }),
  }
}

const FeatureBrowseComponent = ({
  [STORE_PREFIX]: { dataList, listLoading, pagination },
  toggleDrawer,
  setFilter,
  setRecord,
  // getList,
}) => {
  const [firstLoad, setFirstLoad] = useState(true)

  const columns = [
    {
      title: 'Medicine Name',
      dataIndex: 'name',
      key: 'name',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.age - b.age,
    },
    {
      title: 'Generic Name',
      dataIndex: 'generic_name',
      key: 'generic_name',
    },
    {
      title: 'Dosage',
      dataIndex: 'dosage',
      key: 'dosage',
    },
    {
      title: 'Unit',
      dataIndex: 'unit',
      key: 'unit',
      render: unit => <>{unit.toUpperCase()}</>,
    },
    {
      title: 'S2?',
      dataIndex: 'is_s2',
      key: 'is_s2',
      render: isS2 => (
        <>
          {isS2 ? (
            <CheckCircleOutlined style={{ color: 'rgb(55 160 3)' }} />
          ) : (
            <ExclamationCircleOutlined style={{ color: 'rgb(212 3 3)' }} />
          )}
        </>
      ),
    },
    {
      title: 'Injectable?',
      dataIndex: 'is_injectable',
      key: 'is_injectable',
      render: isInjectable => (
        <>
          {isInjectable ? (
            <CheckCircleOutlined style={{ color: 'rgb(55 160 3)' }} />
          ) : (
            <ExclamationCircleOutlined style={{ color: 'rgb(212 3 3)' }} />
          )}
        </>
      ),
    },
    {
      title: 'Active?',
      dataIndex: 'is_active',
      key: 'is_active',
      render: isActive => (
        <>
          {isActive ? (
            <CheckCircleOutlined style={{ color: 'rgb(55 160 3)' }} />
          ) : (
            <ExclamationCircleOutlined style={{ color: 'rgb(212 3 3)' }} />
          )}
        </>
      ),
    },
    {
      title: 'Remarks',
      dataIndex: 'remarks',
      key: 'remarks',
    },
  ]

  const trig = useCallback(() => {
    setFilter()
  }, [setFilter])

  useEffect(() => {
    if (firstLoad) {
      setFirstLoad(false)
      trig()
    }
  }, [firstLoad, trig])

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        {/* <RefreshButtonComponent callback={getList} loading={listLoading} /> */}
        <Space>
          <SearchbarComponent />
          <FiltersComponent />
        </Space>
        <Table
          loading={listLoading}
          dataSource={dataList}
          columns={columns}
          rowKey={row => row.id}
          onChange={trig}
          pagination={{
            showTotal: total => `Total ${total} items`,
            total: pagination.total,
            defaultPageSize: pagination.per_page,
            onChange: (page, pageSize) => {
              console.log('onChange', page, pageSize)
              setFilter({
                page,
                limit: pageSize,
              })
            },
            position: ['bottomCenter'],
          }}
          onRow={record => {
            return {
              onClick: () => {
                toggleDrawer()
                setRecord(record.id)
              },
            }
          }}
        />
      </Space>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FeatureBrowseComponent)
