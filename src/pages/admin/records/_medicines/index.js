import React from 'react'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Button, Tabs } from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import BrowseComponent from './components/browse'
import BrowseGenericComponent from '../medicine_generic/components/browse'
import DrawerComponent from './components/drawer'
import DrawerGenericComponent from '../medicine_generic/components/drawer'
import { STORE_PREFIX, LABEL_SINGULAR, LABEL_PLURAL } from './config'
import { GEN_PREFIX } from '../medicine_generic/config'

const { TabPane } = Tabs

const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }),
    setRecord: id => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }),
    setContentViewMode: viewMode =>
      dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),

    toggleGenDrawer: () => dispatch({ type: `${GEN_PREFIX}/TOGGLE_DRAWER` }),
    setRecordGen: id => dispatch({ type: `${GEN_PREFIX}/SELECT_RECORD`, id }),
    setContentGenViewMode: viewMode =>
      dispatch({ type: `${GEN_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
  }
}
const mapStateToProps = state => ({
  [STORE_PREFIX]: state[STORE_PREFIX],
  [GEN_PREFIX]: state[GEN_PREFIX],
})
const FeatureComponent = ({
  toggleDrawer,
  setRecord,
  setContentViewMode,
  toggleGenDrawer,
  setRecordGen,
  setContentGenViewMode,
}) => {
  return (
    <div>
      <Helmet title={LABEL_PLURAL} />

      <section className="card">
        <div className="card-header">
          <div className="utils__title">
            <Tabs defaultActiveKey="1" type="card" size="default">
              <TabPane tab="Medicine Inventory" key="1">
                <Button
                  type="primary"
                  icon={<PlusOutlined />}
                  style={{ float: 'right' }}
                  onClick={() => {
                    setContentViewMode('new')
                    setRecord(null)
                    toggleDrawer()
                  }}
                >
                  New {LABEL_SINGULAR}
                </Button>
                <div className="card-body">
                  <BrowseComponent />
                  <DrawerComponent />
                </div>
              </TabPane>

              <TabPane tab="Medicine Generic" key="2">
                <Button
                  type="primary"
                  icon={<PlusOutlined />}
                  style={{ float: 'right' }}
                  onClick={() => {
                    setContentGenViewMode('new')
                    setRecordGen(null)
                    toggleGenDrawer()
                  }}
                >
                  New Medicine Generic
                </Button>
                <div className="card-body">
                  <BrowseGenericComponent />
                  <DrawerGenericComponent />
                </div>
              </TabPane>
            </Tabs>
          </div>
        </div>
      </section>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FeatureComponent)
