/* V1.0 */
import Service from 'services/admin/medicines'

export const LABEL_SINGULAR = 'Medicine'
export const LABEL_PLURAL = 'Medicines'
export const DRAWER_WIDTH = '30%'
export const STORE_PREFIX = 'featureMedicine'
export const API_SERVICE = Service
