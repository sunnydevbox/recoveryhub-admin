import React, { useEffect } from 'react'
import { Skeleton, Image, Tabs } from 'antd'
import { connect } from 'react-redux'
import moment from 'moment'
import { patientName, patientAddress, patientContact, patientPermAddress } from 'services/helpers'
import { STORE_PREFIX } from '../../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    formTriggerSave: () => dispatch({ type: `${STORE_PREFIX}/FORM_TRIGGER_SAVE`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
  }
}

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  // const [cancelConfirmVisible, setCancelConfirmVisible] = useState(false)

  useEffect(() => {
    console.log(selectedRecord)
  }, [selectedRecord])

  const renderField = (label, value) => {
    return (
      <div className="field mt-4">
        {value && value.length ? (
          <div className="pl-1">{value}</div>
        ) : (
          <em className="pl-1 text-muted">Not specified</em>
        )}
        <hr className="m-0" />
        <span className="pl-1 text-muted">{label}</span>
      </div>
    )
  }

  const getAvatar = () => {
    const { avatar } = selectedRecord

    let src = 'error'

    if (avatar && avatar['300']) {
      src = avatar['300']
    }

    return (
      <Image
        style={{ width: 'auto', height: 'auto', textAlign: 'center' }}
        width="100%"
        src={src}
        fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
      />
    )
  }
  const { TabPane } = Tabs

  function callback(key) {
    console.log(key)
  }

  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <div className="row mb-4">
              <div className="col-sm-3">{getAvatar()}</div>
              <div className="col-sm-9">
                <h2>{patientName(selectedRecord.general_information)}</h2>
              </div>
            </div>
            <Tabs defaultActiveKey="1" onChange={callback} type="card">
              <TabPane tab="General Information" key="1">
                <div className="row mb-4">
                  <div className="col-sm-4">
                    {renderField(
                      'Date of Birth',
                      moment(selectedRecord.general_information.date_of_birth).format(
                        'MMMM Do YYYY',
                      ),
                    )}
                  </div>
                  <div className="col-sm-4">
                    {renderField(
                      'Place of Birth',
                      selectedRecord.general_information.place_of_birth,
                    )}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Age', selectedRecord.general_information.age)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Marital Status', selectedRecord.general_information.civil_status)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Gender', selectedRecord.general_information.sex)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Nationality', selectedRecord.general_information.nationality)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Weight', selectedRecord.general_information.weight)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Height', selectedRecord.general_information.height)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Religion', selectedRecord.general_information.religion)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Race', selectedRecord.general_information.race)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Ethnicity', selectedRecord.general_information.ethnicity)}
                  </div>
                  <div className="col-sm-4">
                    {renderField(
                      'Highest Educational Attainment',
                      selectedRecord.general_information.highest_educational_attainment,
                    )}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Occupation', selectedRecord.general_information.occupation)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Company', selectedRecord.general_information.company)}
                  </div>
                  <div className="col-sm-4">
                    {renderField(
                      'Company Address',
                      selectedRecord.general_information.company_address_and_contact_details,
                    )}
                  </div>
                </div>
                <div className="row mb-4">
                  <div className="col-sm-12">
                    <strong>Contact Information </strong>
                    <hr className="mb-1 mt-2" />
                  </div>
                  <div className="col-sm-4">
                    {renderField('Phone', selectedRecord.general_information.mobile_number)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Email Address', selectedRecord.email)}
                  </div>
                  <div className="col-sm-4">
                    {renderField('Landline', selectedRecord.general_information.landline_number)}
                  </div>
                </div>
              </TabPane>
              <TabPane tab="Address" key="2">
                <div className="row mb-4">
                  <div className="col-sm-12">
                    <strong>Current Address</strong>
                    <hr className="mb-1 mt-2" />
                  </div>
                  <div className="col-sm-7">
                    {renderField(
                      'Current Address',
                      patientAddress(selectedRecord.general_information),
                    )}
                  </div>
                </div>
                <div className="row mb-4">
                  <div className="col-sm-12">
                    <strong>Permanent Address</strong>
                    <hr className="mb-1 mt-2" />
                  </div>
                  <div className="col-sm-12">
                    {renderField(
                      'Permanent Address',
                      patientPermAddress(selectedRecord.general_information),
                    )}
                  </div>
                </div>
              </TabPane>
              <TabPane tab="Contact Person" key="3">
                <div className="row mb-4">
                  <div className="col-sm-12">
                    <strong>Emergency Contact Information </strong>
                    <hr className="mb-1 mt-2" />
                  </div>
                  <div className="col-sm-4">
                    {renderField(
                      'Contact Address',
                      selectedRecord.general_information.contact_address,
                    )}
                  </div>
                  <div className="col-sm-4">
                    {renderField(
                      'Contact Name',
                      patientContact(selectedRecord.general_information),
                    )}
                  </div>
                  <div className="col-sm-4">
                    {renderField(
                      'Contact Mobile No',
                      selectedRecord.general_information.contact_mobile_no,
                    )}
                  </div>
                </div>
              </TabPane>
            </Tabs>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewRecordDetailsComponent)
