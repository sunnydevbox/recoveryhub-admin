/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import {
  Input,
  Form,
  Spin,
  Tabs,
  Select,
  DatePicker,
  // Button,
} from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import moment from 'moment'
import FormActionButtonsComponent from './action-buttons'
import FormModalExitFormComponent from './modal-exit-form'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const FormItem = Form.Item
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    getList: data => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord, drawerState, formTriggerSave },
  commitRecord,
  setFormTouched,
  // toggleDrawer,
}) => {
  const [form] = Form.useForm()
  const { Option } = Select
  console.log(selectedRecord)
  useEffect(() => {
    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      // populate form
      // console.log(selectedRecord)
      form.setFieldsValue({
        first_name: selectedRecord.general_information.first_name,
        middle_name: selectedRecord.general_information.middle_name,
        last_name: selectedRecord.general_information.last_name,
        sex: selectedRecord.general_information.sex,
        civil_status: selectedRecord.general_information.civil_status,
        age: selectedRecord.general_information.age,
        date_of_birth: moment(selectedRecord.general_information.date_of_birth),
        place_of_birth: selectedRecord.general_information.place_of_birth,
        religion: selectedRecord.general_information.religion,
        nationality: selectedRecord.general_information.nationality,
        race: selectedRecord.general_information.race,
        ethnicity: selectedRecord.general_information.ethnicity,
        address_1: selectedRecord.general_information.address_1,
        barangay: selectedRecord.general_information.barangay,
        city: selectedRecord.general_information.city,
        state: selectedRecord.general_information.state,
        zipcode: selectedRecord.general_information.zipcode,
        perm_no_street: selectedRecord.general_information.perm_no_street,
        perm_barangay: selectedRecord.general_information.perm_barangay,
        perm_city_municipality: selectedRecord.general_information.perm_city_municipality,
        perm_province: selectedRecord.general_information.perm_province,
        perm_zip_code: selectedRecord.general_information.perm_zip_code,
        landline_number: selectedRecord.general_information.landline_number,
        mobile_number: selectedRecord.general_information.mobile_number,
        email: selectedRecord.email,
        contact_first_name: selectedRecord.general_information.contact_first_name,
        contact_land_line: selectedRecord.general_information.contact_land_line,
        contact_last_lame: selectedRecord.general_information.contact_last_lame,
        contact_middle_name: selectedRecord.general_information.contact_middle_name,
        contact_address: selectedRecord.general_information.contact_address,
        contact_mobile_no: selectedRecord.general_information.contact_mobile_no,
        highest_educational_attainment:
          selectedRecord.general_information.highest_educational_attainment,
        occupation: selectedRecord.general_information.occupation,
        company: selectedRecord.general_information.company,
        company_address_and_contact_details:
          selectedRecord.general_information.company_address_and_contact_details,
        height: selectedRecord.general_information.height,
        weight: selectedRecord.general_information.weight,
      })
    } else {
      form.setFieldsValue({
        first_name: form.getFieldValue('first_name'),
        middle_name: form.getFieldValue('middle_name'),
        last_name: form.getFieldValue('last_name'),
        sex: form.getFieldValue('sex'),
        civil_status: form.getFieldValue('civil_status'),
        age: form.getFieldValue('age'),
        date_of_birth: form.getFieldValue('date_of_birth'),
        place_of_birth: form.getFieldValue('place_of_birth'),
        religion: form.getFieldValue('religion'),
        nationality: form.getFieldValue('nationality'),
        race: form.getFieldValue('race'),
        ethnicity: form.getFieldValue('ethnicity'),
        address_1: form.getFieldValue('address_1'),
        barangay: form.getFieldValue('barangay'),
        city: form.getFieldValue('city'),
        state: form.getFieldValue('state'),
        zipcode: form.getFieldValue('zipcode'),
        perm_no_street: form.getFieldValue('perm_no_street'),
        perm_barangay: form.getFieldValue('perm_barangay'),
        perm_city_municipality: form.getFieldValue('perm_city_municipality'),
        perm_province: form.getFieldValue('perm_province'),
        perm_zip_code: form.getFieldValue('perm_zip_code'),
        landline_number: form.getFieldValue('landline_number'),
        mobile_number: form.getFieldValue('mobile_number'),
        email: form.getFieldValue('email'),
        contact_first_name: form.getFieldValue('contact_first_name'),
        contact_land_line: form.getFieldValue('contact_land_line'),
        contact_last_lame: form.getFieldValue('contact_last_lame'),
        contact_middle_name: form.getFieldValue('contact_middle_name'),
        contact_mobile_no: form.getFieldValue('contact_mobile_no'),
        contact_address: form.getFieldValue('contact_address'),
        highest_educational_attainment: form.getFieldValue('highest_educational_attainment'),
        occupation: form.getFieldValue('occupation'),
        company: form.getFieldValue('company'),
        company_address_and_contact_details: form.getFieldValue(
          'company_address_and_contact_details',
        ),
        height: form.getFieldValue('height'),
        weight: form.getFieldValue('weight'),
      })
    }

    // if (formTriggerSave) {
    //   console.log(form.getFieldValue('name'))
    //   form
    //       .validateFields()
    //       .then(values => {
    //         // form.resetFields();
    //         commitRecord(values)
    //       })
    //       .catch(info => {
    //         console.log('Validate Failed:', info);
    //       });
    // }
  }, [form, drawerState, selectedRecord, loadingSelectedRecord, formTriggerSave, commitRecord])

  const handleSave = async values => {
    commitRecord(values)
  }

  const { TabPane } = Tabs

  function callback(key) {
    console.log(key)
  }
  // const formItemLayout = {
  //   labelCol: {
  //     xs: { span: 24 },
  //     sm: { span: 5 },
  //   },
  //   wrapperCol: {
  //     xs: { span: 1 },
  //     sm: { span: 8 },
  //   },
  // };

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        // {...formItemLayout}
        form={form}
        layout="vertical"
        onFinish={handleSave}
        // labelCol={{ span: 3 }}
        // wrapperCol={{ span: 8 }}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="container" style={{ maxWidth: 1250 }}>
          <br />
          <Tabs defaultActiveKey="1" onChange={callback} type="card">
            <TabPane tab="1.General Information" key="1">
              <strong>Demographic Data:</strong>
              <hr />
              <div className="row">
                <div className="col-md-4">
                  <FormItem label="First Name" name="first_name" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Middle Name" name="middle_name" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Last Name" name="last_name" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
              </div>
              <div className="row">
                <div className="col-md-2">
                  <FormItem label="Age" name="age" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-2">
                  <FormItem label="Sex" name="sex" rules={[{ required: true }]}>
                    <Select allowClear>
                      <Option value="male">Male</Option>
                      <Option value="female">Female</Option>
                    </Select>
                  </FormItem>
                </div>
                <div className="col-md-2">
                  <FormItem label="Civil Status" name="civil_status" rules={[{ required: true }]}>
                    <Select allowClear>
                      <Option value="Single">Single</Option>
                      <Option value="Married">Married</Option>
                      <Option value="Widowed">Widowed</Option>
                    </Select>
                  </FormItem>
                </div>
                <div className="col-md-2">
                  <FormItem label="Date of Birth" name="date_of_birth" rules={[{ required: true }]}>
                    <DatePicker
                      style={{ width: 180 }}
                      placeholder="Date of Birth"
                      format="MM-DD-YYYY"
                    />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem
                    label="Place of Birth"
                    name="place_of_birth"
                    rules={[{ required: true }]}
                  >
                    <Input />
                  </FormItem>
                </div>
              </div>

              <div className="row">
                <div className="col-md-3">
                  <FormItem label="Nationality" name="nationality">
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-3">
                  <FormItem label="Religion" name="religion">
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-3">
                  <FormItem label="Race" name="race">
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-3">
                  <FormItem label="Ethnicity" name="ethnicity">
                    <Input />
                  </FormItem>
                </div>
              </div>
              <div className="row">
                {/* <div className="col-md-4">
                  <FormItem label="Place of Birth" name="place_of_birth" rules={[{ required: true }]}>
                    <Input placeholder="Place of Birth" />
                  </FormItem>
                </div> */}
                <div className="col-md-2">
                  <FormItem label="Height" name="height" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-2">
                  <FormItem label="Weight" name="weight" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem
                    label="Highest Educational Attainment"
                    name="highest_educational_attainment"
                    rules={[{ required: true }]}
                  >
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Occupation" name="occupation" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
              </div>
              <div className="row">
                {/* <div className="col-md-4">
                  <FormItem label="Place of Birth" name="place_of_birth" rules={[{ required: true }]}>
                    <Input placeholder="Place of Birth" />
                  </FormItem>
                </div> */}
                <div className="col-md-4">
                  <FormItem label="Company" name="company">
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-8">
                  <FormItem
                    label="Company Address and Contact Details"
                    name="company_address_and_contact_details"
                  >
                    <Input />
                  </FormItem>
                </div>
              </div>
              <br />
              <strong>Contact Information:</strong>
              <hr />
              <div className="row">
                <div className="col-md-4">
                  <FormItem
                    label="Landline No."
                    name="landline_number"
                    rules={[{ required: true }]}
                  >
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Mobile Number" name="mobile_number" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Email Address" name="email" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
              </div>
            </TabPane>
            <TabPane tab="2.Address" key="2">
              <strong>Current Address</strong>
              <hr />
              <div className="row">
                <div className="col-md-6">
                  <FormItem label="No. & Street" name="address_1" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-6">
                  <FormItem label="Barangay" name="barangay" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="City/Municipality" name="city" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Province" name="state" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Zip Code" name="zipcode" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
              </div>
              <strong>Permanent Address</strong>
              <hr />
              <div className="row">
                <div className="col-md-6">
                  <FormItem label="No. & Street" name="perm_no_street" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-6">
                  <FormItem label="Barangay" name="perm_barangay" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem
                    label="City/Municipality"
                    name="perm_city_municipality"
                    rules={[{ required: true }]}
                  >
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Province" name="perm_province" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Zip Code" name="perm_zip_code" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
              </div>
            </TabPane>
            <TabPane tab="3.Contact Person" key="3">
              <strong>Contact Person In Case of Emergency</strong>
              <hr />
              <div className="row">
                <div className="col-md-4">
                  <FormItem
                    label="First Name"
                    name="contact_first_name"
                    rules={[{ required: true }]}
                  >
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Middle Name" name="contact_middle_name">
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-4">
                  <FormItem label="Last Name" name="contact_last_lame" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-6">
                  <FormItem label="Address" name="contact_address">
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-3">
                  <FormItem label="Landline No." name="contact_land_line">
                    <Input />
                  </FormItem>
                </div>
                <div className="col-md-3">
                  <FormItem
                    label="Mobile No."
                    name="contact_mobile_no"
                    rules={[{ required: true }]}
                  >
                    <Input />
                  </FormItem>
                </div>
              </div>
            </TabPane>
          </Tabs>
        </div>

        <div className="container footera ant-drawer-footer text-center px-0 py-2">
          <div className="row">
            <div className="col-sm-12">
              <FormActionButtonsComponent />
            </div>
          </div>
        </div>
      </Form>

      <FormModalExitFormComponent />
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
