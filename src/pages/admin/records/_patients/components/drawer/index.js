import React, { useEffect } from 'react'
import { Drawer, Skeleton, Button } from 'antd'
import { connect } from 'react-redux'
import parser from 'html-react-parser'
import { patientName } from 'services/helpers'
import { EditOutlined } from '@ant-design/icons'
import DrawerContentManagerComponent from './components/content-manager'
import { LABEL_SINGULAR, DRAWER_WIDTH, STORE_PREFIX } from '../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    setContentViewMode: viewMode =>
      dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
  }
}

const DrawerComponent = ({
  [STORE_PREFIX]: { drawerState, selectedRecord, loadingSelectedRecord, viewMode },
  formCancel,
  setContentViewMode,
}) => {
  useEffect(() => {}, [drawerState, selectedRecord])

  const getDrawerTitle = () => {
    let title = ''

    if (!selectedRecord && viewMode === 'new') {
      title = `Add New ${LABEL_SINGULAR} Record`
    } else if (selectedRecord) {
      let label = `${LABEL_SINGULAR}: <strong>${patientName(
        selectedRecord.general_information,
      )}</strong>`

      if (viewMode === 'edit') {
        label = `Update ${LABEL_SINGULAR}: <strong>${patientName(
          selectedRecord.general_information,
        )}</strong>`
      }

      title = !loadingSelectedRecord && selectedRecord ? parser(label) : <Skeleton.Input />
    }
    return title
  }

  const drawerFooter = () => {
    if (viewMode === 'view' && selectedRecord && selectedRecord.id) {
      return (
        <div className="text-center">
          <Button
            type="primary"
            icon={<EditOutlined />}
            className="cta-btn-edit mr-2"
            onClick={() => {
              setContentViewMode('edit')
            }}
          >
            Edit this record
          </Button>

          <Button
            type="primary"
            onClick={() => {
              formCancel()
            }}
          >
            Cancel
          </Button>
        </div>
      )
    }

    return null
  }

  return (
    <>
      <Drawer
        title={getDrawerTitle()}
        placement="right"
        width={DRAWER_WIDTH}
        closable={false}
        onClose={() => {
          formCancel()
        }}
        visible={drawerState}
        footer={drawerFooter()}
      >
        <DrawerContentManagerComponent />
      </Drawer>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent)
