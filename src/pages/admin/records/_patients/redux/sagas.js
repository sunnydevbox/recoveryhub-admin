import { all, takeEvery, put, select, call } from 'redux-saga/effects'
import { Notification } from 'services/helpers'
import actions from './actions'
import { STORE_PREFIX, API_SERVICE } from '../config'

export function* SET_FORM_TOUCHED(payload) {
  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload,
  })
}

export function* FORM_CANCEL(data) {
  const { formTouched } = yield select(state => state[STORE_PREFIX])

  if (formTouched) {
    // FORCE TO CLOSE THE MODAL
    if (data && typeof data.force === 'boolean') {
      if (data.force) {
        yield put({
          type: `${STORE_PREFIX}/SET_STATE`,
          payload: {
            formTouched: false,
            formConfirmCancelModal: false,
          },
        })

        yield call(SELECT_RECORD, { id: null })
        yield call(TOGGLE_DRAWER, { force: false })
      }
    } else {
      // do nothing
      // ALert
      yield put({
        type: `${STORE_PREFIX}/SET_STATE`,
        payload: {
          formConfirmCancelModal: true,
        },
      })
    }
  } else {
    // JUST CLOSE IT
    yield call(SELECT_RECORD, { id: null })
    yield call(TOGGLE_DRAWER, { force: false })
    yield call(SET_CONTENT_VIEW_MODE, { viewMode: 'view' })
  }
}

export function* TOGGLE_DRAWER(data) {
  const { drawerState } = yield select(state => state[STORE_PREFIX])
  let s = !drawerState

  if (data && typeof data.force === 'boolean') {
    s = data.force
  }

  if (!s) {
    yield call(SELECT_RECORD, { payload: { id: null } }) // this blocks flow until onSubmit is done
  }

  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      drawerState: s,
    },
  })
}

export function* SELECT_RECORD(data) {
  let d = null

  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      loadingSelectedRecord: false,
    },
  })

  if (data.id) {
    d = {}
    try {
      yield put({
        type: `${STORE_PREFIX}/SET_STATE`,
        payload: {
          loadingSelectedRecord: true,
        },
      })

      d = yield call(API_SERVICE.info, { id: data.id })
      // temporary
      if (typeof d.data === 'object' && d.data.length) {
        ;[d] = d.data
      }

      // d = d.data
    } catch (error) {
      Notification(error)
    }
    // CALL API
  }

  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      selectedRecord: d,
      loadingSelectedRecord: false,
    },
  })
}

/**
 * Save to back-end
 * @param {*} payload
 */
export function* API_COMMIT_RECORD(payload) {
  const { selectedRecord } = yield select(state => state[STORE_PREFIX])

  // console.log('API_COMMIT_RECORD(payload)', payload, selectedRecord)

  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      loadingSelectedRecord: true,
      formTriggerSave: false,
    },
  })

  let response = null
  try {
    if (selectedRecord && selectedRecord.id) {
      // UPDATE
      response = yield call(API_SERVICE.update, selectedRecord.id, payload.data)
    } else {
      // CREATE NEW
      response = yield call(API_SERVICE.store, payload.data)
    }

    yield put({
      type: `${STORE_PREFIX}/SET_STATE`,
      payload: {
        selectedRecord: response,
        loadingSelectedRecord: false,
        formTouched: false,
      },
    })

    Notification({
      status: 200,
      title: 'Category info saved',
    })

    yield put({
      type: `${STORE_PREFIX}/SET_STATE`,
      payload: {
        selectedRecord: response,
        loadingSelectedRecord: false,
      },
    })

    yield call(API_GET_LIST)
  } catch (error) {
    yield put({
      type: `${STORE_PREFIX}/SET_STATE`,
      payload: {
        loadingSelectedRecord: false,
      },
    })

    Notification(error)
  }
}

/**
 * Pull data from backend
 */
export function* API_GET_LIST() {
  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      listLoading: true,
    },
  })

  // yield call(RESET_PAGINATION)
  const { filters } = yield select(state => state[STORE_PREFIX])
  const response = yield call(API_SERVICE.index, filters)

  if (response && response.data) {
    yield put({
      type: `${STORE_PREFIX}/SET_STATE`,
      payload: {
        listLoading: false,
        dataList: response.data,
      },
    })
  }

  if (response && response.meta && response.meta.pagination) {
    yield call(SET_PAGINATION, response.meta.pagination)
  }
}

/**
 * Backend
 */
export function* API_DELETE_RECORD(payload) {
  // const response = yield call(API_SERVICE.index, filters)
  try {
    yield call(API_SERVICE.destroy, payload.id)
    yield call(API_GET_LIST)

    Notification({
      status: 200,
      title: 'Record deleted',
    })
  } catch (error) {
    Notification({
      status: 500,
      title: 'We encountered while processing your request',
    })
  }
}

export function* DATA_IS_LOADING(payload) {
  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      listLoading: !payload.payload.listLoading,
    },
  })
}

export function* RESET_FILTERS() {
  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      filters: {
        status: '',
      },
    },
  })

  // TRIGGER API_GET_LIST
  yield call(API_GET_LIST)
}

export function* SET_FILTER(data) {
  //  let { filters } = yield select(state => state[STORE_PREFIX])
  let { filters } = yield select(state => state[STORE_PREFIX])

  let d = {}
  if (data.payload) {
    d = data.payload
  } else if (data.filters) {
    d = data.filters
  }

  filters = { ...filters, ...d }

  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      filters,
    },
  })

  // TRIGGER API_GET_LIST
  yield call(API_GET_LIST)
}

export function* SET_SEARCH_TERM(data) {
  yield call(SET_FILTER, { payload: { search: data.search, page: 1 } })
}

export function* RESET_PAGINATION() {
  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      pagination: {
        total: 0,
        count: 0,
        current_page: 1,
        per_page: 15,
        total_pages: 0,
      },
    },
  })
}

export function* SET_PAGINATION(data) {
  let { pagination } = yield select(state => state[STORE_PREFIX])

  if (data.payload && data.payload.pagination) {
    data = data.payload.pagination
  }

  // remove this unnecessary property
  // this comes from back-ends transformer
  delete data.links

  pagination = { ...pagination, ...data }

  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      pagination,
    },
  })
}

export function* FORM_TRIGGER_SAVE() {
  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      formTriggerSave: true,
    },
  })
}

export function* FORM_CANCEL_MODAL() {
  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      formConfirmCancelModal: false,
    },
  })
}

/**
 * Switches the content of Drawer to different
 * views
 *
 * @param {viewMode: true/false} data
 */
export function* SET_CONTENT_VIEW_MODE(data) {
  const { viewMode } = data
  yield put({
    type: `${STORE_PREFIX}/SET_STATE`,
    payload: {
      viewMode,
    },
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.SET_FORM_TOUCHED, SET_FORM_TOUCHED),
    takeEvery(actions.TOGGLE_DRAWER, TOGGLE_DRAWER),
    // takeEvery(actions.TRIGGER_RELOAD_BROWSE, TRIGGER_RELOAD_BROWSE),
    takeEvery(actions.DATA_IS_LOADING, DATA_IS_LOADING),
    takeEvery(actions.SELECT_RECORD, SELECT_RECORD),

    takeEvery(actions.RESET_FILTERS, RESET_FILTERS),
    takeEvery(actions.SET_FILTER, SET_FILTER),
    takeEvery(actions.SET_SEARCH_TERM, SET_SEARCH_TERM),

    takeEvery(actions.FORM_TRIGGER_SAVE, FORM_TRIGGER_SAVE),
    takeEvery(actions.FORM_CANCEL, FORM_CANCEL),
    takeEvery(actions.FORM_CANCEL_MODAL, FORM_CANCEL_MODAL),

    takeEvery(actions.SET_CONTENT_VIEW_MODE, SET_CONTENT_VIEW_MODE),

    takeEvery(actions.API_GET_LIST, API_GET_LIST),
    takeEvery(actions.API_DELETE_RECORD, API_DELETE_RECORD),
    takeEvery(actions.API_COMMIT_RECORD, API_COMMIT_RECORD),
  ])
}
