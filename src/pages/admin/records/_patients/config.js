/* V1.0 */
import Service from 'services/admin/patients'

export const LABEL_SINGULAR = 'Patient'
export const LABEL_PLURAL = 'Patients'
export const DRAWER_WIDTH = '80%'
export const STORE_PREFIX = 'featurePatient'
export const API_SERVICE = Service
