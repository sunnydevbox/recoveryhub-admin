import React, { useEffect, useState } from 'react'
import { Button, Modal } from 'antd'
import { connect } from 'react-redux'
import './styles.scss'
import { GEN_PREFIX } from '../../../../../../config'

const mapStateToProps = state => ({ [GEN_PREFIX]: state[GEN_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${GEN_PREFIX}/TOGGLE_DRAWER`, data }),
    formTriggerSave: () => dispatch({ type: `${GEN_PREFIX}/FORM_TRIGGER_SAVE`, id: null }),
    formCancel: force => dispatch({ type: `${GEN_PREFIX}/FORM_CANCEL`, force }),
    deleteRecord: id => dispatch({ type: `${GEN_PREFIX}/API_DELETE_RECORD`, id }),
  }
}

const FormActionButtonsComponent = ({
  [GEN_PREFIX]: { formTouched, selectedRecord },
  formCancel,
  deleteRecord,
}) => {
  const [modalConfirmDeleteState, setModalConfirmDeleteState] = useState(false)

  useEffect(() => {}, [formTouched])

  return (
    <>
      <Button type="primary" className="mr-2" htmlType="submit" disabled={!formTouched}>
        Save
      </Button>

      <Button
        type="primary"
        className="cta-btn-delete mr-2"
        onClick={() => {
          setModalConfirmDeleteState(true)
        }}
      >
        Delete
      </Button>

      <Button
        type="primary"
        onClick={() => {
          formCancel()
        }}
      >
        Cancel
      </Button>

      <Modal
        title="Confirm"
        visible={modalConfirmDeleteState}
        onOk={() => {
          // formCancel(true)

          // - CALL API
          // - close this modal
          // - cancelForm
          // - refresh table
          deleteRecord(selectedRecord.id)
          setModalConfirmDeleteState(false)
          formCancel({ force: true })
        }}
        onCancel={() => {
          setModalConfirmDeleteState(false)
        }}
        okText="Yes"
        cancelText="No"
      >
        <p>Are you sure you want to delete this record?</p>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormActionButtonsComponent)
