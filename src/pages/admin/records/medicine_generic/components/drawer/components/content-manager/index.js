import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import FormComponent from './form'
import ViewRecordDetailsComponent from './view-record-details'
import { GEN_PREFIX } from '../../../../config'

const mapStateToProps = state => ({ [GEN_PREFIX]: state[GEN_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${GEN_PREFIX}/FORM_CANCEL`, force }),
  }
}

const DrawerContentManagerComponent = ({
  [GEN_PREFIX]: { drawerState, selectedRecord, viewMode },
}) => {
  useEffect(() => {}, [drawerState, selectedRecord])

  return (
    <>
      {viewMode === 'view' ? <ViewRecordDetailsComponent /> : ''}
      {viewMode === 'edit' || viewMode === 'new' ? <FormComponent /> : ''}
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContentManagerComponent)
