import React, { useEffect } from 'react'
import { Space, Select } from 'antd'
import { connect } from 'react-redux'
import { GEN_PREFIX } from '../../../../config'

const { Option } = Select

const mapStateToProps = state => ({ [GEN_PREFIX]: state[GEN_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filters => dispatch({ type: `${GEN_PREFIX}/SET_FILTER`, filters }),
    resetFilters: () => dispatch({ type: `${GEN_PREFIX}/RESET_FILTERS` }),
  }
}

const FiltersComponent = ({ setFilter, resetFilters }) => {
  useEffect(() => {}, [])

  const filterChanged = (key, value) => {
    console.log(value)
    if (key) {
      const d = []
      d[key] = value
      setFilter(d)
      console.log(setFilter(d).filters.status)
    } else {
      resetFilters()
    }

    // if (key === null) {
    //   filters.filters = {}
    // } else {
    //   filters.filters[key] = value
    // }

    // console.log('fileCHanged', key, value, filters.filters)
  }
  return (
    <Space>
      <Select
        placeholder="Is S2?"
        onChange={v => filterChanged('is_s2', v)}
        // value={queryParams.filters.is_s2}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <Select
        placeholder="Is Injectable?"
        onChange={v => filterChanged('is_injectable', v)}
        // value={queryParams.filters.is_injectable}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <Select
        placeholder="Is Active?"
        onChange={v => filterChanged('is_active', v)}
        // value={queryParams.filters.is_active}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>
      <a
        role="button"
        tabIndex={0}
        onClick={() => filterChanged(null)}
        onKeyDown={() => filterChanged(null)}
      >
        Clear Filters
      </a>
    </Space>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FiltersComponent)
