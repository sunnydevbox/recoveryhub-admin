import React, { useEffect } from 'react'
import { Input } from 'antd'
import { connect } from 'react-redux'
import { GEN_PREFIX } from '../../../../config'

const { Search } = Input

const mapStateToProps = state => ({ [GEN_PREFIX]: state[GEN_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    setSearchTerm: search => dispatch({ type: `${GEN_PREFIX}/SET_SEARCH_TERM`, search }),
  }
}
const SearchbarComponent = ({
  [GEN_PREFIX]: {
    // filters,
    searchLoading,
  },
  setSearchTerm,
}) => {
  useEffect(() => {}, [])

  return (
    <>
      <Search
        placeholder="Name"
        onSearch={str => setSearchTerm(str)}
        style={{ width: 200 }}
        allowClear
        loading={searchLoading}
      />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchbarComponent)
