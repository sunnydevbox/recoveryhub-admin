/* V1.0 */
import Service from 'services/admin/medicine-generic'

export const LABEL_SINGULAR = 'Medicine Generic'
export const LABEL_PLURAL = 'Medicine Generics'
export const DRAWER_WIDTH = '30%'
export const GEN_PREFIX = 'featureMedGeneric'
export const API_SERVICE = Service
