import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Helmet } from 'react-helmet'
import { Tabs } from 'antd'
import DrawerComponent from './components/drawer'
import BrowseMedicines from './components/browse-medicines'
import BrowseGeneric from './components/browse-generic'

const { TabPane } = Tabs

const mapStateToProps = ({ recordMedicine }) => ({ recordMedicine })

const Medicines = ({ dispatch, recordMedicine: { drawerState } }) => {
  // const [drawerVisibility, setDrawerVisibility] = useState(false)
  const [drawerType, setDrawerType] = useState(null)
  const [drawerRecordId, setDrawerRecordId] = useState(null)

  useEffect(() => {}, [drawerState])

  const setReloadListState = (reload = true) => {
    dispatch({
      type: 'recordMedicine/TRIGGER_RELOAD_BROWSE',
      payload: {
        reloadListState: reload,
      },
    })
  }

  const setGenericReloadState = (reload = true) => {
    dispatch({
      type: 'recordGeneric/TRIGGER_RELOAD_BROWSE',
      payload: {
        reloadListState: reload,
      },
    })
  }

  const toggleDrawer = state => {
    dispatch({
      type: 'recordMedicine/TOGGLE_DRAWER',
      payload: {
        drawerState: typeof state === 'undefined' ? !drawerState : state,
      },
    })
  }

  return (
    <div>
      <Helmet title="Medicines" />

      <div className="kit__utils__heading">
        <h5>
          <span className="mr-3">Medicines</span>
        </h5>
      </div>
      <div className="card">
        <div className="card-body">
          <Tabs defaultActiveKey="1" type="card" size="default">
            <TabPane tab="Medicine Inventory" key="1">
              <BrowseMedicines
                toggleDrawer={toggleDrawer}
                setReloadListState={setReloadListState}
                drawerVisibilityChanged={(type, id, visibility) => {
                  setDrawerType(type)
                  toggleDrawer(visibility)
                  setDrawerRecordId(id)
                }}
              />
            </TabPane>
            <TabPane tab="Medicine Generic" key="2">
              <BrowseGeneric
                toggleDrawer={toggleDrawer}
                setGenericReloadState={setGenericReloadState}
                drawerVisibilityChanged={(type, id, visibility) => {
                  setDrawerType(type)
                  toggleDrawer(visibility)
                  setDrawerRecordId(id)
                }}
              />
            </TabPane>
          </Tabs>
        </div>
      </div>
      <DrawerComponent
        recordId={drawerRecordId}
        visibility={drawerState}
        // visibility={drawerVisibility}
        setReloadListState={setReloadListState}
        setGenericReloadState={setGenericReloadState}
        toggleDrawer={toggleDrawer}
        type={drawerType}
        // drawerVisibilityChanged={visibility => setDrawerVisibility(visibility)}
      />
    </div>
  )
}
export default connect(mapStateToProps)(Medicines)
