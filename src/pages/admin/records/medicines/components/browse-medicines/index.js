import React, { useState, useEffect } from 'react'
import BrowseComponent from 'components/bread/browse'
import { Space, Button, Row, Col } from 'antd'
import { connect } from 'react-redux'
import { CheckCircleOutlined, ExclamationCircleOutlined, SyncOutlined } from '@ant-design/icons'
import FiltersComponent from '../filters'
import SearchbarComponent from '../searchbar'

const mapStateToProps = ({ recordMedicine }) => ({ recordMedicine })

const Browse = ({
  dispatch,
  setReloadListState,
  recordMedicine: { reloadListState, listLoading, dataSource, pagination, queryParams },
  drawerVisibilityChanged,
}) => {
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(() => {
    if (firstLoad || reloadListState) {
      setFirstLoad(false)
      getList()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [firstLoad, reloadListState])

  const getList = () => {
    dispatch({
      type: 'recordMedicine/SET_BROWSE',
    })
  }

  const tableChanged = (page, filters, sorter, extra) => {
    queryParams.page = page
    queryParams.sorter = sorter

    console.log('(page, filters, sorter, extra)', page, filters, sorter, extra)
    // console.log('(page, filters, sorter, extra)', sorter)

    dispatch({
      type: 'recordMedicine/SET_QUERY_PARAMS',
      payload: {
        queryParams,
      },
    })
  }
  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Space>
          <SearchbarComponent />
          <FiltersComponent />
        </Space>
        <Row>
          <Col span={12}>Total Results: {pagination.total}</Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Space>
              <Button
                type="button"
                disabled={reloadListState}
                onClick={() => drawerVisibilityChanged('new_medicine', null, true)}
              >
                New Medicine
              </Button>
              <Button
                type="link"
                icon={
                  <SyncOutlined
                    onClick={() => setReloadListState(true)}
                    spin={reloadListState}
                    style={{
                      width: 'auto',
                      float: 'right',
                    }}
                  />
                }
                disabled={reloadListState}
              />
            </Space>
          </Col>
        </Row>

        <BrowseComponent
          rowKey={record => record.id}
          loading={reloadListState || listLoading}
          pagination={pagination}
          columns={[
            {
              title: 'Medicine Name',
              dataIndex: 'name',
              key: 'name',
              defaultSortOrder: 'descend',
              sorter: (a, b) => a.age - b.age,
            },
            {
              title: 'Generic Name',
              dataIndex: 'generic_name',
              key: 'generic_name',
            },
            {
              title: 'Dosage',
              dataIndex: 'dosage',
              key: 'dosage',
            },
            {
              title: 'Unit',
              dataIndex: 'unit',
              key: 'unit',
              render: unit => <>{unit.toUpperCase()}</>,
            },
            {
              title: 'S2?',
              dataIndex: 'is_s2',
              key: 'is_s2',
              render: isS2 => (
                <>
                  {isS2 ? (
                    <CheckCircleOutlined style={{ color: 'rgb(55 160 3)' }} />
                  ) : (
                    <ExclamationCircleOutlined style={{ color: 'rgb(212 3 3)' }} />
                  )}
                </>
              ),
            },
            {
              title: 'Injectable?',
              dataIndex: 'is_injectable',
              key: 'is_injectable',
              render: isInjectable => (
                <>
                  {isInjectable ? (
                    <CheckCircleOutlined style={{ color: 'rgb(55 160 3)' }} />
                  ) : (
                    <ExclamationCircleOutlined style={{ color: 'rgb(212 3 3)' }} />
                  )}
                </>
              ),
            },
            {
              title: 'Active?',
              dataIndex: 'is_active',
              key: 'is_active',
              render: isActive => (
                <>
                  {isActive ? (
                    <CheckCircleOutlined style={{ color: 'rgb(55 160 3)' }} />
                  ) : (
                    <ExclamationCircleOutlined style={{ color: 'rgb(212 3 3)' }} />
                  )}
                </>
              ),
            },
            {
              title: 'Remarks',
              dataIndex: 'remarks',
              key: 'remarks',
            },
            {
              title: 'Action',
              key: 'action',
              render: (text, record) => (
                <Space size="middle">
                  <Button
                    type="primary"
                    size="small"
                    onClick={() => drawerVisibilityChanged('edit_medicine', record.id, true)}
                    danger
                  >
                    Edit
                  </Button>
                  <Button
                    type="primary"
                    danger
                    size="small"
                    onClick={() => {
                      console.log(record.id)
                    }}
                  >
                    Delete
                  </Button>
                </Space>
              ),
            },
          ]}
          dataSource={dataSource}
          tableChanged={tableChanged}
        />
      </Space>
    </>
  )
}

export default connect(mapStateToProps)(Browse)
