import React, { useEffect } from 'react'
import { Input } from 'antd'
import { connect } from 'react-redux'

const { Search } = Input

const mapStateToProps = ({ recordMedicine }) => ({ recordMedicine })

const SearchbarComponent = ({ dispatch, recordMedicine: { queryParams, searchLoading } }) => {
  useEffect(() => {}, [])

  const onSearch = str => {
    queryParams.searchTerm = str

    if (queryParams.page) {
      queryParams.page.current = 1
    }

    dispatch({
      type: 'recordMedicine/SET_SEARCH_TERM',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <>
      <Search
        placeholder="Name"
        onSearch={onSearch}
        style={{ width: 200 }}
        allowClear
        loading={searchLoading}
      />
    </>
  )
}

export default connect(mapStateToProps)(SearchbarComponent)
