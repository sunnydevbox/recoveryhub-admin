import React, { useState, useEffect } from 'react'
import { Button } from 'antd'
import DrawerComponent from 'components/bread/drawer'
import FormMedicineComponent from '../form-medicines'
import FormGenericComponent from '../form-generics'

function Drawer(props) {
  const {
    drawerVisibilityChanged,
    visibility,
    type,
    setReloadListState,
    setGenericReloadState,
    recordId,
    toggleDrawer,
  } = props

  // const [drawerVisibility, setDrawerVisibility] = useState(false)
  const [triggeredSave, setTriggeredSave] = useState(false)

  useEffect(() => {
    // setDrawerVisibility(visibility)
  }, [props, visibility, recordId])

  const getTitle = () => {
    switch (type) {
      case 'edit_medicine':
        return 'Edit Medicine'
      case 'edit_generic':
        return 'Edit Generic'
      case 'new_medicine':
        return 'New Medicine'
      default:
      case 'new_generic':
        return 'New Generic'
    }
  }

  const resetTriggeredSave = () => {
    console.log('resetting')
    setTriggeredSave(false)
  }

  const getForm = () => {
    return type === 'edit_medicine' || type === 'new_medicine' ? (
      <FormMedicineComponent
        id={recordId}
        triggeredSave={triggeredSave}
        setReloadListState={setReloadListState}
        toggleDrawer={toggleDrawer}
        resetTriggeredSave={resetTriggeredSave}
        drawerVisibilityChanged={v => {
          drawerVisibilityChanged(v)
        }}
      />
    ) : (
      <FormGenericComponent
        id={recordId}
        triggeredSave={triggeredSave}
        setGenericReloadState={setGenericReloadState}
        toggleDrawer={toggleDrawer}
        resetTriggeredSave={resetTriggeredSave}
        drawerVisibilityChanged={v => {
          drawerVisibilityChanged(v)
        }}
      />
    )
  }

  return (
    <>
      <DrawerComponent
        title={getTitle()}
        visibility={visibility}
        // visibility={drawerVisibility}
        drawerVisibilityChanged={v => {
          toggleDrawer(v)
          // setDrawerVisibility(v)
          // drawerVisibilityChanged(v)
        }}
        content={getForm()}
        footer={
          <div
            style={{
              textAlign: 'right',
            }}
          >
            <Button
              onClick={() => {
                toggleDrawer(false)
                // setDrawerVisibility(false)
                // drawerVisibilityChanged(false)
              }}
              disabled={triggeredSave}
              style={{ marginRight: 8 }}
            >
              Cancel
            </Button>
            <Button
              onClick={() => setTriggeredSave(true)}
              form="editform"
              type="primary"
              disabled={triggeredSave}
            >
              Save
            </Button>
          </div>
        }
      />
    </>
  )
}

export default Drawer
