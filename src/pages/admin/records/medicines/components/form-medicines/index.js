import React, { useState, useEffect } from 'react' // useState
import { Form, Input, message, Spin, Select, Switch } from 'antd'
import MedicineService from 'services/admin/medicines'
import MedicineGenericService from 'services/admin/medicine-generic'
import { onFinishFailed as BaseOnFinishFailed } from 'components/form'
import errorMedicine from 'components/400/medicine'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'

const { Option } = Select
const { TextArea } = Input

const MedicineComponent = props => {
  const { id, triggeredSave, resetTriggeredSave, setReloadListState, toggleDrawer } = props
  const [loading, setLoading] = useState(false)
  const [generics, setGenerics] = useState([])
  const [queryParams, setQueryParams] = useState({ limit: 0 })

  useEffect(() => {
    getGenerics()
    // console.log('triggeredSave', triggeredSave, id)
    if (triggeredSave) {
      doSave()
    }

    if (id) {
      pullRecord()
      setQueryParams(queryParams)
    } else {
      form.setFieldsValue({
        name: null,
        generic_name_id: null,
        dosage: null,
        unit: null,
        remarks: null,
        is_s2: null,
        is_injectable: null,
        is_active: null,
      })
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, triggeredSave])

  const [form] = Form.useForm()

  const getGenerics = async () => {
    await MedicineGenericService.browse(queryParams).then(result => {
      const generic = result.data.map(d => ({
        ids: d.id,
        name: d.name,
      }))
      setQueryParams(queryParams)
      console.log(generic)
      setGenerics(generic)
    })
  }

  const pullRecord = async () => {
    setLoading(true)

    await MedicineService.show(id)
      .then(result => {
        form.setFieldsValue({
          name: result.data.name,
          generic_name_id: result.data.generic_name_id,
          dosage: result.data.dosage,
          unit: result.data.unit,
          remarks: result.data.remarks,
          is_s2: result.data.is_s2,
          is_injectable: result.data.is_injectable,
          is_active: result.data.is_active,
        })

        setLoading(false)
      })
      .catch(() => {
        setLoading(false)
        message.error('There was a problem loading the list')
      })
  }

  // const doSave = () => {
  //   form.submit()
  // }

  const doSave = async () => {
    setLoading(true)

    if (id) {
      await MedicineService.edit(id, form.getFieldsValue())
        .then(() => {
          setLoading(false)
          resetTriggeredSave()
          setReloadListState()
          toggleDrawer()
          message.success('Data Successfully Updated')
        })
        .catch(e => {
          setLoading(false)
          resetTriggeredSave()
          errorMedicine(e)
        })
    } else {
      await MedicineService.add(form.getFieldsValue())
        .then(() => {
          setLoading(false)
          resetTriggeredSave()
          setReloadListState()
          toggleDrawer()
          message.success('Data Successfully Added')
        })
        .catch(e => {
          setLoading(false)
          resetTriggeredSave()
          errorMedicine(e)
        })
    }
    setLoading(false)
    resetTriggeredSave()
  }

  const onFinishFailed = errorInfo => {
    BaseOnFinishFailed(errorInfo)
    resetTriggeredSave()
  }

  return (
    <Spin spinning={loading}>
      <Form
        id="editform"
        form={form}
        labelCol={{ span: 5 }}
        wrapperCol={{ span: 14 }}
        scrollToFirstError="true"
        onFinishFailed={onFinishFailed}
        // onFinish={onFinish}
      >
        <Form.Item label="Medicine Name" name="name" rules={[{ required: true }]}>
          <Input />
        </Form.Item>

        <Form.Item label="Generic Name" name="generic_name_id" rules={[{ required: true }]}>
          <Select
            showSearch
            allowClear
            placeholder="Select Generic Name"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {generics.map(({ ids, name }) => (
              <Option key={ids} value={ids}>
                {name}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item label="Dosage" name="dosage" rules={[{ required: true }]}>
          <Input />
        </Form.Item>

        <Form.Item label="Unit" name="unit" rules={[{ required: true }]}>
          <Input />
        </Form.Item>

        <Form.Item name="is_s2" label="Is S2?" valuePropName="checked">
          <Switch checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} />
        </Form.Item>

        <Form.Item name="is_injectable" label="Is Injectable?" valuePropName="checked">
          <Switch checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} />
        </Form.Item>

        <Form.Item name="is_active" label="Set to Active?" valuePropName="checked">
          <Switch checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} />
        </Form.Item>

        <Form.Item label="Remarks" name="remarks">
          <TextArea rows={3} placeholder="Remarks" />
        </Form.Item>
      </Form>
    </Spin>
  )
}

export default MedicineComponent
