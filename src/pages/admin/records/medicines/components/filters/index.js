import React, { useEffect } from 'react'
import { Select, Space } from 'antd'
import { connect } from 'react-redux'

const { Option } = Select

const mapStateToProps = ({ recordMedicine }) => ({ recordMedicine })

const FiltersComponent = ({ dispatch, recordMedicine: { queryParams } }) => {
  useEffect(() => {}, [])

  const filterChanged = (key, value) => {
    if (key === null || value === 'all') {
      queryParams.filters = {}
    } else {
      queryParams.filters[key] = value
    }

    console.log('fileCHanged', key, value, queryParams.filters)

    dispatch({
      type: 'recordMedicine/SET_QUERY_PARAMS',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <Space>
      <Select
        placeholder="Is S2?"
        onChange={v => filterChanged('is_s2', v)}
        value={queryParams.filters.is_s2}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <Select
        placeholder="Is Injectable?"
        onChange={v => filterChanged('is_injectable', v)}
        value={queryParams.filters.is_injectable}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <Select
        placeholder="Is Active?"
        onChange={v => filterChanged('is_active', v)}
        value={queryParams.filters.is_active}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <a
        role="button"
        tabIndex={0}
        onClick={() => filterChanged(null)}
        onKeyDown={() => filterChanged(null)}
      >
        Clear Filters
      </a>
    </Space>
  )
}

export default connect(mapStateToProps)(FiltersComponent)
