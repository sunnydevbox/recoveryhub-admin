import React, { useState, useEffect } from 'react'
import BrowseComponent from 'components/bread/browse'
import { connect } from 'react-redux'
import { Space, Button, Row, Col } from 'antd'
import { SyncOutlined } from '@ant-design/icons'

import SearchbarGenericComponent from '../searchbar-generic'

const mapStateToProps = ({ recordGeneric }) => ({ recordGeneric })

const Browse = ({
  dispatch,
  recordGeneric: { reloadListState, dataSource, pagination, queryParams, listLoading },
  drawerVisibilityChanged,
  setGenericReloadState,
}) => {
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(() => {
    if (firstLoad || reloadListState) {
      setFirstLoad(false)
      getList()
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [reloadListState, firstLoad])

  const getList = () => {
    dispatch({
      type: 'recordGeneric/SET_BROWSE',
    })
  }

  const tableChanged = (page, filters, sorter, extra) => {
    queryParams.page = page
    queryParams.sorter = sorter

    console.log('(page, filters, sorter, extra)', page, filters, sorter, extra)
    // console.log('(page, filters, sorter, extra)', sorter)

    dispatch({
      type: 'recordGeneric/SET_QUERY_PARAMS',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Space>
          <SearchbarGenericComponent />
        </Space>
        <Row>
          <Col span={12}>Total Results: {pagination.total}</Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Space>
              <Button
                type="button"
                disabled={reloadListState}
                onClick={() => drawerVisibilityChanged('new_generic', null, true)}
              >
                New Generic
              </Button>
              <Button
                type="link"
                icon={
                  <SyncOutlined
                    onClick={() => setGenericReloadState(true)}
                    spin={reloadListState}
                    style={{
                      width: 'auto',
                      float: 'right',
                    }}
                  />
                }
                disabled={reloadListState}
              />
            </Space>
          </Col>
        </Row>

        <BrowseComponent
          rowKey={record => record.id}
          loading={reloadListState || listLoading}
          pagination={pagination}
          columns={[
            {
              title: 'Generic Name',
              dataIndex: 'name',
              key: 'name',
              defaultSortOrder: 'descend',
              sorter: (a, b) => a.age - b.age,
            },
            {
              title: 'Action',
              key: 'action',
              render: (text, record) => (
                <Space size="middle">
                  <Button
                    type="primary"
                    size="small"
                    onClick={() => drawerVisibilityChanged('edit_generic', record.id, true)}
                    danger
                  >
                    Edit
                  </Button>
                  <Button
                    type="primary"
                    danger
                    size="small"
                    onClick={() => {
                      console.log(record.id)
                    }}
                  >
                    Delete
                  </Button>
                </Space>
              ),
            },
          ]}
          dataSource={dataSource}
          tableChanged={tableChanged}
        />
      </Space>
    </>
  )
}

export default connect(mapStateToProps)(Browse)
