import React, { useEffect } from 'react'
import { Input } from 'antd'
import { connect } from 'react-redux'

const { Search } = Input

const mapStateToProps = ({ recordGeneric }) => ({ recordGeneric })

const SearchbarGenericComponent = ({ dispatch, recordGeneric: { queryParams, searchLoading } }) => {
  // const [state, setState] = useState('');

  useEffect(() => {}, [])

  const onSearch = str => {
    queryParams.searchTerm = str

    if (queryParams.page) {
      queryParams.page.current = 1
    }

    dispatch({
      type: 'recordGeneric/SET_SEARCH_TERM',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <>
      <Search
        placeholder="Name"
        onSearch={onSearch}
        style={{ width: 200 }}
        allowClear
        loading={searchLoading}
      />
    </>
  )
}

export default connect(mapStateToProps)(SearchbarGenericComponent)
