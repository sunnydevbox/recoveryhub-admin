import React, { useState, useEffect } from 'react' // useState
import { Form, Input, message, Spin } from 'antd'
import MedicineGenericService from 'services/admin/medicine-generic'
import { onFinishFailed as BaseOnFinishFailed } from 'components/form'
import errorGeneric from 'components/400/medicine-generic'

const MedicineGenericComponent = props => {
  const { id, triggeredSave, resetTriggeredSave, setGenericReloadState, toggleDrawer } = props
  const [loading, setLoading] = useState(false)
  const [form] = Form.useForm()

  useEffect(() => {
    // console.log('triggeredSave', triggeredSave, id)
    if (triggeredSave) {
      doSave()
    }

    if (id) {
      pullRecord()
    } else {
      form.setFieldsValue({
        name: null,
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, triggeredSave])

  const pullRecord = async () => {
    setLoading(true)

    await MedicineGenericService.show(id)
      .then(result => {
        form.setFieldsValue({
          name: result.data.name,
        })
        setLoading(false)
      })
      .catch(() => {
        setLoading(false)
        message.error('There was a problem loading the list')
      })
  }

  // const doSave = () => {
  //   form.submit()
  // }

  const doSave = async () => {
    setLoading(true)

    if (id) {
      await MedicineGenericService.edit(id, form.getFieldsValue())
        .then(() => {
          setLoading(false)
          resetTriggeredSave()
          setGenericReloadState()
          toggleDrawer()
          message.success('Data Successfully Updated')
        })
        .catch(e => {
          setLoading(false)
          resetTriggeredSave()
          errorGeneric(e)
        })
    } else {
      await MedicineGenericService.add(form.getFieldsValue())
        .then(() => {
          setLoading(false)
          resetTriggeredSave()
          setGenericReloadState()
          toggleDrawer()
          message.success('Data Successfully Added')
        })
        .catch(e => {
          setLoading(false)
          resetTriggeredSave()
          errorGeneric(e)
        })
    }
    setLoading(false)
    resetTriggeredSave()
  }

  const onFinishFailed = errorInfo => {
    BaseOnFinishFailed(errorInfo)
    resetTriggeredSave()
  }

  return (
    <Spin spinning={loading}>
      <Form
        id="editform"
        form={form}
        labelCol={{ span: 5 }}
        wrapperCol={{ span: 14 }}
        scrollToFirstError="true"
        onFinishFailed={onFinishFailed}
        // onFinish={onFinish}
        layout="horizontal"
      >
        <Form.Item label="Generic Name" name="name" rules={[{ required: true, min: 3 }]}>
          <Input />
        </Form.Item>
      </Form>
    </Spin>
  )
}

export default MedicineGenericComponent
