import React, { useEffect, useState, useCallback } from 'react'
import { Space, Spin, Row, Col } from 'antd'
import { connect } from 'react-redux'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import timeGridPlugin from '@fullcalendar/timegrid'
import moment from 'moment'
import CalendarFiltersComponent from './components/filters'
import './styles.scss'
import { STORE_PREFIX } from '../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }),
    setFilter: filters => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, filters }),
    setRecord: id => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }),
  }
}

const FeatureBrowseComponent = ({
  [STORE_PREFIX]: { dataList, listLoading },
  toggleDrawer,
  setFilter,
  setRecord,
}) => {
  const calendarRef = React.createRef()
  const [firstLoad, setFirstLoad] = useState(true)

  const setFil = useCallback(
    filter => {
      setFilter(filter)
    },
    [setFilter],
  )

  const trig = useCallback(
    fetchInfo => {
      if (calendarRef && calendarRef.current) {
        let s = moment(
          calendarRef.current.getApi().currentDataManager.state.dateProfile.activeRange.start,
        ).format('YYYY-M-DD 0:0')
        let ee = moment(
          calendarRef.current.getApi().currentDataManager.state.dateProfile.activeRange.end,
        ).format('YYYY-M-DD 23:59')

        // OVERRIDES
        if (fetchInfo && fetchInfo.startStr) {
          s = moment(fetchInfo.startStr).format('YYYY-M-DD 0:0')
          ee = moment(fetchInfo.endStr).format('YYYY-M-DD 23:59')
        }

        setFil({
          dateRange: `${s}|${ee}`,
          limit: 0,
        })
      }
    },
    [calendarRef, setFil],
  )

  useEffect(() => {
    if (firstLoad) {
      setFirstLoad(false)
      trig(null)
    }
  }, [firstLoad, trig, dataList])

  const eventClicked = info => {
    setRecord(info.event.extendedProps.data.id)
    toggleDrawer()
  }

  const eventContent = info => {
    const { data } = info.event.extendedProps
    let doc = ''
    let px = ''
    let time = ''

    if (data.doc) {
      doc = `Dr: ${data.doc.f} ${data.doc.l}`
    }

    if (data.px) {
      px = `PX: ${data.px.f} ${data.px.l}`
    }
    // console.log('eventContent',info)
    time = `${moment(data.start.t, 'YYYY-MM-DD HH:mm:ss').format('hh:mm A')} - ${moment(
      data.end.t,
      'YYYY-MM-DD HH:mm:ss',
    ).format('hh:mm A')}`
    // return info
    return {
      html: `
        <div class="event container">
          <div class="time">${time}</div>
          <div class="status">${data.s}</div>
          <div class="row">
            <div class="doc col-sm-6">
              <div class="caRd">${doc}<br/>${data.doc.r}</div>
            </div>
            <div class="px col-sm-6">
              <div class="caRd">${px}</div> 
            </div>
          </div>
        </div>
      `,
    }
  }

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Row>
          <Col span={12}>
            Displaying Results: <strong>{Object.keys(dataList).length} results</strong>
          </Col>
          <Col span={12} justify="e nd">
            Doctor Schedules
          </Col>
        </Row>
      </Space>
      <Spin spinning={listLoading}>
        <CalendarFiltersComponent />
        asdasd
        <FullCalendar
          ref={calendarRef}
          events={Object.values(dataList)}
          plugins={[dayGridPlugin, interactionPlugin, timeGridPlugin]}
          headerToolbar={{
            left: 'prev,next,today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek',
          }}
          initialView="dayGridMonth"
          eventClick={eventClicked}
          eventContent={eventContent}
          eventClassNames={arg => {
            return `status-${arg.event.extendedProps.data.s}`
          }}
          datesSet={trig}
        />
      </Spin>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FeatureBrowseComponent)
