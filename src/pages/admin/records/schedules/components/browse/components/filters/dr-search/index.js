import React, {  } from 'react'
import { Input } from 'antd'
import { connect } from 'react-redux'
import DropdownComponent from 'components/form/dropdown-search-data'
import  ScheduleService from 'services/admin/calendar'
import { STORE_PREFIX } from '../../../../../config'
import './styles.scss'

const { Search } = Input

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filter => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, payload: filter }),
  }
}

const CalendarFilterDoctorSearchComponent = ({ setFilter }) => {
  // const [value, setValue] = useState('')

  const onChange = e => {
    console.log(e)
    setFilter({ drname: e })
    // setValue(e)
  }

  return (
    <>
      <DropdownComponent
        Service={ScheduleService}
      />
      <Search
        placeholder="Search Doctor"
        onSearch={onChange}
        style={{ width: '100%' }}
      />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarFilterDoctorSearchComponent)
