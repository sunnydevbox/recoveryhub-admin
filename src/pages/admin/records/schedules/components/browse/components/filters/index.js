import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import CalendarFilterStatusComponent from './status'
import CalendarFilterRolesComponent from './roles'
import CalendarFilterDoctorSearchComponent from './dr-search'
import { STORE_PREFIX } from '../../../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = () => {
  return {
    // toggleDrawer: () => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }),
  }
}

const CalendarFilterComponent = () => {
  useEffect(() => {}, [])

  return (
    <>
      Status, Doctor name, px name, overdue paymets, Roles
      <br />
      Filters:
      <div className="row">
        <div className="col-sm-4">
          <div className="form-group">
            <div>Status</div>
            <CalendarFilterStatusComponent />
          </div>
        </div>
        <div className="col-sm-4">
          <div className="form-group">
            <div>Specialization</div>
            <CalendarFilterRolesComponent />
          </div>
        </div>
        <div className="col-sm-4">
          <div className="form-group">
            <div>Search</div>
            <CalendarFilterDoctorSearchComponent />
          </div>
        </div>
      </div>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarFilterComponent)
