import React, { useState, useEffect } from 'react'
import { Select } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from '../../../../../config'
import './styles.scss'

const options = [
  // { label: 'All', value: 'all' },
  { label: 'Psychiatrist', value: 'psychiatrist' },
  { label: 'Psychologist', value: 'psychologist' },
  { label: 'Dermatologist', value: 'dermatologist' },
  { label: 'Surgeon', value: 'surgeon' },
  { label: 'Internist', value: 'internist' },
  { label: 'Family Physician', value: 'family-physician' },
  { label: 'Neurologist', value: 'neurologist' },
  { label: 'ENT', value: 'ent' },
  { label: 'Opthalmologist', value: 'opthalmologist' },
  { label: 'Pediatrician', value: 'pediatrician' },
  { label: 'Gynecologist', value: 'gynecologist' },
  { label: 'Rehab Medicine', value: 'rehab-medicine' },
]

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filter => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, payload: filter }),
  }
}

const CalendarFilterRolesComponent = ({ setFilter }) => {
  const [value, setValue] = useState('psychiatrist')
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(() => {
    if (firstLoad) {
      setFirstLoad(false)
      // setFilter({ status: value })
    }
  }, [firstLoad, setFirstLoad])

  const onChange = e => {
    setFilter({ roles: e })
    setValue(e)
  }

  return (
    <>
      <Select
        options={options}
        defaultValue={value}
        onChange={onChange}
        style={{ width: '100%' }}
      />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarFilterRolesComponent)
