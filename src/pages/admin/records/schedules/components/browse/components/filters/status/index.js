import React, { useState, useEffect } from 'react'
import { Select } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from '../../../../../config'
import './styles.scss'

const options = [
  { label: 'All', value: 'all' },
  { label: 'Open', value: 'open' },
  { label: 'Booked', value: 'booked' },
  { label: 'Reserved', value: 'reserved' },
]

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filters => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, payload: filters }),
  }
}

const CalendarFilterStatusComponent = ({ setFilter }) => {
  const [value, setValue] = useState('open')
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(() => {
    if (firstLoad) {
      setFirstLoad(false)
    }
  }, [firstLoad, setFirstLoad])

  const onChange = e => {
    setFilter({ status: e })
    setValue(e)
  }

  return (
    <>
      <Select
        options={options}
        defaultValue={value}
        onChange={onChange}
        style={{ width: '100%' }}
      />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarFilterStatusComponent)
