import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { STORE_PREFIX, API_SERVICE } from '../../../../../../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }),
  }
}

const TabOrderTransactionsComponent = ({ [STORE_PREFIX]: { selectedRecord } }) => {
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(() => {
    if (firstLoad) {
      API_SERVICE.dataTransactionHistory()
      setFirstLoad(false)
    }
  }, [firstLoad, selectedRecord])

  return <>List the transactions of this schedule</>
}

export default connect(mapStateToProps, mapDispatchToProps)(TabOrderTransactionsComponent)
