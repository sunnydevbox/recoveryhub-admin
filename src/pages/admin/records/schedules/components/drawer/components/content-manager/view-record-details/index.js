import React, { useEffect } from 'react'
import { Skeleton, Tabs } from 'antd'
import { connect } from 'react-redux'
// import moment from 'moment'
import parse from 'html-react-parser'
import TabOrderTransactionsComponent from './tabs/order-transactions'
// import TabActionsComponent from './tabs/actions'
import TabCallLogsComponent from './tabs/call-logs'
import { STORE_PREFIX } from '../../../../../config'

const { TabPane } = Tabs

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  useEffect(() => {
    console.log(selectedRecord)
  }, [selectedRecord])

  const panes = [
    { title: 'Transaction History', content: <TabOrderTransactionsComponent />, key: '1' },
    { title: 'Call Log', content: <TabCallLogsComponent />, key: '2' },
    // { title: 'Actions', content: <TabActionsComponent />, key: '3' },
    { title: 'Logs', content: 'Action Logs', key: '4' },
  ]

  const getName = u => {
    let name = '<em>N/A</em>'
    if (u) {
      name = `${u.f} ${u.l}`
    }

    return name
  }

  const getOTLink = () => {
    let otLink = null

    if (selectedRecord.opentok) {
      otLink = selectedRecord.opentok
      return `<a target="_blank" href="https://tokbox.com/developer/tools/inspector/account/3521142/project/46190132/session/${otLink}">LINK</a>`
    }

    return '<em>N/A</em>'
  }

  const getEventInfo = () => {
    let detail = <Skeleton />

    if (!loadingSelectedRecord && selectedRecord) {
      detail = parse(
        `Doctor: Dr. ${getName(selectedRecord.doc)}<br />
                PX: ${getName(selectedRecord.px)} <br />
                Status: ${selectedRecord.s}<br />
                OT Inspector: ${getOTLink()}
                `,
      )
    }

    return detail
  }

  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            {getEventInfo()}
            <div className="row mb-4">
              <div className="col-sm-12">
                <strong>Contact Info </strong>
                <hr className="mb-1 mt-2" />
              </div>
            </div>

            <div>
              <Tabs hideAdd type="editable-card">
                {panes.map(pane => (
                  <TabPane tab={pane.title} key={pane.key}>
                    {pane.content}
                  </TabPane>
                ))}
              </Tabs>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps)(ViewRecordDetailsComponent)
