import React, { useState, useEffect } from 'react'
import { Form, DatePicker, Modal, Button, notification } from 'antd'
import moment from 'moment'
import CalendarActionsService from 'services/admin/calendar/actions'
import { connect } from 'react-redux'

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: 'recordSchedules/TOGGLE_DRAWER' }),
    getCalendarEntries: params =>
      dispatch({ type: 'recordSchedules/GET_CALENDAR_ENTRIES', payload: { params } }),
  }
}

const App = ({
  recordSchedules: { drawerState, selectedEvent },
  getCalendarEntries,
  toggleDrawer,
}) => {
  const [visible, setVisible] = useState(false)
  const [confirmLoading, setConfirmLoading] = useState(false)
  const [form] = Form.useForm()

  useEffect(() => {
    if (selectedEvent) {
      const startAt = selectedEvent.start_at
      const endAt = selectedEvent.end_at
      console.log(startAt)
      form.setFieldsValue({
        start_at: startAt ? moment(startAt.date) : null,
        end_at: endAt ? moment(endAt.date) : null,
      })
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [drawerState, selectedEvent])

  console.log(selectedEvent)
  const doSave = async () => {
    const data = {
      id: selectedEvent.id,
      start_at: form.getFieldValue('start_at').format('YYYY-MM-DD H:mm:ss'),
      end_at: form.getFieldValue('end_at').format('YYYY-MM-DD H:mm:ss'),
    }
    console.log(data)

    await CalendarActionsService.modifyTime(data)
      .then(() => {
        getCalendarEntries()
        toggleDrawer()
        notification.success({
          message: 'Success',
          description: 'Data Successfully Updated',
        })
      })
      .catch(e => {
        throw e
      })
  }

  const showModal = () => {
    setVisible(true)
  }

  const handleOk = () => {
    try {
      doSave()
      setConfirmLoading(true)
      setVisible(false)
      setConfirmLoading(false)
    } catch (e) {
      console.log(e)
    }
  }

  const handleCancel = () => {
    setVisible(false)
  }

  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf('day')
  }

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Modify Time
      </Button>
      <Modal
        title="Modify Time"
        visible={visible}
        width={300}
        style={{ top: 20 }}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        forceRender
      >
        <Form
          form={form}
          layout="vertical"
          scrollToFirstError="true"
          // onFinishFailed={onFinishFailed}
          // onFinish={onFinish}
        >
          <Form.Item name="start_at" label="Start At:">
            <DatePicker
              placeholder="Select Starting Date"
              format="YYYY-MM-DD HH:mm:ss"
              disabledDate={disabledDate}
              showTime
            />
          </Form.Item>

          <Form.Item name="end_at" label="End At:">
            <DatePicker
              placeholder="Select Ending Date"
              format="YYYY-MM-DD HH:mm:ss"
              disabledDate={disabledDate}
              showTime
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
