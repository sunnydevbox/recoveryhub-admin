/* V1.1 */
import Service from 'services/admin/calendar/'

export const LABEL_SINGULAR = 'Schedule'
export const LABEL_PLURAL = 'Schedules'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureAdminToolSchedules'
export const API_SERVICE = Service
