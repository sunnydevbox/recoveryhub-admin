import React, { useState, useEffect } from 'react'
import BrowseComponent from 'components/bread/browse'
import { Space, Button, Row, Col } from 'antd'
import { CheckCircleOutlined, ExclamationCircleOutlined, SyncOutlined } from '@ant-design/icons'
import moment from 'moment'
import { connect } from 'react-redux'
import DrawerComponent from '../drawer'
import FiltersComponent from '../filters'
import SearchbarComponent from '../searchbar'

const mapStateToProps = ({ recordPatients }) => ({ recordPatients })

const Browse = ({
  dispatch,
  recordPatients: { dataSource, pagination, listLoading, queryParams },
}) => {
  const [drawerVisibility, setDrawerVisibility] = useState(false)

  const getList = async () => {
    dispatch({
      type: 'recordPatients/SET_BROWSE',
    })
  }

  useEffect(() => {
    getList()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [queryParams])

  const tableChanged = (page, filters, sorter, extra) => {
    queryParams.page = page
    queryParams.sorter = sorter

    console.log('(page, filters, sorter, extra)', page, filters, sorter, extra)
    // console.log('(page, filters, sorter, extra)', sorter)

    dispatch({
      type: 'recordPatients/SET_QUERY_PARAMS',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Space>
          <SearchbarComponent />
          <FiltersComponent />
        </Space>
        <Row>
          <Col span={12}>Total Results: {pagination.total}</Col>
          <Col span={12} justify="end">
            <Button
              type="link"
              block
              icon={
                <SyncOutlined
                  onClick={getList}
                  spin={listLoading}
                  style={{
                    width: 'auto',
                    float: 'right',
                  }}
                />
              }
              disabled={listLoading}
            />
          </Col>
        </Row>

        <BrowseComponent
          rowKey={record => record.id}
          loading={listLoading}
          pagination={pagination}
          columns={[
            {
              title: 'Name',
              dataIndex: 'name',
              key: 'name',
              defaultSortOrder: 'descend',
              sorter: (a, b) => a.age - b.age,
            },
            {
              title: 'Status',
              dataIndex: 'status',
              key: 'status',
              render: status => <>{status.toUpperCase()}</>,
            },
            {
              title: 'Verified?',
              dataIndex: 'is_verified',
              key: 'is_verified',
              render: isVerified => (
                <>
                  {isVerified ? (
                    <CheckCircleOutlined style={{ color: 'rgb(55 160 3)' }} />
                  ) : (
                    <ExclamationCircleOutlined style={{ color: 'rgb(212 3 3)' }} />
                  )}
                </>
              ),
            },
            {
              title: 'Date Joined',
              dataIndex: 'joined_at',
              key: 'joined_at',
              render: joinedAt => <>{moment(joinedAt).format('MMM DD, YYYY  HH:mm')}</>,
              defaultSortOrder: 'descend',
              sorter: (a, b) => a.age - b.age,
            },
            {
              title: 'Deleted?',
              dataIndex: 'is_deleted',
              key: 'is_deleted',
              render: isDeleted => <>{isDeleted ? <>Yes</> : <>No</>}</>,
            },
            {
              title: 'Roles',
              dataIndex: 'roles',
              key: 'roles',
              render: roles => (
                <>{typeof roles === 'object' ? roles.join('&nbsp;') : <em>Not Set</em>}</>
              ),
            },
            {
              title: 'Action',
              key: 'action',
              render: (text, record) => (
                <Space size="middle">
                  <Button
                    type="primary"
                    size="small"
                    onClick={() => {
                      setDrawerVisibility(true)
                      console.log(record.id)
                    }}
                    danger
                  >
                    Edit
                  </Button>
                  <Button
                    type="primary"
                    danger
                    size="small"
                    onClick={() => {
                      console.log(record.id)
                    }}
                  >
                    Delete
                  </Button>
                </Space>
              ),
            },
          ]}
          dataSource={dataSource}
          tableChanged={tableChanged}
        />
      </Space>

      <DrawerComponent
        drawerVisibility={drawerVisibility}
        drawerVisibilityChanged={visibility => setDrawerVisibility(visibility)}
      />
    </>
  )
}

export default connect(mapStateToProps)(Browse)
