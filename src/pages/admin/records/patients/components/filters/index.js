import React, { useEffect } from 'react'
import { Select, Space } from 'antd'
import { connect } from 'react-redux'

const { Option } = Select

const mapStateToProps = ({ recordPatients }) => ({ recordPatients })

const FiltersComponent = ({ dispatch, recordPatients: { queryParams } }) => {
  useEffect(() => {}, [])

  const filterChanged = (key, value) => {
    if (key === null) {
      queryParams.filters = {}
    } else {
      queryParams.filters[key] = value
    }

    console.log('fileCHanged', key, value, queryParams.filters)

    dispatch({
      type: 'recordPatients/SET_QUERY_PARAMS',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <Space>
      <Select
        placeholder="Select Status"
        onChange={v => filterChanged('status', v)}
        value={queryParams.filters.status}
      >
        <Option value="all">All</Option>
        <Option value="active">Active</Option>
        <Option value="inactive">Inactive</Option>
      </Select>

      <Select
        placeholder="Is Verified?"
        onChange={v => filterChanged('verified', v)}
        value={queryParams.filters.verified}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <Select
        placeholder="Is Deleted?"
        onChange={v => filterChanged('deleted', v)}
        value={queryParams.filters.deleted}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <a
        role="button"
        tabIndex={0}
        onClick={() => filterChanged(null)}
        onKeyDown={() => filterChanged(null)}
      >
        Clear Filters
      </a>
    </Space>
  )
}

export default connect(mapStateToProps)(FiltersComponent)
