import React, { useState, useEffect } from 'react'
import { Drawer } from 'antd'
import FormComponent from '../form'

function DrawerComponent(props) {
  const { drawerVisibilityChanged } = props
  const [drawerVisibility, setDrawerVisibility] = useState(false)

  useEffect(() => {
    setDrawerVisibility(props.drawerVisibility)
  }, [props])

  return (
    <>
      <Drawer
        title="Basic Drawer"
        placement="right"
        width="50%"
        closable={false}
        onClose={() => {
          setDrawerVisibility(false)
          drawerVisibilityChanged(false)
        }}
        visible={drawerVisibility}
      >
        <FormComponent />
      </Drawer>
    </>
  )
}

export default DrawerComponent
