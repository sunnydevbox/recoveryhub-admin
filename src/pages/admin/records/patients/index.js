import React from 'react'
import { Helmet } from 'react-helmet'
import Browse from './components/browse'

const Patients = () => {
  return (
    <div>
      <Helmet title="Patients" />

      <div className="kit__utils__heading">
        <h5>
          <span className="mr-3">Patients</span>
        </h5>
      </div>
      <div className="card">
        <div className="card-body">
          <Browse />
        </div>
      </div>
    </div>
  )
}

export default Patients
