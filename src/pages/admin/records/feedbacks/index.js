import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Helmet } from 'react-helmet'
import BrowseComponent from './components/browse'
import DrawerComponent from './components/drawer'

const mapStateToProps = ({ recordFeedback }) => ({ recordFeedback })

const Feedbacks = ({ dispatch, recordFeedback: { drawerState } }) => {
  const [drawerRecordId, setDrawerRecordId] = useState(null)
  const [drawerType, setDrawerType] = useState(null)

  useEffect(() => {}, [drawerState])

  const setReloadListState = (reload = true) => {
    dispatch({
      type: 'recordFeedback/TRIGGER_RELOAD_BROWSE',
      payload: {
        reloadListState: reload,
      },
    })
  }

  const toggleDrawer = state => {
    dispatch({
      type: 'recordFeedback/TOGGLE_DRAWER',
      payload: {
        drawerState: typeof state === 'undefined' ? !drawerState : state,
      },
    })
  }

  return (
    <>
      <Helmet title="Patients" />

      <div className="kit__utils__heading">
        <h5>
          <span className="mr-3">Feedbacks</span>
        </h5>
      </div>
      <div className="card">
        <div className="card-body">
          <BrowseComponent
            toggleDrawer={toggleDrawer}
            setReloadListState={setReloadListState}
            drawerVisibilityChanged={(type, id, visibility) => {
              setDrawerType(type)
              toggleDrawer(visibility)
              setDrawerRecordId(id)
            }}
          />
          <DrawerComponent
            recordId={drawerRecordId}
            visibility={drawerState}
            setReloadListState={setReloadListState}
            toggleDrawer={toggleDrawer}
            type={drawerType}
          />
        </div>
      </div>
    </>
  )
}

export default connect(mapStateToProps)(Feedbacks)
