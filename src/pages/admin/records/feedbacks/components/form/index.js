import React, { useState, useEffect } from 'react' // useState
import { Form, Input, Spin, InputNumber, Select, DatePicker, Switch, message } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import moment from 'moment'
import CouponCodeService from 'services/admin/coupon-codes'
import { onFinishFailed as BaseOnFinishFailed } from 'components/form'
import errorCoupon from 'components/400/coupon-codes'

const { TextArea } = Input

const FormComponent = props => {
  const { id, triggeredSave, resetTriggeredSave, setReloadListState, toggleDrawer } = props
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    // console.log('triggeredSave', triggeredSave, id)
    if (triggeredSave) {
      doSave()
    }

    if (id) {
      pullRecord()
    } else {
      form.setFieldsValue({
        code: null,
        name: null,
        description: null,
        value: 0,
        discount: 0,
        item_type: 'both',
        usage_limit: 0,
        active: 0,
        expires_at: null,
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, triggeredSave])

  const [form] = Form.useForm()

  const pullRecord = async () => {
    setLoading(true)

    await CouponCodeService.show(id)
      .then(result => {
        const expiresAt = result.data.expires_at
        form.setFieldsValue({
          code: result.data.code,
          name: result.data.name,
          description: result.data.description,
          value: result.data.value,
          discount: result.data.discount,
          item_type: result.data.item_type,
          usage_limit: result.data.usage_limit,
          expires_at: expiresAt ? moment.utc(expiresAt.date) : null,
          active: result.data.active,
        })
        setLoading(false)
      })
      .catch(() => {
        setLoading(false)
        message.error('There was a problem loading the list')
      })
  }

  // const doSave = () => {
  //   form.submit()
  // }

  const doSave = async () => {
    setLoading(true)
    if (id) {
      await CouponCodeService.edit(id, form.getFieldsValue())
        .then(() => {
          message.success('Form saved')
          setLoading(false)
          resetTriggeredSave()
          setReloadListState()
          toggleDrawer()
        })
        .catch(e => {
          setLoading(false)
          resetTriggeredSave()
          errorCoupon(e)
        })
    } else {
      await CouponCodeService.add(form.getFieldsValue())
        .then(() => {
          message.success('Form saved')
          setLoading(false)
          resetTriggeredSave()
          setReloadListState()
          toggleDrawer()
        })
        .catch(e => {
          setLoading(false)
          resetTriggeredSave()
          errorCoupon(e)
        })
    }
  }

  const onFinishFailed = errorInfo => {
    BaseOnFinishFailed(errorInfo)
    resetTriggeredSave()
  }

  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf('day')
  }

  return (
    <Spin spinning={loading}>
      <Form
        id="editform"
        form={form}
        layout="vertical"
        scrollToFirstError="true"
        onFinishFailed={onFinishFailed}
        // onFinish={onFinish}
      >
        <Form.Item label="Coupon Code" name="code" rules={[{ required: true }]}>
          <Input placeholder="Coupon Code" style={{ width: 560 }} />
        </Form.Item>

        <Form.Item label="Coupon Name" name="name" rules={[{ required: true }]}>
          <Input placeholder="Coupon Name" style={{ width: 560 }} />
        </Form.Item>

        <Form.Item label="Description" name="description" rules={[{ required: true }]}>
          <TextArea rows={5} placeholder="Description" style={{ width: 560 }} />
        </Form.Item>

        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Value" name="value">
              <InputNumber placeholder="Value" min={0} style={{ width: 250 }} />
            </Form.Item>
          </div>

          <div className="col-md-6">
            <Form.Item label="Discount" name="discount">
              <InputNumber placeholder="Discount" min={0} max={1} style={{ width: 250 }} />
            </Form.Item>
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Applies To" name="item_type">
              <Select
                placeholder="Item Type"
                style={{ width: 250 }}
                options={[
                  {
                    value: 'both',
                    label: 'All',
                  },
                  {
                    value: 'appointment',
                    label: 'Appointments',
                  },
                  {
                    value: 'medicine',
                    label: 'Medicines',
                  },
                ]}
              />
            </Form.Item>
          </div>

          <div className="col-md-6">
            <Form.Item label="Usage Limit" name="usage_limit" rules={[{ required: true }]}>
              <InputNumber placeholder="Usage Limit" style={{ width: 250 }} />
            </Form.Item>
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <Form.Item name="expires_at" label="Expires At">
              <DatePicker
                placeholder="Select Expiration Date"
                format="YYYY-MM-DD HH:mm:ss"
                disabledDate={disabledDate}
                showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
              />
            </Form.Item>
          </div>

          <div className="col-md-6">
            <Form.Item name="active" label="Set to Active?" valuePropName="checked">
              <Switch checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} />
            </Form.Item>
          </div>
        </div>
      </Form>
    </Spin>
  )
}

export default FormComponent
