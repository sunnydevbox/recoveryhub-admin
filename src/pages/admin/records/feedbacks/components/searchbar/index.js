import React from 'react' // , { useState, useEffect }
import { Input } from 'antd'
import { connect } from 'react-redux'

const { Search } = Input

const mapStateToProps = ({ recordFeedback }) => ({ recordFeedback })

const SearchbarComponent = ({ dispatch, recordFeedback: { queryParams, searchLoading } }) => {
  const onSearch = str => {
    queryParams.searchTerm = str

    if (queryParams.page) {
      queryParams.page.current = 1
    }

    dispatch({
      type: 'recordFeedback/SET_SEARCH_TERM',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <>
      <Search
        placeholder="Name"
        onSearch={onSearch}
        style={{ width: 200 }}
        allowClear
        loading={searchLoading}
      />
    </>
  )
}

export default connect(mapStateToProps)(SearchbarComponent)
