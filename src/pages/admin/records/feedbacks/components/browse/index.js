import React, { useState, useEffect } from 'react'
import BrowseComponent from 'components/bread/browse'
import { Space, Button, Row, Col } from 'antd'
import { SyncOutlined } from '@ant-design/icons'
import { connect } from 'react-redux'
// import FiltersComponent from '../filters'
import SearchbarComponent from '../searchbar'
// import FormRole from '../form-role'

const mapStateToProps = ({ recordFeedback }) => ({ recordFeedback })

const Browse = ({
  drawerVisibilityChanged,
  setReloadListState,
  dispatch,
  recordFeedback: { reloadListState, dataSource, pagination, listLoading, queryParams },
}) => {
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(() => {
    console.log('reloadListState', reloadListState)
    if (firstLoad || reloadListState) {
      setFirstLoad(false)
      getList()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [reloadListState, firstLoad])

  const getList = async () => {
    dispatch({
      type: 'recordFeedback/SET_BROWSE',
    })
  }

  const tableChanged = (page, filters, sorter, extra) => {
    queryParams.page = page
    queryParams.sorter = sorter

    console.log('(page, filters, sorter, extra)', page, filters, sorter, extra)
    // console.log('(page, filters, sorter, extra)', sorter)

    dispatch({
      type: 'recordFeedback/SET_QUERY_PARAMS',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Space>
          <SearchbarComponent />
        </Space>
        <Row>
          <Col span={12}>Total Results: {pagination.total}</Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Space>
              <Button
                type="button"
                disabled={reloadListState}
                onClick={() => drawerVisibilityChanged('new_coupon', null, true)}
              >
                Add Coupon Code
              </Button>
              <Button
                type="link"
                icon={
                  <SyncOutlined
                    onClick={() => setReloadListState(true)}
                    spin={reloadListState}
                    style={{
                      width: 'auto',
                      float: 'right',
                    }}
                  />
                }
                disabled={reloadListState}
              />
            </Space>
          </Col>
        </Row>

        <BrowseComponent
          rowKey={record => record.id}
          loading={reloadListState || listLoading}
          pagination={pagination}
          columns={[
            {
              title: 'Name',
              dataIndex: 'form_data',
              key: 'formData',
              render: formData => <>{formData.name}</>,
            },

            {
              title: 'Subject',
              dataIndex: 'subject',
              key: 'subject',
            },

            {
              title: 'Contents',
              dataIndex: 'contents',
              key: 'contents',
            },

            {
              title: 'From',
              dataIndex: 'from',
              key: 'from',
            },

            {
              title: 'Phone',
              dataIndex: 'form_data',
              key: 'formData',
              render: formData => <>{formData.phone}</>,
            },

            {
              title: 'Action',
              key: 'action',
              render: (text, record) => (
                <Space size="middle">
                  <Button
                    type="primary"
                    size="small"
                    onClick={() => drawerVisibilityChanged('edit_coupon', record.id, true)}
                    danger
                  >
                    Edit
                  </Button>
                  <Button
                    type="primary"
                    danger
                    size="small"
                    onClick={() => {
                      console.log(record.id)
                    }}
                  >
                    Delete
                  </Button>
                </Space>
              ),
            },
          ]}
          dataSource={dataSource}
          tableChanged={tableChanged}
        />
      </Space>
    </>
  )
}

export default connect(mapStateToProps)(Browse)
