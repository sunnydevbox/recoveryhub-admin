import React, { useEffect, useState, useCallback } from 'react'
import { Radio } from 'antd'
import { connect } from 'react-redux'
import './styles.scss'

const options = [
  // { label: 'All', value: 'all' },
  { label: 'Psychiatrist', value: 'psychiatrist' },
  { label: 'Psychologist', value: 'psychologist' },
  { label: 'Dermatologist', value: 'dermatologist' },
  { label: 'Surgeon', value: 'surgeon' },
  { label: 'Internist', value: 'internist' },
  { label: 'Family Physician', value: 'family-physician' },
  { label: 'Neurologist', value: 'neurologist' },
  { label: 'ENT', value: 'ent' },
  { label: 'Opthalmologist', value: 'opthalmologist' },
  { label: 'Pediatrician', value: 'pediatrician' },
  { label: 'Gynecologist', value: 'gynecologist' },
  { label: 'Rehab Medicine', value: 'rehab-medicine' },
]

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filter => dispatch({ type: 'recordSchedules/SET_FILTER', payload: filter }),
  }
}

const CalendarFilterRolesComponent = ({ setFilter }) => {
  const [value, setValue] = useState('')

  const setFil = useCallback(
    filter => {
      setFilter(filter)
    },
    [setFilter],
  )

  useEffect(() => {
    // if (firstLoad) {
    //   setFirstLoad(false)
    // }
  }, [])

  const onChange = e => {
    setFil({ roles: e.target.value })
    setValue(e.target.value)
  }

  return (
    <>
      [Roles]
      <Radio.Group options={options} value={value} onChange={onChange} />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarFilterRolesComponent)
