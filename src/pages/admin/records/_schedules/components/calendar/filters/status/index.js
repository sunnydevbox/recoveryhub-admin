import React, { useEffect, useState, useCallback } from 'react'
import { Radio } from 'antd'
import { connect } from 'react-redux'
import './styles.scss'

const options = [
  { label: 'All', value: 'all' },
  { label: 'Open', value: 'open' },
  { label: 'Booked', value: 'booked' },
  { label: 'Reserved', value: 'reserved' },
]

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filter => dispatch({ type: 'recordSchedules/SET_FILTER', payload: filter }),
  }
}

const CalendarFilterStatusComponent = ({ setFilter }) => {
  const [value, setValue] = useState('open')

  const setFil = useCallback(
    filter => {
      setFilter(filter)
    },
    [setFilter],
  )

  useEffect(() => {
    // if (firstLoad) {
    //   setFirstLoad(false)
    // }
  }, [])

  const onChange = e => {
    setFil({ status: e.target.value })
    setValue(e.target.value)
  }

  return (
    <>
      [Status]
      <Radio.Group options={options} value={value} onChange={onChange} />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarFilterStatusComponent)
