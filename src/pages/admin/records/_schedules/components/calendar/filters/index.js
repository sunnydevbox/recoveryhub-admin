import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import CalendarFilterStatusComponent from './status'
import CalendarFilterRolesComponent from './roles'
import CalendarFilterDoctorSearchComponent from './dr-search'
import './styles.scss'

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: 'recordSchedules/TOGGLE_DRAWER' }),
  }
}

const CalendarFilterComponent = () => {
  useEffect(() => {}, [])

  return (
    <>
      Status, Doctor name, px name, overdue paymets, Roles
      <br />
      Filters:
      <br />
      <CalendarFilterStatusComponent />
      <br />
      <CalendarFilterRolesComponent />
      <br />
      <CalendarFilterDoctorSearchComponent />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarFilterComponent)
