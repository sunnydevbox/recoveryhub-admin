import React, { useEffect, useCallback } from 'react'
import { Input } from 'antd'
import { connect } from 'react-redux'
import './styles.scss'

const { Search } = Input

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filter => dispatch({ type: 'recordSchedules/SET_FILTER', payload: filter }),
  }
}

const CalendarFilterDoctorSearchComponent = ({ setFilter }) => {
  // const [value, setValue] = useState('');

  const setFil = useCallback(
    filter => {
      setFilter(filter)
    },
    [setFilter],
  )

  useEffect(() => {
    // if (firstLoad) {
    //   setFirstLoad(false)
    // }
  }, [])

  const onChange = e => {
    console.log(e)
    setFil({ drname: e })
    // setValue(e)
  }

  return (
    <>
      [Dr Search]
      <Search placeholder="input search text" onSearch={onChange} style={{ width: 200 }} />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarFilterDoctorSearchComponent)
