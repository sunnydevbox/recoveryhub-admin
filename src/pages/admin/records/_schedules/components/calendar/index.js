import React, { useCallback, useEffect, useState } from 'react'
import { Space, Row, Col, Spin } from 'antd'
import { connect } from 'react-redux'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import timeGridPlugin from '@fullcalendar/timegrid'
import moment from 'moment'
// import actions from 'redux/admin/records/schedules/actions'
import CalendarFiltersComponent from './filters'
import DrawerComponent from '../drawer'
import './styles.scss'

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: 'recordSchedules/TOGGLE_DRAWER' }),
    setEventSelected: id => dispatch({ type: 'recordSchedules/SELECT_EVENT', payload: { id } }),
    setFilter: filter => dispatch({ type: 'recordSchedules/SET_FILTER', payload: filter }),
  }
}

const CalendarComponent = ({
  recordSchedules: { listLoading, calendarEntries },
  toggleDrawer,
  setEventSelected,
  setFilter,
}) => {
  const calendarRef = React.createRef()
  const [firstLoad, setFirstLoad] = useState(true)

  const setFil = useCallback(
    filter => {
      setFilter(filter)
    },
    [setFilter],
  )

  const trig = useCallback(
    fetchInfo => {
      if (calendarRef && calendarRef.current) {
        let s = moment(
          calendarRef.current.getApi().currentDataManager.state.dateProfile.activeRange.start,
        ).format('YYYY-M-DD 0:0')
        let ee = moment(
          calendarRef.current.getApi().currentDataManager.state.dateProfile.activeRange.end,
        ).format('YYYY-M-DD 23:59')

        if (fetchInfo) {
          s = moment(fetchInfo.startStr).format('YYYY-M-DD 0:0')
          ee = moment(fetchInfo.endStr).format('YYYY-M-DD 23:59')
        }

        setFil({
          dateRange: `${s}|${ee}`,
          limit: 0,
        })
      }
    },
    [calendarRef, setFil],
  )

  useEffect(() => {
    if (firstLoad) {
      setFirstLoad(false)
      trig(null)
    }

    // console.log('setCalendarEntries(calendarEntries)', calendarEntries)

    //  console.log('entries', calendarEntries.constructor, calendarEntries)
    // if (typeof calendarEntries === 'object') {

    //   setEnt(Object.keys(calendarEntries))
    //   console.log(entries)
    // }
  }, [firstLoad, trig, calendarEntries])

  const eventClicked = info => {
    setEventSelected(info.event.extendedProps.data.id)
    toggleDrawer()
  }

  const eventContent = info => {
    const { data } = info.event.extendedProps
    let doc = ''
    let px = ''
    let time = ''

    if (data.doc) {
      doc = `Dr: ${data.doc.f} ${data.doc.l}`
    }

    if (data.px) {
      px = `PX: ${data.px.f} ${data.px.l}`
    }
    // console.log('eventContent',info)
    time = `${moment(data.start.t, 'YYYY-MM-DD HH:mm:ss').format('hh:mm A')} - ${moment(
      data.end.t,
      'YYYY-MM-DD HH:mm:ss',
    ).format('hh:mm A')}`
    // return info
    return {
      html: `
        <div class="event container">
          <div class="time">${time}</div>
          <div class="status">${data.s}</div>
          <div class="row">
            <div class="doc col-sm-6">
              <div class="caRd">${doc}<br/>${data.doc.r}</div>
            </div>
            <div class="px col-sm-6">
              <div class="caRd">${px}</div> 
            </div>
          </div>
        </div>
      `,
    }
  }

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Row>
          <Col span={12}>Displaying Results: {calendarEntries.length}</Col>
          <Col span={12} justify="end">
            Doctor Schedules
          </Col>
        </Row>
      </Space>

      {/* [{recordSchedules}] */}

      <Spin spinning={listLoading}>
        <CalendarFiltersComponent />

        <FullCalendar
          ref={calendarRef}
          events={Object.values(calendarEntries)}
          plugins={[dayGridPlugin, interactionPlugin, timeGridPlugin]}
          headerToolbar={{
            left: 'prev,next,today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek',
          }}
          initialView="dayGridMonth"
          eventClick={eventClicked}
          eventContent={eventContent}
          eventClassNames={arg => {
            return `status-${arg.event.extendedProps.data.s}`
          }}
          datesSet={trig}
        />
      </Spin>

      <DrawerComponent />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarComponent)
