import React, { useEffect } from 'react'
import { Drawer, Skeleton, Tabs } from 'antd'
import { connect } from 'react-redux'
import moment from 'moment'
import parse from 'html-react-parser'
import TabOrderTransactionsComponent from './tabs/order-transactions'
import TabActionsComponent from './tabs/actions'
import TabCallLogsComponent from './tabs/call-logs'
import './styles.scss'

const { TabPane } = Tabs

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: 'recordSchedules/TOGGLE_DRAWER' }),
  }
}

const DrawerComponent = ({
  recordSchedules: { drawerState, selectedEvent, loadingSelectedEvent },
  toggleDrawer,
}) => {
  // const [firstLoad, setFirstLoad] = useState(true)
  // const [drawerState, setDrawerState] = useState(false)

  const panes = [
    { title: 'Transaction History', content: <TabOrderTransactionsComponent />, key: '1' },
    { title: 'Call Log', content: <TabCallLogsComponent />, key: '2' },
    { title: 'Actions', content: <TabActionsComponent />, key: '3' },
    { title: 'Logs', content: 'Action Logs', key: '4' },
  ]

  useEffect(() => {
    // if (firstLoad) {
    //   setFirstLoad(false)
    // }

    if (drawerState && selectedEvent) {
      // console.log('DrawerComponent drawerState', drawerState, selectedEvent)
    }
  }, [drawerState, selectedEvent])

  const getDrawerTitle = () => {
    let title = <Skeleton.Button />
    // console.log(selectedEvent);
    if (!loadingSelectedEvent && selectedEvent) {
      const month = moment(selectedEvent)
        .startOf('month')
        .format('MMMM')
      const time = `<strong>${moment(selectedEvent.start_at.date).format('hh:mm A')} -${moment(
        selectedEvent.end_at.date,
      ).format('hh:mm A')}</strong>`

      title = parse(`${month} ${time}`)
    }
    return title
  }

  const getEventInfo = () => {
    let detail = <Skeleton />

    if (!loadingSelectedEvent && selectedEvent) {
      detail = parse(`Doctor: Dr. ${selectedEvent.doctor.first_name} ${selectedEvent.doctor.last_name}<br />
                Status: ${selectedEvent.status}<br />
                OT Inspector: <a target="_blank" href="https://tokbox.com/developer/tools/inspector/account/3521142/project/46190132/session/2_MX40NjE5MDEzMn5-MTYxOTU4ODk2MzYwMn54eFYxQ1NYYVd4QU1EVElJVWZTb2RpTTV-UH4">OPEN</a>
                `)
    }

    return detail
  }

  return (
    <>
      <Drawer
        title={getDrawerTitle()}
        placement="right"
        width="90%"
        closable={false}
        onClose={toggleDrawer}
        visible={drawerState}
      >
        {getEventInfo()}
        <div>
          <Tabs hideAdd type="editable-card">
            {panes.map(pane => (
              <TabPane tab={pane.title} key={pane.key}>
                {pane.content}
              </TabPane>
            ))}
          </Tabs>
        </div>
      </Drawer>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent)
