import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Timeline } from 'antd'
import './styles.scss'

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: 'recordSchedules/TOGGLE_DRAWER' }),
  }
}

const TabCallLogsComponent = ({ recordSchedules: { drawerState, selectedEvent } }) => {
  useEffect(() => {}, [drawerState, selectedEvent])

  return (
    <>
      <Timeline mode="left">
        <Timeline.Item label="2015-09-01">Create a services</Timeline.Item>
        <Timeline.Item label="2015-09-01 09:12:11">Solve initial network problems</Timeline.Item>
        <Timeline.Item>Technical testing</Timeline.Item>
        <Timeline.Item label="2015-09-01 09:12:11">Network problems being solved</Timeline.Item>
      </Timeline>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(TabCallLogsComponent)
