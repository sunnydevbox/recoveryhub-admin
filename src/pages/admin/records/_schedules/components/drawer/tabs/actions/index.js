import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Card, Row, Col, Space } from 'antd'
import ModifyTimeComponent from './modify-time/index'
import ReScheduleComponent from './re-schedule/index'
import './styles.scss'

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: 'recordSchedules/TOGGLE_DRAWER' }),
  }
}

const TabActionsComponent = ({ recordSchedules: { drawerState, selectedEvent } }) => {
  useEffect(() => {}, [drawerState, selectedEvent])

  return (
    <>
      <ul>
        <li>Move to another schedue of the same doctor</li>
        <li>Move to another schedue of a different doctor</li>
        <li>Delete this Schedule</li>
        <li>*Update to new time</li>
        <li>Trigger checking of transaction status</li>
        <li>Resend PDFs</li>
        <li>Regenerate PDFs</li>
      </ul>

      <Row gutter={16}>
        <Space>
          <Col span={3}>
            <Card title="Delete this schedule" style={{ width: 300 }}>
              <p>Card content</p>
            </Card>
          </Col>

          <Col span={3}>
            <Card title="Modify Time" style={{ width: 300 }}>
              <ModifyTimeComponent />
            </Card>
          </Col>

          <Col span={3}>
            <Card title="Re Schedule" style={{ width: 300 }}>
              <ReScheduleComponent />
            </Card>
          </Col>

          <Col span={3}>
            <Card title="Card title" style={{ width: 300 }}>
              <p>Card content</p>
              <p>Card content</p>
              <p>Card content</p>
            </Card>
          </Col>
        </Space>
      </Row>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(TabActionsComponent)
