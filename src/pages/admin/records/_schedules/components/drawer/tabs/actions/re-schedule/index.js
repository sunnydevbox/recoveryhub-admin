import React, { useState, useEffect } from 'react'
import { Form, Modal, Button, Select, notification, Spin } from 'antd'
import CalendarActionsService from 'services/admin/calendar/actions'
import { connect } from 'react-redux'
import moment from 'moment'

const { Option } = Select

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: 'recordSchedules/TOGGLE_DRAWER' }),
    getCalendarEntries: params =>
      dispatch({ type: 'recordSchedules/GET_CALENDAR_ENTRIES', payload: { params } }),
  }
}

const App = ({
  recordSchedules: { drawerState, selectedEvent },
  toggleDrawer,
  getCalendarEntries,
}) => {
  const [visible, setVisible] = useState(false)
  const [confirmLoading, setConfirmLoading] = useState(false)
  const [schedule, setSchedule] = useState([])
  // const [firstLoad, setFirstLoad] = useState(true)
  const [loading, setLoading] = useState(false)
  const [form] = Form.useForm()

  useEffect(() => {
    if (selectedEvent) {
      getSchedule()
    } else {
      form.setFieldsValue({
        new_id: null,
      })
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [drawerState, selectedEvent])

  const getSchedule = async () => {
    await CalendarActionsService.getSched().then(result => {
      const sched = result.map(d => ({
        id: d.id,
        startAt: moment(d.start_at.date).format('YYYY-MM-DD H:mm:ss'),
        endAt: moment(d.end_at.date).format('YYYY-MM-DD H:mm:ss'),
      }))
      setSchedule(sched)
    })
  }

  const showModal = () => {
    setVisible(true)
  }

  const handleOk = async () => {
    setLoading(true)
    try {
      const data = {
        current_id: selectedEvent.id,
        new_id: form.getFieldValue('new_id'),
      }

      await CalendarActionsService.postSched(data).then(() => {
        getCalendarEntries()
        toggleDrawer(false)
        notification.success({
          message: 'Success',
          description: 'Sucessfully Rescheduled',
        })
      })
      setConfirmLoading(true)
      setVisible(false)
      setConfirmLoading(false)
      setLoading(false)
    } catch (e) {
      throw e
    }
  }

  const handleCancel = () => {
    setVisible(false)
  }

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Re Schedule
      </Button>
      <Modal
        title="Schedule"
        visible={visible}
        width={400}
        style={{ top: 20 }}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        forceRender
      >
        <Spin spinning={loading}>
          <Form
            form={form}
            layout="vertical"
            scrollToFirstError="true"
            // onFinishFailed={onFinishFailed}
            // onFinish={onFinish}
          >
            <Form.Item label="Schedule" name="new_id" rules={[{ required: true }]}>
              <Select
                showSearch
                allowClear
                placeholder="Select Schedule"
                // optionFilterProp="children"
                // filterOption={(input, option) =>
                //   option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                // }
              >
                {schedule.map(({ id, startAt, endAt }) => (
                  <Option key={id} value={id}>
                    Start: {startAt} | End: {endAt}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
