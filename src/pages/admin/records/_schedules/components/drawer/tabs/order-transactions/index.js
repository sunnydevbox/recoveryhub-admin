import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import './styles.scss'

const mapStateToProps = ({ recordSchedules }) => ({ recordSchedules })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: 'recordSchedules/TOGGLE_DRAWER' }),
  }
}

const TabOrderTransactionsComponent = ({ recordSchedules: { drawerState, selectedEvent } }) => {
  useEffect(() => {}, [drawerState, selectedEvent])

  return <>List the transactions of this schedule</>
}

export default connect(mapStateToProps, mapDispatchToProps)(TabOrderTransactionsComponent)
