import React from 'react'
import { Helmet } from 'react-helmet'
import CalendarComponent from './components/calendar'

const Schedules = () => {
  return (
    <div>
      <Helmet title="Calendar" />

      <div className="kit__utils__heading">
        <h5>
          <span className="mr-3">Calendar</span>
        </h5>
      </div>
      <div className="card">
        <div className="card-body">
          <CalendarComponent />
        </div>
      </div>
    </div>
  )
}

export default Schedules
