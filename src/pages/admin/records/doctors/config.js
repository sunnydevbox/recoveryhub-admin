/* V1.0 */
import Service from 'services/admin/doctors'

export const LABEL_SINGULAR = 'Doctor'
export const LABEL_PLURAL = 'Doctors'
export const DRAWER_WIDTH = '80%'
export const STORE_PREFIX = 'featureDoctor'
export const API_SERVICE = Service
