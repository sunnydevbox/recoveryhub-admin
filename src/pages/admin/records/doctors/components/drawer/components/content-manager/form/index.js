/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import {
  Input,
  Form,
  Spin,
  DatePicker,
  // Checkbox,
  Switch,
  // Switch
  // Button,
} from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import moment from 'moment'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import FormActionButtonsComponent from './action-buttons'
import FormModalExitFormComponent from './modal-exit-form'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const FormItem = Form.Item
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    getList: data => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord, drawerState, formTriggerSave },
  commitRecord,
  setFormTouched,
  // toggleDrawer,
}) => {
  const [form] = Form.useForm()

  useEffect(() => {
    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      // populate form
      // console.log(selectedRecord)
      form.setFieldsValue({
        first_name: selectedRecord.first_name,
        last_name: selectedRecord.last_name,
        prefix: selectedRecord.general_information.prefix,
        suffix: selectedRecord.general_information.suffix,
        streetNo: selectedRecord.general_information.streetNo,
        streetAddress: selectedRecord.general_information.streetAddress,
        city: selectedRecord.general_information.city,
        state: selectedRecord.general_information.state,
        zipcode: selectedRecord.general_information.zipcode,
        phone: selectedRecord.general_information.phone,
        license_number: selectedRecord.general_information.license_number,
        license_expiration_date: moment(selectedRecord.general_informationlicense_expiration_date),
        s2_number: selectedRecord.general_information.s2_number,
        s2_expiration_date: selectedRecord.general_information.s2_expiration_date,
        prt_number: selectedRecord.general_information.prt_number,
        tin: selectedRecord.general_information.tin,
        medical_association: selectedRecord.general_information.medical_association,
        hospitalAffiliation: selectedRecord.general_information.hospitalAffiliation,
        life: selectedRecord.general_information.life,
        email: selectedRecord.email,
      })
    } else {
      form.setFieldsValue({
        first_name: form.getFieldValue('first_name'),
        last_name: form.getFieldValue('last_name'),
        prefix: form.getFieldValue('prefix'),
        suffix: form.getFieldValue('suffix'),
        streetNo: form.getFieldValue('streetNo'),
        streetAddress: form.getFieldValue('streetAddress'),
        city: form.getFieldValue('city'),
        state: form.getFieldValue('state'),
        zipcode: form.getFieldValue('zipcode'),
        phone: form.getFieldValue('phone'),
        license_number: form.getFieldValue('license_number'),
        license_expiration_date: form.getFieldValue('license_expiration_date'),
        s2_number: form.getFieldValue('s2_number'),
        s2_expiration_date: form.getFieldValue('s2_expiration_date'),
        prt_number: form.getFieldValue('prt_number'),
        tin: form.getFieldValue('tin'),
        medical_association: form.getFieldValue('medical_association'),
        hospitalAffiliation: form.getFieldValue('hospitalAffiliation'),
        life: form.getFieldValue('life'),
        email: form.getFieldValue('email'),
      })
    }

    // if (formTriggerSave) {
    //   console.log(form.getFieldValue('name'))
    //   form
    //       .validateFields()
    //       .then(values => {
    //         // form.resetFields();
    //         commitRecord(values)
    //       })
    //       .catch(info => {
    //         console.log('Validate Failed:', info);
    //       });
    // }
  }, [form, drawerState, selectedRecord, loadingSelectedRecord, formTriggerSave, commitRecord])

  const handleSave = async values => {
    commitRecord(values)
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        onFinish={handleSave}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <strong>Personal Data</strong>
              <hr />
              <div className="row">
                <div className="col-lg-3">
                  <FormItem label="First Name" name="first_name" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-lg-3">
                  <FormItem label="Last Name" name="last_name" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-lg-3">
                  <FormItem label="Prefix" name="prefix" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
                <div className="col-lg-3">
                  <FormItem label="Suffix" name="suffix" rules={[{ required: true }]}>
                    <Input />
                  </FormItem>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <strong>Clinic Information</strong>
              <hr />

              <div className="row">
                <div className="col-lg-6">
                  <FormItem label="Street No." name="streetNo">
                    <Input />
                  </FormItem>
                </div>
                <div className="col-lg-6">
                  <FormItem label="Street Address" name="streetAddress">
                    <Input />
                  </FormItem>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-3">
                  <FormItem label="City" name="city">
                    <Input />
                  </FormItem>
                </div>

                <div className="col-lg-3">
                  <FormItem label="State" name="state">
                    <Input />
                  </FormItem>
                </div>

                <div className="col-lg-3">
                  <FormItem label="Zipcode" name="zipcode">
                    <Input />
                  </FormItem>
                </div>

                <div className="col-lg-3">
                  <FormItem label="Phone No." name="phone">
                    <Input />
                  </FormItem>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <strong>Professional Information </strong>
              <hr />

              <div className="form-group">
                <div className="row">
                  <div className="col-lg-4">
                    <FormItem label="License Number" name="license_number">
                      <Input />
                    </FormItem>
                  </div>
                  <div className="col-lg-4">
                    <FormItem label="License Expiration Date" name="license_expiration_date">
                      <DatePicker style={{ width: 345 }} format="MM-DD-YYYY" />
                    </FormItem>
                  </div>
                  <div className="col-lg-4">
                    <FormItem label="S2 Number" name="s2_number">
                      <Input />
                    </FormItem>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-4">
                    <FormItem label="S2 Expiration Date" name="s2_expiration_date">
                      <Input />
                    </FormItem>
                  </div>
                  <div className="col-lg-4">
                    <FormItem label="PTR Number" name="prt_number">
                      <Input />
                    </FormItem>
                  </div>
                  <div className="col-lg-4">
                    <FormItem label="TIN" name="tin">
                      <Input />
                    </FormItem>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-4">
                    <FormItem label="Medical Association" name="medical_association">
                      <Input />
                    </FormItem>
                  </div>
                  <div className="col-lg-4">
                    <FormItem label="Hospital Affiliation" name="hospitalAffiliation">
                      <Input />
                    </FormItem>
                  </div>
                  <div className="col-lg-4">
                    <FormItem label="Sub Specialty" name="sub_specialty">
                      <Input />
                    </FormItem>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-4">
                    <FormItem label="Email Address" name="email" rules={[{ required: true }]}>
                      <Input />
                    </FormItem>
                  </div>
                  <div className="col-lg-4">
                    <FormItem label="Life" name="life" valuePropName="checked">
                      <Switch
                        checkedChildren={<CheckOutlined />}
                        unCheckedChildren={<CloseOutlined />}
                      />
                    </FormItem>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container footera ant-drawer-footer text-center px-0 py-2">
          <div className="row">
            <div className="col-sm-12">
              <FormActionButtonsComponent />
            </div>
          </div>
        </div>
      </Form>

      <FormModalExitFormComponent />
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
