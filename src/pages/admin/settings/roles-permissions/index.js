import React, { useState, useEffect } from 'react'
import { Helmet } from 'react-helmet'
import { Tabs } from 'antd'
import DrawerComponent from './components/drawer'
import BrowseRoles from './components/browse-roles'
import BrowsePermissions from './components/browse-permissions'

const { TabPane } = Tabs

const Patients = () => {
  const [drawerVisibility, setDrawerVisibility] = useState(false)
  const [drawerType, setDrawerType] = useState(null)
  const [drawerRecordId, setDrawerRecordId] = useState(null)

  useEffect(() => {}, [drawerVisibility])

  return (
    <div>
      <Helmet title="Roles & Permissions" />

      <div className="kit__utils__heading">
        <h5>
          <span className="mr-3">Roles &amp; Permissions</span>
        </h5>
      </div>
      <div className="card">
        <div className="card-body">
          <Tabs defaultActiveKey="1" type="card" size="default">
            <TabPane tab="Roles" key="1">
              <BrowseRoles
                drawerVisibilityChanged={(type, id, visibility) => {
                  setDrawerVisibility(visibility)
                  setDrawerRecordId(id)
                  setDrawerType(type)
                }}
              />
            </TabPane>
            <TabPane tab="Permissions" key="2">
              <BrowsePermissions />
            </TabPane>
          </Tabs>
        </div>
      </div>

      <DrawerComponent
        type={drawerType}
        recordId={drawerRecordId}
        visibility={drawerVisibility}
        drawerVisibilityChanged={visibility => setDrawerVisibility(visibility)}
      />
    </div>
  )
}

export default Patients
