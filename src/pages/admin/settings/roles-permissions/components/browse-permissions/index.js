import React, { useState, useEffect } from 'react'
import BrowseComponent from 'components/bread/browse'
import { Space, Button, message, Row, Col } from 'antd'
import PermissionService from 'services/admin/permissions'
import { SyncOutlined } from '@ant-design/icons'
import DrawerComponent from '../drawer'
import FiltersComponent from '../filters'
import SearchbarComponent from '../searchbar'

const Browse = () => {
  const [dataSource, setDataSource] = useState([])
  const [loading, setLoading] = useState(false)
  const [queryParams, setQueryParams] = useState({})
  const [searchLoading, setSearchLoading] = useState(false)
  const [drawerVisibility, setDrawerVisibility] = useState(false)
  const [pagination, setPagination] = useState({
    total: 0,
    pageSize: 3,
  })

  const getList = async () => {
    setLoading(true)

    if (queryParams.search) {
      setSearchLoading(true)
    }

    await PermissionService.browse(queryParams)
      .then(result => {
        setLoading(false)
        setSearchLoading(false)
        setDataSource(result.data)
        setPagination({
          total: result.meta.pagination.total,
          current: result.meta.pagination.current_page,
          pageSize: result.meta.pagination.per_page,
        })
      })
      .catch(() => {
        setLoading(false)
        setSearchLoading(false)

        message.error('There was a problem loading the list')
      })
  }

  useEffect(() => {
    getList()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [queryParams])

  const tableChanged = (page, filters, sorter, extra) => {
    // console.log('(page, filters, sorter, extra)', page, filters, sorter, extra)
    // console.log('(page, filters, sorter, extra)', sorter)

    if (Object.keys(filters).length) {
      console.log('filters', filters)
    }

    if (Object.keys(extra).length) {
      console.log('extra', extra)
    }

    delete queryParams.orderBy
    delete queryParams.sortBy
    if (sorter.column) {
      queryParams.orderBy = sorter.field
      queryParams.sortBy = sorter.order === 'ascend' ? 'asc' : 'desc'
    }

    setQueryParams({
      ...queryParams,
      page: page.current,
      limit: page.pageSize,
    })

    setPagination({
      pageSize: page.pageSize,
    })
  }

  const onSearch = str => {
    delete queryParams.search
    queryParams.page = 1 // Always reset to page when searching

    if (str && str.length) {
      queryParams.search = str
    }

    setQueryParams({
      ...queryParams,
      page: 1,
    })
  }

  const filterChanged = filters => {
    delete queryParams.filters
    queryParams.page = 1 // Always reset to page when searching

    setQueryParams({
      ...queryParams,
      filters,
    })
  }

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Space>
          <SearchbarComponent onSearch={onSearch} searchLoading={searchLoading} />
          <FiltersComponent filterChanged={filterChanged} />
        </Space>
        <Row>
          <Col span={12}>Total Results: {pagination.total}</Col>
          <Col span={12} justify="end">
            <Button
              type="link"
              block
              icon={
                <SyncOutlined
                  onClick={getList}
                  spin={loading}
                  style={{
                    width: 'auto',
                    float: 'right',
                  }}
                />
              }
              disabled={loading}
            />
          </Col>
        </Row>

        <BrowseComponent
          rowKey={record => record.id}
          loading={loading}
          pagination={pagination}
          columns={[
            {
              title: 'Name',
              dataIndex: 'name',
              key: 'name',
              defaultSortOrder: 'descend',
              sorter: (a, b) => a.age - b.age,
            },

            {
              title: 'Action',
              key: 'action',
              render: (text, record) => (
                <Space size="middle">
                  <Button
                    type="primary"
                    size="small"
                    onClick={() => {
                      setDrawerVisibility(true)
                      console.log(record.id)
                    }}
                    danger
                  >
                    Edit
                  </Button>
                  <Button
                    type="primary"
                    danger
                    size="small"
                    onClick={() => {
                      console.log(record.id)
                    }}
                  >
                    Delete
                  </Button>
                </Space>
              ),
            },
          ]}
          dataSource={dataSource}
          tableChanged={tableChanged}
        />
      </Space>

      <DrawerComponent
        drawerVisibility={drawerVisibility}
        drawerVisibilityChanged={visibility => setDrawerVisibility(visibility)}
      />
    </>
  )
}

export default Browse
