import React, { useState, useEffect } from 'react' // useState
import { Form, Input, message, Spin } from 'antd'
import RoleService from 'services/admin/roles'

const FormComponent = props => {
  const { id, triggeredSave, resetTriggeredSave } = props
  const [loading, setLoading] = useState(false)
  const [form] = Form.useForm()

  useEffect(() => {
    // console.log('triggeredSave', triggeredSave, id)
    if (triggeredSave) {
      doSave()
    }

    if (id) {
      pullRecord()
    } else {
      form.setFieldsValue({ name: null })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, triggeredSave])

  const pullRecord = async () => {
    setLoading(true)

    await RoleService.show(id)
      .then(result => {
        form.setFieldsValue({ name: result.data.name })
        setLoading(false)
      })
      .catch(() => {
        setLoading(false)
        message.error('There was a problem loading the list')
      })
  }

  const doSave = async () => {
    setLoading(true)

    if (id) {
      await RoleService.edit(id, form.getFieldsValue())
        .then(() => {
          setLoading(false)
          resetTriggeredSave()
        })
        .catch(() => {
          setLoading(false)
          resetTriggeredSave()
          message.error('There was a problem loading the list')
        })
    } else {
      await RoleService.add(form.getFieldsValue())
        .then(() => {
          setLoading(false)
          resetTriggeredSave()
        })
        .catch(() => {
          setLoading(false)
          resetTriggeredSave()
          message.error('There was a problem loading the list')
        })
    }

    resetTriggeredSave()
  }

  return (
    <Spin spinning={loading}>
      <Form
        id="editform"
        form={form}
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 14 }}
        layout="horizontal"
      >
        <Form.Item label="Role Name" name="name" rules={[{ required: true, min: 3 }]}>
          <Input />
        </Form.Item>
      </Form>
    </Spin>
  )
}

export default FormComponent
