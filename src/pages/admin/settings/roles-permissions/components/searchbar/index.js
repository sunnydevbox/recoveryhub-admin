import React from 'react' // , { useState, useEffect }
import { Input } from 'antd'

const { Search } = Input

function SearchbarComponent({ onSearch, searchLoading }) {
  // const [state, setState] = useState('');

  // useEffect(() => {
  // return () => {

  //     }
  // }, []);

  return (
    <>
      <Search
        placeholder="ID / Name / Email"
        onSearch={onSearch}
        style={{ width: 200 }}
        allowClear
        loading={searchLoading}
      />
    </>
  )
}

export default SearchbarComponent
