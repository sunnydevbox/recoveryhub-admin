import React, { useEffect, useState } from 'react'
import { Button } from 'antd'
import DrawerComponent from 'components/bread/drawer'
import FormRoleComponent from '../form-role'
import FormPermissionComponent from '../form-permission'

function Drawer(props) {
  const { drawerVisibilityChanged, visibility, type, recordId } = props

  // const [drawerVisibility, setDrawerVisibility] = useState(false)
  const [triggeredSave, setTriggeredSave] = useState(false)

  useEffect(() => {
    // setDrawerVisibility(visibility)
  }, [props, visibility, recordId])

  const getTitle = () => {
    switch (type) {
      case 'edit_role':
        return 'Edit Role'
      case 'edit_permission':
        return 'Edit Permission'
      case 'new_role':
        return 'New Role'
      default:
      case 'new_permission':
        return 'New Permission'
    }
  }

  const resetTriggeredSave = () => {
    console.log('resetting')
    setTriggeredSave(false)
  }

  const getForm = () => {
    return type === 'edit_role' || type === 'new_role' ? (
      <FormRoleComponent
        id={recordId}
        triggeredSave={triggeredSave}
        resetTriggeredSave={resetTriggeredSave}
      />
    ) : (
      <FormPermissionComponent
        id={recordId}
        triggeredSave={triggeredSave}
        resetTriggeredSave={resetTriggeredSave}
      />
    )
  }

  return (
    <>
      <DrawerComponent
        title={getTitle()}
        visibility={visibility}
        drawerVisibilityChanged={v => {
          // setDrawerVisibility(v)
          drawerVisibilityChanged(v)
        }}
        content={getForm()}
        footer={
          <div
            style={{
              textAlign: 'right',
            }}
          >
            <Button
              onClick={() => {
                // setDrawerVisibility(false)
                drawerVisibilityChanged(false)
              }}
              disabled={triggeredSave}
              style={{ marginRight: 8 }}
            >
              Cancel
            </Button>
            <Button
              onClick={() => setTriggeredSave(true)}
              form="editform"
              type="primary"
              disabled={triggeredSave}
            >
              Save
            </Button>
          </div>
        }
      />
    </>
  )
}

export default Drawer
