import React, { useState, useEffect } from 'react'
import { Select, Space } from 'antd'

const { Option } = Select

function FiltersComponent({ filterChanged }) {
  const [status, setStatus] = useState(null)
  const [verified, setVerified] = useState(null)
  const [deleted, setDeleted] = useState(null)

  useEffect(() => {
    const filters = {}

    if (status && status !== 'all' && status.length) {
      filters.status = status
    }

    if (verified && verified !== 'all' && verified.length) {
      filters.verified = verified
    }

    if (deleted && deleted !== 'all' && deleted.length) {
      filters.deleted = deleted
    }

    const q = []
    // console.log('filters', filters, status, verified, deleted)
    Object.keys(filters).forEach(filter => {
      q.push(`${filter}:${filters[filter]}`)
    })

    filterChanged(q.join(';'))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status, verified, deleted])

  const clearFilters = () => {
    setStatus(null)
    setVerified(null)
    setDeleted(null)
  }

  return (
    <Space>
      <Select placeholder="Select Status" onChange={v => setStatus(v)} value={status}>
        <Option value="all">All</Option>
        <Option value="active">Active</Option>
        <Option value="inactive">Inactive</Option>
      </Select>

      <Select placeholder="Is Verified?" onChange={v => setVerified(v)} value={verified}>
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <Select placeholder="Is Deleted?" onChange={v => setDeleted(v)} value={deleted}>
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <a role="button" tabIndex={0} onClick={clearFilters} onKeyDown={clearFilters}>
        Clear Filters
      </a>
    </Space>
  )
}

export default FiltersComponent
