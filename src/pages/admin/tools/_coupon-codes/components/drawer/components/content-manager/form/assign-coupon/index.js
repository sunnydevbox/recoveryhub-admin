import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Form, Modal, Button, notification, Spin, Select } from 'antd'
import DoctorListServices from 'services/admin/doctors/index'
import CouponAssignService from 'services/admin/coupon-codes/actions'
import { STORE_PREFIX } from '../../../../../../config'
// import SmartSearchComponent from 'components/smart-search/index'

// import moment from 'moment'

const { Option } = Select

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    formTriggerSave: () => dispatch({ type: `${STORE_PREFIX}/FORM_TRIGGER_SAVE`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
  }
}

const AssignCoupon = ({ [STORE_PREFIX]: { selectedRecord }, toggleDrawer }) => {
  const [visible, setVisible] = useState(false)
  const [confirmLoading, setConfirmLoading] = useState(false)
  const [loading, setLoading] = useState(false)
  const [form] = Form.useForm()
  const [doctors, setDoctors] = useState([])
  const [queryParams, setQueryParams] = useState({ limit: 0 })

  useEffect(() => {
    const getDoctors = async () => {
      await DoctorListServices.browse(queryParams).then(result => {
        const doctor = result.data.map(d => ({
          id: d.id,
          name: d.name,
        }))
        setQueryParams(queryParams)
        console.log(doctor)
        setDoctors(doctor)
      })
    }

    getDoctors()
    console.log(selectedRecord.id)
  }, [selectedRecord, queryParams])

  const showModal = () => {
    setVisible(true)
  }

  const handleOk = async () => {
    setLoading(true)
    try {
      const data = {
        user_id: form.getFieldValue('doctor_id'),
        coupon_id: selectedRecord.id,
      }
      await CouponAssignService.add(data).then(() => {
        notification.success({
          message: 'Success',
          description: 'Sucessfully Assign Coupon',
        })
      })
      setConfirmLoading(true)
      setVisible(false)
      setConfirmLoading(false)
      setLoading(false)
      toggleDrawer(false)
    } catch (e) {
      throw e
    }
  }

  const handleCancel = () => {
    setVisible(false)
  }

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Assign Coupon
      </Button>
      <br />
      <br />
      <div>
        <h3>
          {selectedRecord.assignCoupons.map(texts => (
            <li key={texts.id}>
              Dr. {texts.first_name} {texts.last_name}
            </li>
          ))}
        </h3>
      </div>
      <Modal
        title="Assign Coupon"
        visible={visible}
        width={500}
        style={{ top: 20 }}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        forceRender
      >
        <Spin spinning={loading}>
          <Form
            form={form}
            layout="vertical"
            scrollToFirstError="true"
            // onFinishFailed={onFinishFailed}
            // onFinish={onFinish}
          >
            <Form.Item label="Doctors" name="doctor_id" rules={[{ required: true }]}>
              <Select
                allowClear
                showSearch
                placeholder="Select Doctors"
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {doctors.map(({ id, name }) => (
                  <Option key={id} value={id}>
                    {name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(AssignCoupon)
