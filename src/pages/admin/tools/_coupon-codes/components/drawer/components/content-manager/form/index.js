/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import {
  Input,
  Form,
  Spin,
  Select,
  DatePicker,
  Switch,
  // InputNumber,
  // Button,
} from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import { connect } from 'react-redux'
import moment from 'moment'
import FormActionButtonsComponent from './action-buttons'
import FormModalExitFormComponent from './modal-exit-form'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const FormItem = Form.Item
const { TextArea } = Input
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    getList: data => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord, drawerState, formTriggerSave },
  commitRecord,
  setFormTouched,
  // toggleDrawer,
}) => {
  const [form] = Form.useForm()
  console.log(selectedRecord)
  useEffect(() => {
    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      // populate form
      // console.log(selectedRecord)
      const expiresAt = selectedRecord.expires_at
      form.setFieldsValue({
        code: selectedRecord.code,
        name: selectedRecord.name,
        description: selectedRecord.description,
        value: selectedRecord.value,
        discount: selectedRecord.discount,
        item_type: selectedRecord.item_type,
        expires_at: expiresAt ? moment.utc(expiresAt.date) : null,
        usage_limit: selectedRecord.usage_limit,
        active: selectedRecord.active,
      })
    } else {
      form.setFieldsValue({
        code: form.getFieldValue('code'),
        name: form.getFieldValue('name'),
        description: form.getFieldValue('description'),
        value: form.getFieldValue('value'),
        discount: form.getFieldValue('discount'),
        item_type: form.getFieldValue('item_type'),
        usage_limit: form.getFieldValue('usage_limit'),
        expires_at: form.getFieldValue('expires_at'),
        active: form.getFieldValue('active'),
      })
    }

    // if (formTriggerSave) {
    //   console.log(form.getFieldValue('name'))
    //   form
    //       .validateFields()
    //       .then(values => {
    //         // form.resetFields();
    //         commitRecord(values)
    //       })
    //       .catch(info => {
    //         console.log('Validate Failed:', info);
    //       });
    // }
  }, [form, drawerState, selectedRecord, loadingSelectedRecord, formTriggerSave, commitRecord])

  const handleSave = async values => {
    commitRecord(values)
  }

  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf('day')
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        form={form}
        layout="vertical"
        onFinish={handleSave}
        // labelCol={{ span: 3 }}
        // wrapperCol={{ span: 8 }}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="container">
          <FormItem label="Coupon Code" name="code" rules={[{ required: true }]}>
            <Input placeholder="Coupon Code" />
          </FormItem>

          <FormItem label="Coupon Name" name="name" rules={[{ required: true }]}>
            <Input placeholder="Coupon Name" />
          </FormItem>

          <FormItem label="Description" name="description" rules={[{ required: true }]}>
            <TextArea rows={5} placeholder="Description" />
          </FormItem>

          <div className="row">
            <div className="col-lg-6">
              <FormItem label="Value" name="value">
                <Input placeholder="Value" min={0} />
              </FormItem>
            </div>

            <div className="col-lg-6">
              <FormItem label="Discount" name="discount">
                <Input placeholder="Discount" min={0} max={1} />
              </FormItem>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-6">
              <FormItem label="Applies To" name="item_type">
                <Select
                  placeholder="Item Type"
                  options={[
                    {
                      value: 'both',
                      label: 'All',
                    },
                    {
                      value: 'appointment',
                      label: 'Appointments',
                    },
                    {
                      value: 'medicine',
                      label: 'Medicines',
                    },
                  ]}
                />
              </FormItem>
            </div>

            <div className="col-lg-6">
              <FormItem label="Usage Limit" name="usage_limit" rules={[{ required: true }]}>
                <Input placeholder="Usage Limit" />
              </FormItem>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-6">
              <FormItem name="expires_at" label="Expiration">
                <DatePicker
                  format="YYYY-MM-DD HH:mm:ss"
                  disabledDate={disabledDate}
                  showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
                />
              </FormItem>
            </div>
            <div className="col-lg-6">
              <FormItem name="active" label="Set to Active?" valuePropName="checked">
                <Switch checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} />
              </FormItem>
            </div>
          </div>

          <div className="container footera ant-drawer-footer text-center px-0 py-2">
            <div className="row">
              <div className="col-sm-12">
                <FormActionButtonsComponent />
              </div>
            </div>
          </div>
        </div>
      </Form>
      <FormModalExitFormComponent />
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
