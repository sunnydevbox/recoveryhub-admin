import React, { useEffect } from 'react'
import { Skeleton, Tabs } from 'antd'
import { Table } from 'reactstrap'
import { connect } from 'react-redux'
import moment from 'moment'
import AssignCouponComponent from '../form/assign-coupon/index'
import { STORE_PREFIX } from '../../../../../config'

const { TabPane } = Tabs
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    formTriggerSave: () => dispatch({ type: `${STORE_PREFIX}/FORM_TRIGGER_SAVE`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
  }
}

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  // const [cancelConfirmVisible, setCancelConfirmVisible] = useState(false)

  useEffect(() => {
    console.log(selectedRecord)
  }, [selectedRecord])

  // const renderField = (label, value) => {
  //   return (
  //     <div className="field mt-4">
  //       {value && value.length ? (
  //         <div className="pl-1">{value}</div>
  //       ) : (
  //         <em className="pl-1 text-muted">Not specified</em>
  //       )}
  //       <hr className="m-0" />
  //       <span className="pl-1 text-muted">{label}</span>
  //     </div>
  //   )
  // }
  //  const expiresAt = selectedRecord.expires_at

  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <Tabs defaultActiveKey="1" type="card">
              <TabPane tab="Information" key="1">
                <Table responsive>
                  <tbody>
                    <tr>
                      <td>Name:</td>
                      <td>{selectedRecord.name}</td>
                    </tr>
                    <tr>
                      <td>Description:</td>
                      <td>{selectedRecord.description}</td>
                    </tr>
                    <tr>
                      <td>Value:</td>
                      <td>{selectedRecord.value}</td>
                    </tr>
                    <tr>
                      <td>Discount:</td>
                      <td>{selectedRecord.discount}</td>
                    </tr>
                    <tr>
                      <td>Item Type:</td>
                      <td>{selectedRecord.item_type}</td>
                    </tr>
                    <tr>
                      <td>Usage Limit:</td>
                      <td>{selectedRecord.usage_limit}</td>
                    </tr>
                    <tr>
                      <td>Expiration:</td>
                      <td>
                        {selectedRecord.expires_at
                          ? moment(selectedRecord.expires_at.date).format('YYYY-MM-DD HH:mm:ss')
                          : 'No'}
                      </td>
                    </tr>
                    <tr>
                      <td>Active:</td>
                      <td>{selectedRecord.active ? 'Yes' : 'No'}</td>
                    </tr>
                  </tbody>
                </Table>
              </TabPane>
              <TabPane tab="Assign Coupon" key="2">
                <div className="pt-3">
                  <AssignCouponComponent />
                </div>
              </TabPane>
            </Tabs>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewRecordDetailsComponent)
