/* V1.0 */
import Service from 'services/admin/coupon-codes'

export const LABEL_SINGULAR = 'Coupon Code'
export const LABEL_PLURAL = 'Coupon Codes'
export const DRAWER_WIDTH = '45%'
export const STORE_PREFIX = 'featureCoupon'
export const API_SERVICE = Service
