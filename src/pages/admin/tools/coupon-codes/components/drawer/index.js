import React, { useEffect, useState } from 'react'
import { Button } from 'antd'
import DrawerComponent from 'components/bread/drawer'
import CouponCodeComponent from '../form'

function Drawer(props) {
  const {
    drawerVisibilityChanged,
    visibility,
    type,
    recordId,
    setReloadListState,
    toggleDrawer,
  } = props

  const [triggeredSave, setTriggeredSave] = useState(false)

  useEffect(() => {}, [props, visibility, recordId])

  const getTitle = () => {
    switch (type) {
      case 'edit_coupon':
        return 'Edit Coupon'
      default:
      case 'new_coupon':
        return 'New Coupon'
    }
  }

  const resetTriggeredSave = () => {
    setTriggeredSave(false)
  }

  const getForm = () => {
    return (
      <CouponCodeComponent
        id={recordId}
        triggeredSave={triggeredSave}
        setReloadListState={setReloadListState}
        toggleDrawer={toggleDrawer}
        resetTriggeredSave={resetTriggeredSave}
        drawerVisibilityChanged={v => {
          drawerVisibilityChanged(v)
        }}
      />
    )
  }
  return (
    <>
      <DrawerComponent
        title={getTitle()}
        visibility={visibility}
        drawerVisibilityChanged={v => {
          toggleDrawer(v)
          // drawerVisibilityChanged(v)
        }}
        content={getForm()}
        footer={
          <div
            style={{
              textAlign: 'right',
            }}
          >
            <Button
              onClick={() => {
                toggleDrawer(false)
                // drawerVisibilityChanged(false)
              }}
              disabled={triggeredSave}
              style={{ marginRight: 8 }}
            >
              Cancel
            </Button>
            <Button
              onClick={() => setTriggeredSave(true)}
              form="editform"
              type="primary"
              disabled={triggeredSave}
            >
              Save
            </Button>
          </div>
        }
      />
    </>
  )
}

export default Drawer
