import React, { useState, useEffect } from 'react'
import BrowseComponent from 'components/bread/browse'
import { Space, Button, Row, Col } from 'antd'
import { SyncOutlined, CheckCircleOutlined, ExclamationCircleOutlined } from '@ant-design/icons'
import { connect } from 'react-redux'
import moment from 'moment'
import FiltersComponent from '../filters'
import SearchbarComponent from '../searchbar'
// import FormRole from '../form-role'

const mapStateToProps = ({ couponCodes }) => ({ couponCodes })

const Browse = ({
  drawerVisibilityChanged,
  setReloadListState,
  dispatch,
  couponCodes: { reloadListState, dataSource, pagination, listLoading, queryParams },
}) => {
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(() => {
    console.log('reloadListState', reloadListState)
    if (firstLoad || reloadListState) {
      setFirstLoad(false)
      getList()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [reloadListState, firstLoad])

  const getList = async () => {
    dispatch({
      type: 'toolCouponCode/SET_BROWSE',
    })
  }

  const tableChanged = (page, filters, sorter, extra) => {
    queryParams.page = page
    queryParams.sorter = sorter

    console.log('(page, filters, sorter, extra)', page, filters, sorter, extra)
    // console.log('(page, filters, sorter, extra)', sorter)

    dispatch({
      type: 'toolCouponCode/SET_QUERY_PARAMS',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Space>
          <SearchbarComponent />
          <FiltersComponent />
        </Space>
        <Row>
          <Col span={12}>Total Results: {pagination.total}</Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Space>
              <Button
                type="button"
                disabled={reloadListState}
                onClick={() => drawerVisibilityChanged('new_coupon', null, true)}
              >
                Add Coupon Code
              </Button>
              <Button
                type="link"
                icon={
                  <SyncOutlined
                    onClick={() => setReloadListState(true)}
                    spin={reloadListState}
                    style={{
                      width: 'auto',
                      float: 'right',
                    }}
                  />
                }
                disabled={reloadListState}
              />
            </Space>
          </Col>
        </Row>

        <BrowseComponent
          rowKey={record => record.id}
          loading={reloadListState || listLoading}
          pagination={pagination}
          columns={[
            {
              title: 'Code',
              dataIndex: 'code',
              key: 'code',
              defaultSortOrder: 'descend',
              sorter: (a, b) => a.age - b.age,
            },
            {
              title: 'Name',
              dataIndex: 'name',
              key: 'name',
              defaultSortOrder: 'descend',
              sorter: (a, b) => a.age - b.age,
            },

            {
              title: 'Description',
              dataIndex: 'description',
              key: 'description',
            },

            {
              title: 'Value',
              dataIndex: 'value',
              key: 'value',
            },

            {
              title: 'Discount',
              dataIndex: 'discount',
              key: 'discount',
            },

            {
              title: 'Usage Limit',
              dataIndex: 'usage_limit',
              key: 'usageLimit',
            },

            {
              title: 'Active',
              dataIndex: 'active',
              key: 'active',
              render: active => (
                <>
                  {active ? (
                    <CheckCircleOutlined style={{ color: 'rgb(55 160 3)' }} />
                  ) : (
                    <ExclamationCircleOutlined style={{ color: 'rgb(212 3 3)' }} />
                  )}
                </>
              ),
            },

            {
              title: 'Expiration',
              dataIndex: 'expires_at',
              key: 'expires_at',
              render: expiresAt => {
                return !expiresAt ? 'No' : moment(expiresAt.date).format('YYYY-MM-DD HH:mm:ss')
              },
            },

            {
              title: 'Action',
              key: 'action',
              render: (text, record) => (
                <Space size="middle">
                  <Button
                    type="primary"
                    size="small"
                    onClick={() => drawerVisibilityChanged('edit_coupon', record.id, true)}
                    danger
                  >
                    Edit
                  </Button>
                  <Button
                    type="primary"
                    danger
                    size="small"
                    onClick={() => {
                      console.log(record.id)
                    }}
                  >
                    Delete
                  </Button>
                </Space>
              ),
            },
          ]}
          dataSource={dataSource}
          tableChanged={tableChanged}
        />
      </Space>
    </>
  )
}

export default connect(mapStateToProps)(Browse)
