import React, { useEffect } from 'react'
import { Select, Space } from 'antd'
import { connect } from 'react-redux'

const { Option } = Select

const mapStateToProps = ({ couponCodes }) => ({ couponCodes })

const FiltersComponent = ({ dispatch, couponCodes: { queryParams } }) => {
  useEffect(() => {}, [])
  const filterChanged = (key, value) => {
    if (key === null || value === 'all') {
      queryParams.filters = {}
    } else {
      queryParams.filters[key] = value
    }

    console.log('fileCHanged', key, value, queryParams.filters)

    dispatch({
      type: 'toolCouponCode/CLEAR_FILTERS',
      payload: {
        queryParams,
        key,
      },
    })
  }

  return (
    <Space>
      <Select
        placeholder="Is Active?"
        onChange={v => filterChanged('active', v)}
        value={queryParams.filters.active}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <a
        role="button"
        tabIndex={0}
        onClick={() => filterChanged(null)}
        onKeyDown={() => filterChanged(null)}
      >
        Clear Filters
      </a>
    </Space>
  )
}

export default connect(mapStateToProps)(FiltersComponent)
