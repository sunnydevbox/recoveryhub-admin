import React from 'react' // , { useState, useEffect }
import { Input } from 'antd'
import { connect } from 'react-redux'

const { Search } = Input

const mapStateToProps = ({ couponCodes }) => ({ couponCodes })

const SearchbarComponent = ({ dispatch, couponCodes: { queryParams, searchLoading } }) => {
  const onSearch = str => {
    queryParams.searchTerm = str

    if (queryParams.page) {
      queryParams.page.current = 1
    }

    dispatch({
      type: 'toolCouponCode/SET_SEARCH_TERM',
      payload: {
        queryParams,
      },
    })
  }

  return (
    <>
      <Search
        placeholder="Code / Name / Description / SKU"
        onSearch={onSearch}
        style={{ width: 200 }}
        allowClear
        loading={searchLoading}
      />
    </>
  )
}

export default connect(mapStateToProps)(SearchbarComponent)
