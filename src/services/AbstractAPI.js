/* eslint-disable prefer-template */
/* eslint-disable no-restricted-syntax */
import axios from 'axios'
import store from 'store'
import { notification } from 'antd'

// define the api
const axiosAPI = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  timeout: 10000,
  headers: {
    Accept: process.env.REACT_APP_API_HEADER_ACCEPT,
    'Content-Type': process.env.REACT_APP_API_HEADER_CONTENT_TYPE,
  },
})

/*
 * INTERCEPTORS
 */
axiosAPI.interceptors.request.use(
  async config => {
    const token = await store.get('accessToken')

    if (token) {
      config.headers.authorization = `Bearer ${token}`
    }

    return config
  },
  error => {
    console.log(error)

    // Do something with request error
    return Promise.reject(error)
  },
)

axiosAPI.interceptors.response.use(
  response => {
    // console.log('axiosAPI.interceptors.response', response)
    // Do something with response data
    return response
  },
  error => {
    let message = 'There was problem with the system. Please try again later.'

    // console.log('**** ERRIOR', error)

    if (!error.response) {
      message = 'Network failure. Please check your internet connection.'
    } else {
      switch (error.response.status) {
        case 400:
          message = ''
          if (typeof error.response.data.message === 'object') {
            for (const m in error.response.data.message) {
              if (typeof error.response.data.message[m] === 'object') {
                for (const mm in error.response.data.message[m]) {
                  if (mm) {
                    message += (message.length ? '\n' : '') + error.response.data.message[m][mm]
                  }
                }
              }
            }
          } else {
            // const { message } = error.response.data;
            // message = error.response.data.message
          }
          break

        case 401:
          if (error.response.data.error === 'invalid_credentials') {
            message = 'Invalid email or password'
            notification.warning({
              message: 'Permission denied',
              description: 'Email or password is invalid!',
            })
          } else {
            // Redirect to login
            // CLEAR the
            // LOG OUT the stored user

            // iStore. .LOGOUT()
            // iStore.dispatch({
            //   type: 'user/LOGOUT',
            // })
            // console.log('lets redirect to login', NavigationActions)
            // // console.log(NavigationActions.navigate({ routeName: 'LoginScreen' }));
            // NavigationActions.navigate({ routeName: 'LoginScreen'});

            // store.delete('loggedInUserAuth');
            // store.delete('loggedInUser');
            // message = 'Please login.'
            // type = 'warning'
            // shouldShowMessage = false;
            // Navigati

            // console.log('redirecting', store.clearAll())
            // window.location.href = '/login'
            return null
          }
          break

        case 403:
          notification.warning({
            message: 'You are not authorized to perform this action',
            // description: 'Email or password is invalid!',
          })
          break

        case 500:
          message = 'The server is encountering an issue. Please try again later.'
          break

        default:
          break
      }
    }

    // console.log(error.message, error.config, error.code, error.request, error.response)

    // if (shouldShowMessage) {
    //   showMessage({
    //     message: message,
    //     type: type,
    //     hideOnPress: true,
    //     floating: true,
    //     autoHide: true,
    //   })
    // }

    return Promise.reject(error)
    // return Promise.reject(error.response.data)
  },
)

export default class API {
  endpoint = null

  instance = axiosAPI

  sq = ''

  apiURL(endpoint = null) {
    if (endpoint) {
      this.endpoint = endpoint
    }

    return endpoint || this.endpoint
    // return endpoint ? endpoint : this.endpoint;
  }

  index = (params = {}) => {
    // console.log(this.apiURL() + this.buildQueryString(params))
    return this.axiosAPI
      .get(this.apiURL() + this.buildQueryString(params))
      .then(res => {
        // console.log(res)
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  show = (id, params) => {
    // console.log(this.apiURL() + '/' + id +  this.buildQueryString(params))
    return this.axiosAPI
      .get(`${this.apiURL()}/${id}${this.buildQueryString(params)}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  update = (id, data = {}) => {
    return this.axiosAPI
      .put(`${this.apiURL()}/${id}`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        console.log('What happened? ' + err.response.data)
        return Promise.reject(err.response)
      })
  }

  destroy = (id, data = {}) => {
    return this.axiosAPI
      .delete(`${this.apiURL()}/${id}`, { data })
      .then(res => {
        console.log('destory', res)
        return res
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  store = (data = {}) => {
    return this.post(data)
  }

  post = (data = {}) => {
    return this.axiosAPI
      .post(this.apiURL(), data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err);
        return Promise.reject(err)
      })
  }

  setHeader(key, value) {
    this.axiosAPI.defaults.headers[key] = value
  }

  buildQueryString(filterQuery) {
    this.sq = ''
    if (filterQuery) {
      this.sq += '?'
    }

    for (const filter in filterQuery) {
      if (typeof filter === 'string') {
        this.sq += `${filter}=${filterQuery[filter]}&`
      } else if (typeof filter === 'object') {
        // return new Promise(() => {
        //   this.sq = Object.keys(filter).map(
        //     (key) => {
        //         sq += `${key}=${filter[key]}`;
        //         return sq;
        //     }
        //   );
        // })
      }
    }
    return this.sq
  }

  constructor() {
    this.axiosAPI = axiosAPI
  }
}
