import axios from 'axios'
import store from 'store'
import { notification } from 'antd'

const apiClient = axios.create({
  // baseURL: '/api',
  // timeout: 1000,
  // headers: { 'X-Custom-Header': 'foobar' }
  baseURL: process.env.REACT_APP_API_URL,
  // timeout: 10000,
  timeout: 100000,
  headers: {
    Accept: process.env.REACT_APP_API_HEADER_ACCEPT,
    'Content-Type': process.env.REACT_APP_API_HEADER_CONTENT_TYPE,
  },
})

apiClient.interceptors.request.use(request => {
  const accessToken = store.get('accessToken')
  if (accessToken) {
    request.headers.Authorization = `Bearer ${accessToken}`
    request.headers.AccessToken = accessToken
  }
  return request
})

apiClient.interceptors.response.use(undefined, error => {
  const { code, response } = error
  console.log('error', code,)
  // Errors handling
  // const { response } = error

  if (code === 'ECONNABORTED') {
    notification.warning({
      message: 'The server took too long to reply. We may be experiencing very high traffic volumne. Please try again later',
    })
  } else { 
    const { data } = response

    if (data.status_code > 299 || data.status_code < 200) {
      notification.warning({
        message: data.message,
      })
    }
  }
})

export default apiClient
