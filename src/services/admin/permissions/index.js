// let { API } = require('./AbstractAPI');
// import store from 'store'
import API from 'services/AbstractAPI'

class PermissionService extends API {
  endpoint = '/admin/roles/permissions'

  browse = params => {
    const response = this.axiosAPI
      .get(this.apiURL() + this.buildQueryString(params))
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }
}

const service = new PermissionService()
export default service
