// let { API } = require('./AbstractAPI');
// import store from 'store'
import API from 'services/AbstractAPI'

class CouponAssignService extends API {
  endpoint = '/admin/coupon-assign'

  add = (data = {}) => {
    const response = this.axiosAPI
      .post(this.apiURL(), data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }
}

const service = new CouponAssignService()
export default service
