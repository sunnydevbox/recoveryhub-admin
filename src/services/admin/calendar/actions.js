// let { API } = require('./AbstractAPI');
// import store from 'store'
import API from 'services/AbstractAPI'

class CalenderActionsService extends API {
  endpoint = '/admin/schedules/actions/'

  browse = params => {
    const response = this.axiosAPI
      .get(this.apiURL() + this.buildQueryString(params))
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  getSched = () => {
    const response = this.axiosAPI
      .get(`${this.apiURL()}get-schedule`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  postSched = (data = {}) => {
    const response = this.axiosAPI
      .post(`${this.apiURL()}re-schedule`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  modifyTime = (data = {}) => {
    const response = this.axiosAPI
      .post(`${this.apiURL()}modify-time`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  add = (data = {}) => {
    const response = this.axiosAPI
      .post(this.apiURL(), data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  show = id => {
    const response = this.axiosAPI
      .get(`${this.apiURL()}/${id}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  delete = id => {
    const response = this.axiosAPI
      .delete(`${this.apiURL()}/${id}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }
}

const service = new CalenderActionsService()
export default service
