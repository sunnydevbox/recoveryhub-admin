// let { API } = require('./AbstractAPI');
// import store from 'store'
import API from 'services/AbstractAPI'

class ScheduleService extends API {
  endpoint = '/admin/schedules'

  dropdownResult = search => {
    return this.axiosAPI
      .get(`${this.apiURL()}/dropdown${this.buildQueryString({ search })}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  browse = params => {
    const response = this.axiosAPI
      .get(this.apiURL() + this.buildQueryString(params))
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  edit = (id, data = {}) => {
    const response = this.axiosAPI
      .put(`${this.apiURL()}/${id}`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  add = (data = {}) => {
    const response = this.axiosAPI
      .post(this.apiURL(), data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  show = id => {
    const response = this.axiosAPI
      .get(`${this.apiURL()}/${id}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  delete = id => {
    const response = this.axiosAPI
      .delete(`${this.apiURL()}/${id}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  dataTransactionHistory = () => {
    const response = this.axiosAPI
      .post(`${this.apiURL()}/data/transactions-history`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }
}

const service = new ScheduleService()
export default service
