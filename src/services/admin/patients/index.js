import API from 'services/AbstractAPI'

class PatientService extends API {
  endpoint = '/admin/patients'

  info = id => {
    const response = this.axiosAPI
      .post(`${this.apiURL()}/info`, { id })
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    return response
  }

  // browse = params => {
  //     const response = this.axiosAPI
  //       .get(this.apiURL() + this.buildQueryString(params))
  //       .then(res => {
  //         return res.data
  //       })
  //       .catch(err => {
  //         return Promise.reject(err.response)
  //       })

  //     return response
  //   }
}

const service = new PatientService()
export default service
