/* eslint no-use-before-define: 0 */ // --> OFF

// import { StorageType, StorageService } from './storage.service'
// import { store } from '../index'
import { notification } from 'antd'
import parser from 'html-react-parser'

// const storedCredentials = store.get('loggedInUser')

export const listProvinces = () => {
  const l = [
    'Abra',
    'Apayao',
    'Benguet',
    'Ifugao',
    'Kalinga',
    'Mountain Province',
    'Ilocos Norte',
    'Ilocos Sur',
    'La Union',
    'Pangasinan',
    'Batanes',
    'Cagayan',
    'Isabela',
    'Nueva Vizcaya',
    'Quirino',
    'Aurora',
    'Bataan',
    'Bulacan',
    'Nueva Ecija',
    'Pampanga',
    'Tarlac',
    'Zambales',
    'Batangas',
    'Cavite',
    'Laguna',
    'Quezon',
    'Rizal',
    'Marinduque',
    'Occidental Mindoro',
    'Oriental Mindoro',
    'Palawan',
    'Romblon',
    'Albay',
    'Camarines Norte',
    'Camarines Sur',
    'Catanduanes',
    'Masbate',
    'Sorsogon',
    'Aklan',
    'Antique',
    'Capiz',
    'Guimaras',
    'Iloilo',
    'Negros Occidental',
    'Bohol',
    'Cebu',
    'Negros Oriental',
    'Siquijor',
    'Biliran',
    'Eastern Samar',
    'Leyte',
    'Northern Samar',
    'Samar',
    'Southern Leyte',
    'Zamboanga del Norte',
    'Zamboanga del Sur',
    'Zamboanga Sibugay',
    'Bukidnon',
    'Camiguin',
    'Lanao del Norte',
    'Misamis Occidental',
    'Misamis Oriental',
    'Compostela Valley',
    'Davao del Norte',
    'Davao del Sur',
    'Davao Occidental',
    'Davao Oriental',
    'Cotabato',
    'Sarangani',
    'South Cotabato',
    'Sultan Kudarat',
    'Agusan del Norte',
    'Agusan del Sur',
    'Dinagat Islands',
    'Surigao del Norte',
    'Surigao del Sur',
    'Basilan',
    'Lanao del Sur',
    'Maguindanao',
    'Sulu',
    'Tawi-Tawi',
    'Metro Manila',
  ]

  return l.sort()
}

export function ucwords(string) {
  const str = string.toLowerCase().replace(/\b[a-z]/g, function(letter) {
    return letter.toUpperCase()
  })
  return str
}

export function profilePicture(avatarSet, size = 300, returnDefault = true) {
  if (!avatarSet || !avatarSet[size] || returnDefault) {
    return '../../../assets/images/doctor-placeholder.png'
  }

  return avatarSet[size]
}

export function doctorTitle(title) {
  let t = ucwords(title)
  if (title === 'medical-practitioner') {
    t = 'Psychiatrist'
  } else if (title === 'ent') {
    t = 'ENT'
  }

  return t
}

export function doctorName(doctor, format = 1) {
  let name = ''

  switch (format) {
    case 1:
    default:
      // prefix first name , last name suffix
      name += doctor.prefix && doctor.prefix.length ? `${doctor.prefix} ` : ''
      name += doctor.first_name && doctor.first_name.length ? `${ucwords(doctor.first_name)} ` : ''
      name += doctor.last_name && doctor.last_name.length ? `${ucwords(doctor.last_name)} ` : ''
      name += doctor.suffix && doctor.suffix.length ? `${doctor.suffix}` : ''
      break

    case 2:
      name += doctor.first_name && doctor.first_name.length ? `${ucwords(doctor.first_name)} ` : ''
      name += doctor.last_name && doctor.last_name.length ? `${ucwords(doctor.last_name)} ` : ''
      break

    case 3:
      name += doctor.last_name && doctor.last_name.length ? `${ucwords(doctor.last_name)}, ` : ''
      name += doctor.first_name && doctor.first_name.length ? `${ucwords(doctor.first_name)} ` : ''
      break
  }

  return name
}

export function doctorAddress(doctor, format = 1) {
  let address = ''
  switch (format) {
    case 1:
    default:
      // prefix first name , last name suffix
      address += doctor.streetNo && doctor.streetNo.length ? `${doctor.streetNo} ` : ''
      address +=
        doctor.streetAddress && doctor.streetAddress.length ? `${doctor.streetAddress} ` : ''
      address += doctor.state && doctor.state.length ? `${ucwords(doctor.state)} ` : ''
      address += doctor.city && doctor.city.length ? `${ucwords(doctor.city)} ` : ''
      break
  }

  return address
}

export function patientName(patient, format = 1) {
  let name = ''

  switch (format) {
    case 1:
    default:
      // prefix first name , last name suffix
      name += patient.prefix && patient.prefix.length && patient.prefix ? `${patient.prefix} ` : ''
      name +=
        patient.first_name && patient.first_name.length ? `${ucwords(patient.first_name)} ` : ''
      name += patient.last_name && patient.last_name.length ? `${ucwords(patient.last_name)} ` : ''
      name += patient.suffix && patient.suffix.length ? `${patient.suffix}` : ''
      break

    case 2:
      name +=
        patient.first_name && patient.first_name.length ? `${ucwords(patient.first_name)} ` : ''
      name += patient.last_name && patient.last_name.length ? `${ucwords(patient.last_name)} ` : ''
      break

    case 3:
      name += patient.last_name && patient.last_name.length ? `${ucwords(patient.last_name)}, ` : ''
      name +=
        patient.first_name && patient.first_name.length ? `${ucwords(patient.first_name)} ` : ''
      break
  }

  return name
}

export function patientContact(patient, format = 1) {
  let name = ''

  switch (format) {
    case 1:
    default:
      // prefix first name , last name suffix
      name +=
        patient.contact_first_name && patient.contact_first_name.length
          ? `${ucwords(patient.contact_first_name)} `
          : ''
      name +=
        patient.contact_middle_name && patient.contact_middle_name.length
          ? `${ucwords(patient.contact_middle_name)} `
          : ''
      name +=
        patient.contact_last_lame && patient.contact_last_lame.length
          ? `${ucwords(patient.contact_last_lame)} `
          : ''
      name += patient.suffix && patient.suffix.length ? `${patient.suffix}` : ''
      break

    case 2:
      name +=
        patient.contact_first_name && patient.contact_first_name.length
          ? `${ucwords(patient.contact_first_name)} `
          : ''
      name +=
        patient.contact_last_name && patient.contact_last_name.length
          ? `${ucwords(patient.contact_last_name)} `
          : ''
      break

    case 3:
      name +=
        patient.contact_last_name && patient.contact_last_name.length
          ? `${ucwords(patient.contact_last_name)}, `
          : ''
      name +=
        patient.contact_first_name && patient.contact_first_name.length
          ? `${ucwords(patient.contact_first_name)} `
          : ''
      break
  }

  return name
}

export function patientAddress(patient, format = 1) {
  let address = ''
  switch (format) {
    case 1:
    default:
      // prefix first name , last name suffix
      address += patient.address_1 && patient.address_1.length ? `${patient.address_1} ` : ''
      address += patient.barangay && patient.barangay.length ? `${patient.barangay} ` : ''
      address += patient.city && patient.city.length ? `${ucwords(patient.city)} ` : ''
      address += patient.state && patient.state.length ? `${ucwords(patient.state)} ` : ''
      break
  }

  return address
}

export function patientPermAddress(patient, format = 1) {
  let address = ''
  switch (format) {
    case 1:
    default:
      // prefix first name , last name suffix
      address +=
        patient.perm_no_street && patient.perm_no_street.length ? `${patient.perm_no_street} ` : ''
      address +=
        patient.perm_barangay && patient.perm_barangay.length ? `${patient.perm_barangay} ` : ''
      address +=
        patient.perm_city_municipality && patient.perm_city_municipality.length
          ? `${ucwords(patient.perm_city_municipality)} `
          : ''
      address +=
        patient.perm_province && patient.perm_province.length
          ? `${ucwords(patient.perm_province)} `
          : ''
      address +=
        patient.perm_zip_code && patient.perm_zip_code.length
          ? `${ucwords(patient.perm_zip_code)} `
          : ''

      break
  }

  return address
}

export function couponCode(coupon, format = 1) {
  let code = ''
  switch (format) {
    case 1:
    default:
      // prefix first name , last name suffix
      code += coupon.code && coupon.code.length ? `${coupon.code} ` : ''

      break
  }

  return code
}

export function specialization(roles) {
  const newRoles = []

  roles.forEach(role => {
    if (role !== 'dummy' && role !== 'doctor' && role !== 'medical-practitioner') {
      newRoles.push(doctorTitle(role))
    }
  })

  return newRoles.join(', ')
}

export function medicalAssociation(medAssoc) {
  if (typeof medAssoc !== 'string') return ''

  if (typeof medAssoc === 'string' && medAssoc.length) {
    if (medAssoc === 'fellow') {
      medAssoc = 'Fellow Philippine Psychiatric Association'
    } else if (medAssoc === 'diplomate') {
      medAssoc = 'Diplomate Philippine Psychiatric Association'
    }

    return ucwords(medAssoc)
  }

  return null
}

/*
 * specific for Psycholigst and pscyhiatrist only
 */
export function isPsychRole(roles) {
  let status = false

  roles.forEach(role => {
    let r = role

    if (role.name) {
      r = role.name
    }

    if (r === 'medical-practitioner' || r === 'psychologist' || r === 'psychiatrist') {
      status = true
    }
  })

  return status
}

export function isDoctorRole(roles) {
  let status = false
  console.log('roles', roles)
  if (typeof roles === 'string') {
    if (roles === 'doctor') {
      status = true
    }
  } else if (typeof roles === 'object') {
    roles.forEach(role => {
      let r = role

      if (role.name) {
        r = role.name
      }

      if (r === 'doctor') {
        status = true
      }
    })
  }

  return status
}

export function isPatientRole(roles) {
  let status = false

  roles.forEach(role => {
    let r = role

    if (role.name) {
      r = role.name
    }

    if (r === 'patient' || r === 'in-patient') {
      status = true
    }
  })

  return status
}

export function iAmPsychologist() {
  const storee = StorageService.storage(StorageType.LOCAL)
  const storedCredentialss = JSON.parse(storee.getItem('credentials'))

  let isADoctor = false
  let isPsychologist = false

  storedCredentialss.roles.forEach(role => {
    if (role.name === 'doctor') {
      isADoctor = true
    }

    if (role.name === 'psychologist') {
      isPsychologist = true
    }
  })

  return isADoctor && isPsychologist
}

export function iAmPsychiatrist() {
  const storee = StorageService.storage(StorageType.LOCAL)
  const storedCredentialss = JSON.parse(storee.getItem('credentials'))

  let isADoctor = false
  let isPsychiatrist = false

  storedCredentialss.roles.forEach(role => {
    if (role.name === 'doctor') {
      isADoctor = true
    }

    if (role.name === 'medical-practitioner' || role.name === 'psychiatrist') {
      isPsychiatrist = true
    }
  })

  return isADoctor && isPsychiatrist
}

export function iAmPsychRole() {
  return iAmPsychologist() || iAmPsychiatrist()
}

export function canSelectMedicalAssociation(role) {
  let canSelect = true

  if (typeof role === 'undefined') {
    if (iAmPsychologist()) {
      canSelect = false
    }
  } else {
    if (role === 'psychologist' || !role) {
      canSelect = false
    }
  }

  return canSelect
}

export function canSetS2(role) {
  let canSet = true

  if (typeof role === 'undefined') {
    if (iAmPsychologist()) {
      canSet = false
    }
  } else {
    if (role === 'psychologist' || !role) {
      canSet = false
    }
  }

  return canSet
}

export function medicalAssociationOptions(role) {
  // THIS SHOULD BE FROM BACKEND
  let canGetOptions = false

  // THIS condition indicates that current logged in user
  // will be used
  if (typeof role === 'undefined') {
    if (iAmDoctor() && canSelectMedicalAssociation()) {
      canGetOptions = true
    }
  } else {
    if (canSelectMedicalAssociation(role)) {
      canGetOptions = true
    }
  }

  if (canGetOptions) {
    let options = ['Fellow', 'Diplomate', 'Associate Member']

    storedCredentials.roles.forEach(role => {
      if (role.name !== 'patient' && role.name !== 'doctor' && role.name !== 'dummy') {
        role = role.name
      }
    })

    switch (role) {
      case 'dermatologist':
        options.push('Philippine Dermatological Society')
        break
      case 'pediatrician':
        options.push('Philippine Pediatric Society')
        break
      case 'surgeon':
        options.push('Philippine Society of General Surgery')
        break
      case 'physician':
        options.push('Philippine College of Physicians')
        break
      case 'family-medicine':
        options.push('Philippine Academy of Family Medicine')
        break
      case 'neurologist':
        options.push('Philippine Neurological association')
        break
      case 'ent':
        options.push('Philippine Society of Otolaryngology - Head & Neck Surgery')
        break

      case 'opthalmologist':
        options.push('Philippine Academy of Opthalmology')
        break
      case 'gynecologist':
        options.push('Philippine Society of Obstetrics & Gynecology')
        break
      case 'physiatrists':
        options.push('Philippine Academy of Rehabilitation Medicine')
        break
    }

    return options
  }

  return []
}

export function iAmDoctor() {
  const store = StorageService.storage(StorageType.LOCAL)
  const storedCredentials = JSON.parse(store.get('credentials'))

  let isADoctor = false

  storedCredentials.roles.forEach(role => {
    if (role.name === 'doctor') {
      isADoctor = true
    }
  })

  return isADoctor
}

export function iAmPatient() {
  const store = StorageService.storage(StorageType.LOCAL)
  const storedCredentials = JSON.parse(store.get('credentials'))

  let isAPatient = false

  storedCredentials.roles.forEach(role => {
    if (role.name === 'patient' || role.name === 'in-patient') {
      isAPatient = true
    }
  })

  return isAPatient
}

export function avatar(size, source) {
  let avatar = '../assets/images/doctor-placeholder.png'

  if (!source) {
    const store = StorageService.storage(StorageType.LOCAL)
    const storedCredentials = JSON.parse(store.get('credentials'))
    source = storedCredentials.avatar
  }

  // console.log('avatar source', source)
  if (typeof source[size] !== 'undefined') {
    return source[size].url
  }

  if (typeof source['picture'] !== 'undefined' && source['picture'].url.length) {
    return source['picture'].url
  }

  return avatar
}

/**** CONSULTATION NOTES HELPERS ****/

export function canAccessConsultationNotes_LaboratoryRequest() {
  return iAmDoctor() && !iAmPsychologist() ? true : false
}

export function canAccessConsultationNotes_Medication() {
  return iAmDoctor() && !iAmPsychologist() ? true : false
}

export function canAccessConsultationNotes_Diagnosis() {
  return iAmDoctor() && !iAmPsychologist() ? true : false
}

export function canAccessConsultationNotes_Diagnosis_CustomInput() {
  return iAmDoctor() && !iAmPsychologist() && !iAmPsychiatrist() ? true : false
}

export function canAccessConsultationNotes_NextMeeting() {
  return iAmDoctor() ? true : false
}

export function canAccessConsultationNotes_Diagnosis_PsychiatristDropdown() {
  return iAmPsychiatrist() ? true : false
}

export function consulationNotes_Label_MedicalAbstract() {
  return iAmDoctor() && !iAmPsychologist() ? 'Medical Abstract' : 'Summary Evaluation'
}

export function consulationNotes_Label_MedicalCertificate() {
  return iAmDoctor() && !iAmPsychologist() ? 'Medical Certificate' : 'Consultation Report'
}

export function consulationNotes_Label_AdmittingOrder() {
  return iAmDoctor() && !iAmPsychologist() ? 'Admitting Order' : 'Referral Letter'
}

/** ROLES  **/
export function getRoles() {
  const roles = [
    'psychologist',
    'psychiatrist',
    'opthalmologist',
    'dermatologist',
    'surgeon',
    'internist',
    'family-physician',
    'neurologist',
    'ent',
    'opthalmologist',
    'pediatrician',
    'gynecologist',
    'physiatrists',
    'rehab-medicine',
  ]

  return roles
}

export function parseRole(roleSet) {
  let r = ''
  // console.log('parseRole(roleSet)',roleSet)
  roleSet.forEach(role => {
    if (role !== 'dummy' && role !== 'doctor') {
      r = role
    }
  })

  return r
}

/*** CALENDAR  ***/
export function calendarEventStatusBGColor(status) {
  let className = ''

  if (!status) {
    return className
  }

  switch (status.toUpperCase()) {
    case 'BOOKED':
      className = 'status-booked'
      break

    case 'RESERVED':
      className = 'status-reserved'
      break

    case 'OPEN':
    default:
      className = 'status-open'
      break
  }
  // console.log("className", className)

  return className
}

export function Notification(serverData) {
  let type = 'info'
  let title = 'Info'
  let description = ''

  if (serverData) {
    if (serverData.status) {
      if (serverData.status >= 400 && serverData.status <= 499) {
        type = 'warning'
        title = 'Warning'
      } else if (serverData.status >= 200 && serverData.status <= 299) {
        type = 'success'
        title = 'Success'
      } else if (serverData.status >= 500 && serverData.status <= 599) {
        type = 'error'
        title = 'Error'
      }
    }

    if (serverData.description) {
      ;({ description } = serverData)
    } else if (serverData.data && serverData.data.message) {
      if (typeof serverData.data.message === 'object') {
        Object.keys(serverData.data.message).forEach(field => {
          // description += `<strong>${field}</strong>`

          if (typeof serverData.data.message[field] === 'object') {
            Object.values(serverData.data.message[field]).forEach(err => {
              description += `${err}<br />`
            })
          }
        })
      }
    }

    // Title
    if (serverData && serverData.title) {
      ;({ title } = serverData)
    }
  }

  notification[type]({
    message: title,
    description: parser(description),
  })
}

export function renderField(label, value) {
  return (
    <div className="field mt-4">
      {value && value.length ? (
        <div className="pl-1" style={{ whiteSpace: 'pre' }}>
          {value}
        </div>
      ) : (
        <em className="pl-1 text-muted">Not specified</em>
      )}
      <hr className="m-0" />
      <span className="pl-1 text-muted">{label}</span>
    </div>
  )
}

export function pathUrl(p) {
  return `${process.env.PUBLIC_URL}${p}`
}
