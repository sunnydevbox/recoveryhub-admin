import React, { useState, useEffect } from 'react'
import { AutoComplete } from 'antd'

const { Option } = AutoComplete

const Complete = ({ suggestions }) => {
  const [value, setValue] = useState('')
  const [results, setResult] = useState([])

  useEffect(() => {}, [])

  const handleSearch = searchText => {
    let res = []

    if (searchText.length >= 3) {
      res = suggestions
    }
    setResult(res)
  }

  const onSelect = data => {
    console.log('onSelect', data)
  }

  const onChange = data => {
    setValue(data)
  }

  return (
    <AutoComplete
      style={{
        width: 200,
      }}
      filterOption={(inputValue, option) =>
        option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
      }
      value={value}
      onSearch={handleSearch}
      onSelect={onSelect}
      onChange={onChange}
      placeholder="Input Here Index 3"
    >
      {results.map(({ id, name }) => (
        <Option key={id} value={name}>
          {name}
        </Option>
      ))}
    </AutoComplete>
  )
}

export default Complete
