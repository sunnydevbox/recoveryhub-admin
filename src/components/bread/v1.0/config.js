/* V1.0 */
import Service from 'services/CustomerService'

export const LABEL_SINGULAR = 'Customer'
export const LABEL_PLURAL = 'Customers'
export const DRAWER_WIDTH = '80%'
export const STORE_PREFIX = 'featureCustomer'
export const API_SERVICE = Service
