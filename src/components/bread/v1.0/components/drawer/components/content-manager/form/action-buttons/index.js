import React, { useEffect, useState } from 'react'
import { Button, Modal } from 'antd'
import { connect } from 'react-redux'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    formTriggerSave: () => dispatch({ type: `${STORE_PREFIX}/FORM_TRIGGER_SAVE`, id: null }),
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    deleteRecord: id => dispatch({ type: `${STORE_PREFIX}/API_DELETE_RECORD`, id }),
  }
}

const FormActionButtonsComponent = ({
  [STORE_PREFIX]: { formTouched, selectedRecord },
  formCancel,
  deleteRecord,
}) => {
  const [modalConfirmDeleteState, setModalConfirmDeleteState] = useState(false)

  useEffect(() => {}, [formTouched])

  return (
    <>
      <Button type="primary" className="mr-2" htmlType="submit" disabled={!formTouched}>
        Save
      </Button>

      <Button
        type="primary"
        className="cta-btn-delete mr-2"
        onClick={() => {
          setModalConfirmDeleteState(true)
        }}
      >
        Delete
      </Button>

      <Button
        type="primary"
        onClick={() => {
          formCancel()
        }}
      >
        Cancel
      </Button>

      <Modal
        title="Confirm"
        visible={modalConfirmDeleteState}
        onOk={() => {
          // formCancel(true)

          // - CALL API
          // - close this modal
          // - cancelForm
          // - refresh table
          deleteRecord(selectedRecord.id)
          setModalConfirmDeleteState(false)
          formCancel({ force: true })
        }}
        onCancel={() => {
          setModalConfirmDeleteState(false)
        }}
        okText="Yes"
        cancelText="No"
      >
        <p>Are you sure you want to delete this record?</p>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormActionButtonsComponent)
