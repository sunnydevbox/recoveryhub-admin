/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import {
  Input,
  Form,
  Spin,
  // Button,
} from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import FormActionButtonsComponent from './action-buttons'
import FormModalExitFormComponent from './modal-exit-form'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const FormItem = Form.Item
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    getList: data => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord, drawerState, formTriggerSave },
  commitRecord,
  setFormTouched,
  // toggleDrawer,
}) => {
  const [form] = Form.useForm()

  useEffect(() => {
    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      // populate form
      // console.log(selectedRecord)
      form.setFieldsValue({
        address: selectedRecord.address,
        city: selectedRecord.city,
        contact_email: selectedRecord.contact_email,
        contact_fax: selectedRecord.contact_fax,
        contact_name: selectedRecord.contact_name,
        contact_phone: selectedRecord.contact_phone,
        contact_title: selectedRecord.contact_title,
        country: selectedRecord.country,
        name: selectedRecord.name,
        state: selectedRecord.state,
        zipcode: selectedRecord.zipcode,
      })
    } else {
      form.setFieldsValue({
        address: form.getFieldValue('address'),
        city: form.getFieldValue('city'),
        contact_email: form.getFieldValue('contact_email'),
        contact_fax: form.getFieldValue('contact_fax'),
        contact_name: form.getFieldValue('contact_name'),
        contact_phone: form.getFieldValue('contact_phone'),
        contact_title: form.getFieldValue('contact_title'),
        country: form.getFieldValue('country'),
        name: form.getFieldValue('name'),
        state: form.getFieldValue('state'),
        zipcode: form.getFieldValue('zipcode'),
      })
    }

    // if (formTriggerSave) {
    //   console.log(form.getFieldValue('name'))
    //   form
    //       .validateFields()
    //       .then(values => {
    //         // form.resetFields();
    //         commitRecord(values)
    //       })
    //       .catch(info => {
    //         console.log('Validate Failed:', info);
    //       });
    // }
  }, [form, drawerState, selectedRecord, loadingSelectedRecord, formTriggerSave, commitRecord])

  const handleSave = async values => {
    commitRecord(values)
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        onFinish={handleSave}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group">
                <FormItem
                  name="name"
                  rules={[
                    { required: true, message: 'Enter name' },
                    { min: 3, message: 'Minimum of 3 characters' },
                  ]}
                  extra="Company name or person's name"
                >
                  <Input placeholder="Name *" />
                </FormItem>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <strong>Address</strong>
              <hr />

              <div className="form-group">
                <FormItem name="address">
                  <Input placeholder="Address" />
                </FormItem>
              </div>

              <div className="row">
                <div className="col-lg-4">
                  <FormItem name="city">
                    <Input placeholder="City" />
                  </FormItem>
                </div>

                <div className="col-lg-4">
                  <FormItem name="state">
                    <Input placeholder="State" />
                  </FormItem>
                </div>

                <div className="col-lg-4">
                  <FormItem name="zipcode">
                    <Input placeholder="Zip Code" />
                  </FormItem>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <strong>Contact Info </strong>
              <hr />

              <div className="form-group">
                <div className="row">
                  <div className="col-lg-12">
                    <FormItem name="contact_name">
                      <Input placeholder="Contact Person's Name" />
                    </FormItem>
                  </div>
                  <div className="col-lg-12">
                    <FormItem name="contact_title">
                      <Input placeholder="Designation" />
                    </FormItem>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-6">
                    <FormItem
                      name="contact_email"
                      rules={[{ type: 'email', message: `Email is not a valid!` }]}
                    >
                      <Input placeholder="Email" />
                    </FormItem>
                  </div>

                  <div className="col-lg-6">
                    <FormItem name="contact_phone">
                      <Input placeholder="Phone" />
                    </FormItem>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container footera ant-drawer-footer text-center px-0 py-2">
          <div className="row">
            <div className="col-sm-12">
              <FormActionButtonsComponent />
            </div>
          </div>
        </div>
      </Form>

      <FormModalExitFormComponent />
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
