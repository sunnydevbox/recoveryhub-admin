import React, { useEffect } from 'react'
import { Skeleton } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from '../../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    formTriggerSave: () => dispatch({ type: `${STORE_PREFIX}/FORM_TRIGGER_SAVE`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
  }
}

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  // const [cancelConfirmVisible, setCancelConfirmVisible] = useState(false)

  useEffect(() => {
    console.log(selectedRecord)
  }, [selectedRecord])

  const renderField = (label, value) => {
    return (
      <div className="field mt-4">
        {value && value.length ? (
          <div className="pl-1">{value}</div>
        ) : (
          <em className="pl-1 text-muted">Not specified</em>
        )}
        <hr className="m-0" />
        <span className="pl-1 text-muted">{label}</span>
      </div>
    )
  }

  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <div className="row mb-4">
              <div className="col-sm-12">
                <strong>Contact Info </strong>
                <hr className="mb-1 mt-2" />
                {renderField('Name', selectedRecord.name)}
              </div>
            </div>

            <div className="row mb-4">
              <div className="col-sm-12">
                <strong>Address</strong>
                <hr className="mb-1 mt-2" />
                <div>
                  {renderField(
                    'Address',
                    `
                  ${selectedRecord.address ? selectedRecord.address : ''} 
                  ${selectedRecord.city ? selectedRecord.city : ''}
                  ${selectedRecord.state ? selectedRecord.state : ''}
                  ${selectedRecord.country ? selectedRecord.country : ''}
                  ${selectedRecord.zipcode ? selectedRecord.zipcode : ''}
                `,
                  )}
                  {/* {selectedRecord.address} {selectedRecord.city} {selectedRecord.state} {selectedRecord.country} {selectedRecord.zipcode} */}
                </div>
              </div>
            </div>

            <div className="row mb-4">
              <div className="col-sm-12">
                <strong>Contact Info </strong>
                <hr className="mb-1 mt-2" />
                {renderField('Designation', selectedRecord.contact_title)}
                {renderField('Name', selectedRecord.contact_name)}
                {renderField('Email', selectedRecord.contact_email)}
                {renderField('Phone', selectedRecord.contact_phone)}
              </div>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewRecordDetailsComponent)
