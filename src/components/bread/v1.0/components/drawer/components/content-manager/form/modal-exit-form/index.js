/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import { Modal } from 'antd'
import { connect } from 'react-redux'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    cancelFormConfirmCancelModal: async () =>
      dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL_MODAL` }),
  }
}

const FormModalExitFormComponent = ({
  [STORE_PREFIX]: { formConfirmCancelModal },
  formCancel,
  cancelFormConfirmCancelModal,
}) => {
  useEffect(() => {}, [formConfirmCancelModal])

  //   const handleSave = async values => {
  //     commitRecord(values)
  //   }

  return (
    <Modal
      title="Confirm"
      visible={formConfirmCancelModal}
      onOk={() => {
        formCancel(true)
      }}
      onCancel={() => {
        cancelFormConfirmCancelModal()
      }}
      okText="Yes"
      cancelText="No"
    >
      <p>
        You have made changes to this form. You will lose any changes if you exit.
        <br />
        <br />
        Continue to exit?
      </p>
    </Modal>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormModalExitFormComponent)
