import React, { useState, useEffect } from 'react'
import { Table } from 'antd'

const BrowseComponent = props => {
  const { rowKey } = props
  const [dataSource, setDataSource] = useState([])
  const [columns, setColumns] = useState([])
  const [loading, setLoading] = useState([])
  const [pagination, setPagination] = useState({
    position: ['none', 'bottomRight'],
    total: 0,
  })

  useEffect(() => {
    setDataSource(props.dataSource)
    setColumns(props.columns)
    setPagination(props.pagination)
    setLoading(props.loading)
  }, [props])

  const tableChanged = (page, filters, sorter, extra) => {
    if (props.tableChanged) {
      props.tableChanged(page, filters, sorter, extra)
    }
  }

  return (
    <>
      <Table
        dataSource={dataSource}
        columns={columns}
        pagination={pagination}
        rowKey={rowKey}
        loading={loading}
        onChange={(page, filters, sorter, extra) => {
          tableChanged(page, filters, sorter, extra)
        }}
      />
    </>
  )
}

export default BrowseComponent
