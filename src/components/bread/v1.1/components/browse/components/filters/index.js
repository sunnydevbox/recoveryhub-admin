import React, { useEffect } from 'react'
import { Space, Select } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from '../../../../config'

const { Option } = Select

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filters => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, filters }),
    resetFilters: () => dispatch({ type: `${STORE_PREFIX}/RESET_FILTERS` }),
  }
}

const FiltersComponent = ({ setFilter, resetFilters }) => {
  useEffect(() => {}, [])

  const filterChanged = (key, value) => {
    if (key) {
      const d = []
      d[key] = value
      setFilter(d)
    } else {
      resetFilters()
    }

    // if (key === null) {
    //   filters.filters = {}
    // } else {
    //   filters.filters[key] = value
    // }

    // console.log('fileCHanged', key, value, filters.filters)
  }

  return (
    <Space>
      <Select
        placeholder="Select Status"
        onChange={v => filterChanged('status', v)}
        // value={filters.filters.status}
      >
        <Option value="all">All</Option>
        <Option value="active">Active</Option>
        <Option value="inactive">Inactive</Option>
      </Select>

      <Select
        placeholder="Is Verified?"
        onChange={v => filterChanged('verified', v)}
        // value={filters.filters.verified}
      >
        <Option value="all">All</Option>
        <Option value="1">Yes</Option>
        <Option value="0">No</Option>
      </Select>

      <a
        role="button"
        tabIndex={0}
        onClick={() => filterChanged(null)}
        onKeyDown={() => filterChanged(null)}
      >
        Clear Filters
      </a>
    </Space>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FiltersComponent)
