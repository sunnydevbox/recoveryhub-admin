import React from 'react'
import { Drawer, Skeleton } from 'antd'
import { connect } from 'react-redux'
import parser from 'html-react-parser'
import DrawerContentManagerComponent from './components/content-manager'
import FooterButtonsComponent from './components/footer-buttons'
import { LABEL_SINGULAR, DRAWER_WIDTH, STORE_PREFIX, DRAWER_PLACEMENT } from '../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    setContentViewMode: viewMode =>
      dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
  }
}

const DrawerComponent = ({
  [STORE_PREFIX]: { drawerState, selectedRecord, loadingSelectedRecord, viewMode },
  formCancel,
}) => {
  const getDrawerTitle = () => {
    let title = ''

    if (!selectedRecord && viewMode === 'new') {
      title = `${LABEL_SINGULAR}`
    } else if (selectedRecord) {
      let label = `${LABEL_SINGULAR}: <strong>${selectedRecord.name}</strong>`

      if (viewMode === 'edit') {
        label = `Update ${LABEL_SINGULAR}: <strong>${selectedRecord.name}</strong>`
      }

      title = !loadingSelectedRecord && selectedRecord ? parser(label) : <Skeleton.Input />
    }
    return title
  }

  return (
    <>
      <Drawer
        title={getDrawerTitle()}
        placement={DRAWER_PLACEMENT}
        height={DRAWER_WIDTH}
        closable={false}
        onClose={() => {
          formCancel()
        }}
        visible={drawerState}
        footer={<FooterButtonsComponent />}
      >
        <DrawerContentManagerComponent />
      </Drawer>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent)
