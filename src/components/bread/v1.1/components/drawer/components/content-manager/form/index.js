/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import { Input, Form, Spin } from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import DropdownSearchDataComponent from 'components/form/dropdown-search-data'
import CategoryService from 'services/CategoryService'
import MetricService from 'services/MetricService'
import FormModalExitFormComponent from './modal-exit-form'
import AvatarComponent from './avatar'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'
import fields from './form-fields'

const FormItem = Form.Item
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference =>
      dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord, drawerState, formReference },
  commitRecord,
  setFormTouched,
  setFormReference,
}) => {
  const [form] = Form.useForm()

  useEffect(() => {
    if (form && !formReference) {
      setFormReference(form)
    }

    const newFields = {}
    let step = 1
    Object.keys(fields).forEach(key => {
      if (drawerState && selectedRecord && !loadingSelectedRecord) {
        step = 1
        newFields[key] = selectedRecord[key]
      } else {
        step = 2
        newFields[key] = form.getFieldValue(key)
      }
    })

    if (step === 1) {
      form.setFieldsValue(newFields)
    } else if (step === 2) {
      form.setFieldsValue(newFields)
    }
  }, [form, setFormReference, formReference, drawerState, selectedRecord, loadingSelectedRecord])

  const handleSave = async values => {
    console.log('handleSave')
    commitRecord(values)
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        onFinish={handleSave}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="container">
          <div className="row">
            <div className="col-md-1">
              <AvatarComponent />
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <FormItem
                  name="name"
                  rules={[{ required: true }, { min: 3, message: 'Minimum of 3 characters' }]}
                >
                  <Input placeholder="Name" />
                </FormItem>
              </div>
            </div>

            <FormItem name="metric_id" rules={[{ required: true }]}>
              <DropdownSearchDataComponent
                style={{ width: '100%' }}
                setSelected={metricId => {
                  form.setFieldsValue({ metric_id: metricId })
                }}
                renderOptionItem={item => {
                  return { value: item.id, label: `${item.name} (${item.symbol})` }
                }}
                Service={MetricService}
                placeholder="Select Unit of Meaurement"
              />
            </FormItem>

            <FormItem name="category_id" rules={[{ required: true }]}>
              <DropdownSearchDataComponent
                style={{ width: '100%' }}
                setSelected={categoryId => {
                  form.setFieldsValue({ category_id: categoryId })
                }}
                renderOptionItem={item => {
                  return { value: item.id, label: item.name }
                }}
                Service={CategoryService}
                placeholder="Select Category"
              />
            </FormItem>
          </div>
        </div>
      </Form>

      <FormModalExitFormComponent />
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
