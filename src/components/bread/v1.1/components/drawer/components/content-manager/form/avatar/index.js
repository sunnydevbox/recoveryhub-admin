/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React from 'react'
import { Avatar, Upload } from 'antd'
import { UserOutlined } from '@ant-design/icons'
// import './styles.scss'

const AvatarComponent = () => {
  const onChange = file => {
    console.log(file)
  }

  return (
    <>
      <Upload maxCount="1" listType="picture" multiple={false} onChange={onChange}>
        <Avatar shape="square" icon={<UserOutlined />} />
      </Upload>
    </>
  )
}

export default AvatarComponent
