import React from 'react'
import { Skeleton } from 'antd'
import { connect } from 'react-redux'
import { renderField } from 'services/helpers'
import { STORE_PREFIX } from '../../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = () => {
  return {}
}

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <div className="row mb-4">
              <div className="col-sm-12">
                <strong>Contact Info </strong>
                <hr className="mb-1 mt-2" />
                {renderField('Name', selectedRecord.name)}
              </div>
            </div>

            <div className="row mb-4">
              <div className="col-sm-12">
                <strong>Address</strong>
                <hr className="mb-1 mt-2" />
                <div>
                  {renderField(
                    'Address',
                    `
                  ${selectedRecord.address ? selectedRecord.address : ''} 
                  ${selectedRecord.city ? selectedRecord.city : ''}
                  ${selectedRecord.state ? selectedRecord.state : ''}
                  ${selectedRecord.country ? selectedRecord.country : ''}
                  ${selectedRecord.zipcode ? selectedRecord.zipcode : ''}
                `,
                  )}
                  {/* {selectedRecord.address} {selectedRecord.city} {selectedRecord.state} {selectedRecord.country} {selectedRecord.zipcode} */}
                </div>
              </div>
            </div>

            <div className="row mb-4">
              <div className="col-sm-12">
                <strong>Contact Info </strong>
                <hr className="mb-1 mt-2" />
                {renderField('Designation', selectedRecord.contact_title)}
                {renderField('Name', selectedRecord.contact_name)}
                {renderField('Email', selectedRecord.contact_email)}
                {renderField('Phone', selectedRecord.contact_phone)}
              </div>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewRecordDetailsComponent)
