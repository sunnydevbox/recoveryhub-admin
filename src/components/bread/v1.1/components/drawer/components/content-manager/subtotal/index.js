import React, { useEffect } from 'react'
import { Table } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from '../../../../../config'
import columns from './columns'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    formTriggerSave: () => dispatch({ type: `${STORE_PREFIX}/FORM_TRIGGER_SAVE`, id: null }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
  }
}

const dataSource = [
  {
    key: 'subtotal',
    label: 'Subtotal',
    value: '1',
  },
  {
    key: 'discount',
    label: 'Discount',
    value: '2',
  },
  {
    key: 'subtotallesdiscount',
    label: 'Subtotal Less Discount',
    value: '3',
  },
  {
    key: 'taxrate',
    label: 'Tax Rate',
    value: '4',
  },
  {
    key: 'totaltax',
    label: 'Total Tax',
    value: '5',
  },
  {
    key: 'shipping',
    label: 'Shipping/Handling',
    value: '6',
  },
  {
    key: 'total',
    label: 'Total',
    value: '7',
  },
]

const ItemsSubTotalComponent = ({ [STORE_PREFIX]: { selectedRecord } }) => {
  useEffect(() => {}, [selectedRecord])

  return (
    <>
      <Table dataSource={dataSource} columns={columns} showHeader={false} />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsSubTotalComponent)
