import React, { useEffect } from 'react'
import { Drawer } from 'antd'

const DrawerComponent = props => {
  const { drawerVisibilityChanged, title, visibility, content, footer } = props
  // const [drawerVisibility, setDrawerVisibility] = useState(false)

  useEffect(() => {
    // setDrawerVisibility(visibility)
  }, [props, visibility])

  const getTitle = () => {
    return title
  }

  const getContent = () => {
    if (!content) return 'Not defined'

    return content
  }

  const getFooter = () => {
    if (footer) return footer

    return null
  }

  return (
    <>
      <Drawer
        title={getTitle()}
        placement="right"
        width="50%"
        closable={false}
        onClose={() => {
          // setDrawerVisibility(false)
          drawerVisibilityChanged(false)
        }}
        visible={visibility}
        footer={getFooter()}
      >
        {getContent()}
      </Drawer>
    </>
  )
}

export default DrawerComponent
