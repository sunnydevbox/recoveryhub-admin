import React from 'react'
import { Button } from 'antd'
import { SyncOutlined } from '@ant-design/icons'

const RefreshButtonComponent = ({ callback, loading }) => {
  return (
    <>
      <Button
        icon={<SyncOutlined spin={loading} />}
        onClick={callback}
        type="primary"
        shape="round"
      />
    </>
  )
}

export default RefreshButtonComponent
