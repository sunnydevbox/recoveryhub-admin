/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useState } from 'react'
import { Select, Spin } from 'antd'
import './styles.scss'

const DropdownComponent = ({ setSelected, Service, renderOptionItem, ...props }) => {
  const [options, setOptions] = useState([])
  const [value, setValue] = useState([])
  const [fetching, setFetching] = useState(false)

  const handleSearch = str => {
    setOptions([])
    setFetching(true)
    Service.dropdownResult(str)
      .then(response => {
        setFetching(false)
        setOptions(
          response.data.map(item => {
            return renderOptionItem(item)
          }),
        )
      })
      .catch(() => {
        setFetching(false)
      })
  }

  const handleChange = str => {
    if (setSelected) {
      setSelected(str)
    }
    setValue(str)
  }

  return (
    <>
      <Select
        showSearch
        value={value}
        defaultActiveFirstOption={false}
        showArrow={false}
        filterOption={false}
        onSearch={handleSearch}
        onChange={handleChange}
        notFoundContent={fetching ? <Spin size="small" /> : null}
        options={options}
        onFocus={() => {
          handleSearch('')
        }}
        {...props}
      />
    </>
  )
}

export default DropdownComponent
