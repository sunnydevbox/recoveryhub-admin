import { message } from 'antd'

export const onFinishFailed = errorInfo => {
  console.log(errorInfo)
  message.warning('The form was not submitted')
}

export default onFinishFailed
