import errorNotif from 'components/400'

const errorMedicine = error => {
  const errorData = error.data
  const status = errorData.code
  const messages = [
    { id: 1, name: errorData.message.name },
    { id: 2, name: errorData.message.generic_name_id },
    { id: 3, name: errorData.message.dosage },
    { id: 4, name: errorData.message.unit },
  ]
  errorNotif(messages, status)
}

export default errorMedicine
