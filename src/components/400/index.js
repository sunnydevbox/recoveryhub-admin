import React from 'react'
import { notification } from 'antd'

const errorNotif = (messages, status) => {
  notification.error({
    message: <div style={{ marginBottom: '15px' }}>Error: {status} </div>,
    description: messages.map(texts => (
      <ul key={texts.id} style={{ listStyleType: 'none' }}>
        <li>{texts.name}</li>
      </ul>
    )),
  })
}

export default errorNotif
