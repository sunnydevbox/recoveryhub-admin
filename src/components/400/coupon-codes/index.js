import errorNotif from 'components/400'

const errorCoupon = error => {
  const errorData = error.data
  const status = errorData.code
  const messages = [
    { id: 1, name: errorData.message.code },
    { id: 2, name: errorData.message.name },
    { id: 3, name: errorData.message.description },
  ]
  errorNotif(messages, status)
}

export default errorCoupon
