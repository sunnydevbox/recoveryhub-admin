import errorNotif from 'components/400'

const errorGeneric = error => {
  const errorData = error.data
  const status = errorData.code
  const messages = [{ id: 1, name: errorData.message.name }]
  errorNotif(messages, status)
}

export default errorGeneric
