import actions from './actions'

const initialState = {
  drawerState: false, // true or false for open or close
  reloadListState: false, // used when adding new records to DB
  listLoading: false, // used in Table List
  searchLoading: false, // used in searching
  dataSource: [], // Database
  queryParams: {
    filters: {},
    searchTerm: '',
    sorter: '',
    page: '',
  }, // query parameters
  pagination: {
    total: 0,
    pageSize: 3,
  },
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload }
    default:
      return state
  }
}
