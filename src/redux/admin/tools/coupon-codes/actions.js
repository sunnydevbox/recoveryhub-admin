const actions = {
  SET_STATE: 'toolCouponCode/SET_STATE',
  GET_DATA: 'toolCouponCode/GET_DATA',
  TOGGLE_DRAWER: 'toolCouponCode/TOGGLE_DRAWER',
  TRIGGER_RELOAD_BROWSE: 'toolCouponCode/TRIGGER_RELOAD_BROWSE',
  SET_BROWSE: 'toolCouponCode/SET_BROWSE',
  GET_BROWSE_FAILED: 'toolCouponCode/GET_BROWSE_FAILED',
  SET_QUERY_PARAMS: 'toolCouponCode/SET_QUERY_PARAMS',
  SET_SEARCH_TERM: 'toolCouponCode/SET_SEARCH_TERM',
}

export default actions
