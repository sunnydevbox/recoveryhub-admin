const actions = {
  SET_STATE: 'recordFeedback/SET_STATE',
  GET_DATA: 'recordFeedback/GET_DATA',
  TOGGLE_DRAWER: 'recordFeedback/TOGGLE_DRAWER',
  TRIGGER_RELOAD_BROWSE: 'recordFeedback/TRIGGER_RELOAD_BROWSE',
  SET_BROWSE: 'recordFeedback/SET_BROWSE',
  GET_BROWSE_FAILED: 'recordFeedback/GET_BROWSE_FAILED',
  SET_QUERY_PARAMS: 'recordFeedback/SET_QUERY_PARAMS',
  SET_SEARCH_TERM: 'recordFeedback/SET_SEARCH_TERM',
}

export default actions
