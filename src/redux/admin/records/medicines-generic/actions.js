const actions = {
  SET_STATE: 'recordGeneric/SET_STATE',
  GET_DATA: 'recordGeneric/GET_DATA',
  TOGGLE_DRAWER: 'recordGeneric/TOGGLE_DRAWER',
  TRIGGER_RELOAD_BROWSE: 'recordGeneric/TRIGGER_RELOAD_BROWSE',
  SET_BROWSE: 'recordGeneric/SET_BROWSE',
  SET_LIMIT: 'recordGeneric/SET_LIMIT',
  GET_BROWSE_FAILED: 'recordGeneric/GET_BROWSE_FAILED',
  SET_QUERY_PARAMS: 'recordGeneric/SET_QUERY_PARAMS',
  SET_SEARCH_TERM: 'recordGeneric/SET_SEARCH_TERM',
}

export default actions
