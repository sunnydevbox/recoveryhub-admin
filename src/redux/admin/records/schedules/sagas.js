import { all, takeEvery, put, select, call } from 'redux-saga/effects'
import moment from 'moment'
import actions from './actions'
import ScheduleService from '../../../../services/admin/calendar'

export function* TOGGLE_DRAWER() {
  // const { drawerState } = yield select(state => state)
  const { drawerState } = yield select(state => state.recordSchedules)
  const s = !drawerState
  console.log(s)
  if (!s) {
    yield call(SELECT_EVENT, { payload: { id: null } }) // this blocks flow until onSubmit is done
  }

  yield put({
    type: 'recordSchedules/SET_STATE',
    payload: {
      drawerState: s,
    },
  })
}

export function* SELECT_EVENT(data) {
  let d = null

  yield put({
    type: 'recordSchedules/SET_STATE',
    payload: {
      loadingSelectedEvent: false,
    },
  })

  if (data.payload.id) {
    yield put({
      type: 'recordSchedules/SET_STATE',
      payload: {
        loadingSelectedEvent: true,
      },
    })

    // CALL API
    d = yield call(ScheduleService.show, data.payload.id)
  }

  yield put({
    type: 'recordSchedules/SET_STATE',
    payload: {
      selectedEvent: d,
      loadingSelectedEvent: false,
    },
  })
}

export function* DATA_IS_LOADING(payload) {
  console.log('payload.payload.listLoading', payload.payload.listLoading)
  yield put({
    type: 'recordSchedules/SET_STATE',
    payload: {
      listLoading: !payload.payload.listLoading,
    },
  })
}

export function* GET_CALENDAR_ENTRIES() {
  yield put({
    type: 'recordSchedules/SET_STATE',
    payload: {
      listLoading: true,
    },
  })

  const { filters } = yield select(state => state.recordSchedules)
  const res = yield call(ScheduleService.browse, filters)

  // Formatting the data for the calendar
  let calendarEntries = res.data.map(e => ({
    data: e,
    id: e.id,
    title: 'title',
    start: moment(e.start.t, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:mm:ss'),
    end: moment(e.end.t, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:mm:ss'),
  }))
  calendarEntries = Object.assign({}, calendarEntries)

  yield put({
    type: 'recordSchedules/SET_STATE',
    payload: {
      listLoading: false,
      calendarEntries,
    },
  })
}

export function* RESET_FILTERS() {
  yield put({
    type: 'recordSchedules/SET_STATE',
    payload: {
      filters: {
        status: 'OPEN',
        roles: 'all',
      },
    },
  })

  // TRIGGER GET_CALENDAR_ENTRIES
}

export function* SET_FILTER(data) {
  let { filters } = yield select(state => state.recordSchedules)
  filters = { ...filters, ...data.payload }
  console.log('SET_FILTERS', filters, data.payload)

  yield put({
    type: 'recordSchedules/SET_STATE',
    payload: {
      filters,
    },
  })

  // TRIGGER GET_CALENDAR_ENTRIES
  yield call(GET_CALENDAR_ENTRIES)
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.TOGGLE_DRAWER, TOGGLE_DRAWER),
    // takeEvery(actions.TRIGGER_RELOAD_BROWSE, TRIGGER_RELOAD_BROWSE),
    takeEvery(actions.DATA_IS_LOADING, DATA_IS_LOADING),
    takeEvery(actions.SELECT_EVENT, SELECT_EVENT),
    takeEvery(actions.GET_CALENDAR_ENTRIES, GET_CALENDAR_ENTRIES),
    takeEvery(actions.RESET_FILTERS, RESET_FILTERS),
    takeEvery(actions.SET_FILTER, SET_FILTER),
  ])
}
