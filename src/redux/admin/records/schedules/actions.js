const actions = {
  SET_STATE: 'recordSchedules/SET_STATE',
  GET_DATA: 'recordSchedules/GET_DATA',
  TOGGLE_DRAWER: 'recordSchedules/TOGGLE_DRAWER',
  TRIGGER_RELOAD_BROWSE: 'recordSchedules/TRIGGER_RELOAD_BROWSE',
  DATA_IS_LOADING: 'recordSchedules/DATA_IS_LOADING',
  SELECT_EVENT: 'recordSchedules/SELECT_EVENT',
  GET_CALENDAR_ENTRIES: 'recordSchedules/GET_CALENDAR_ENTRIES',
  RESET_FILTERS: 'recordSchedules/RESET_FILTERS',
  SET_FILTER: 'recordSchedules/SET_FILTER',
}

export default actions
