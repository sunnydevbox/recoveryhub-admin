import actions from './actions'

const initialState = {
  drawerState: false, // true or false for open or close
  reloadListState: false,

  // Calendar Entries
  listLoading: true,
  calendarEntries: {},

  // Selected Event
  loadingSelectedEvent: false,
  selectedEvent: null,

  // Filters`
  filters: {
    status: 'open',
  },
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload }
    default:
      return state
  }
}
