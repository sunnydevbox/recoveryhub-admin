const actions = {
  SET_STATE: 'recordPatients/SET_STATE',
  GET_DATA: 'recordPatients/GET_DATA',
  TOGGLE_DRAWER: 'recordPatients/TOGGLE_DRAWER',
  TRIGGER_RELOAD_BROWSE: 'recordPatients/TRIGGER_RELOAD_BROWSE',
  SET_BROWSE: 'recordPatients/SET_BROWSE',
  GET_BROWSE_FAILED: 'recordPatients/GET_BROWSE_FAILED',
  SET_QUERY_PARAMS: 'recordPatients/SET_QUERY_PARAMS',
  SET_SEARCH_TERM: 'recordPatients/SET_SEARCH_TERM',
}

export default actions
