import { all, takeEvery, put, select, call } from 'redux-saga/effects'
import { message } from 'antd'
import actions from './actions'
import PatientService from '../../../../services/admin/patients/index'

export function* TOGGLE_DRAWER() {
  const { drawerState } = yield select(state => state.recordPatients)

  console.log('drawerState', drawerState)
  yield put({
    type: 'recordPatients/SET_STATE',
    payload: {
      drawerState: !drawerState,
    },
  })
}

export function* TRIGGER_RELOAD_BROWSE(payload) {
  yield put({
    type: 'recordPatients/SET_STATE',
    payload: {
      reloadListState: payload.payload.reloadListState,
    },
  })
}

export function* SET_BROWSE() {
  try {
    const {
      queryParams: { filters, searchTerm, sorter, page },
      queryParams,
    } = yield select(state => state.recordPatients)
    // console.log('qp first', dataSource)

    let f = ''
    Object.keys(filters).forEach(k => {
      f += `${k}:${filters[k]};`
    })
    const qp = {
      filters: f,
    }

    console.log('search', searchTerm)
    if (searchTerm && searchTerm.length) {
      qp.search = searchTerm
    }

    // console.log('queryParams' , queryParams)

    // console.log('page', page)
    if (page) {
      qp.page = page.current
      qp.limit = page.pageSize
    }
    console.log('sort', sorter)
    if (sorter.column) {
      qp.orderBy = sorter.field
      qp.sortBy = sorter.order === 'ascend' ? 'asc' : 'desc'
    }

    yield put({
      type: 'recordPatients/SET_STATE',
      payload: {
        listLoading: true,
      },
    })

    const result = yield call(PatientService.browse, qp)
    // console.log(queryParams);
    const dataSource1 = result.data
    const pagination = {
      total: result.meta.pagination.total,
      current: result.meta.pagination.current_page,
      pageSize: result.meta.pagination.per_page,
    }

    yield put({
      type: 'recordPatients/SET_STATE',
      payload: {
        dataSource: dataSource1,
        pagination,
        queryParams,
        listLoading: false,
        searchLoading: false,
      },
    })
  } catch (e) {
    message.error('There was a problem loading the list!')
    console.log(e)
    yield put({
      type: 'recordPatients/SET_STATE',
      payload: {
        listLoading: false,
      },
    })
  }
}

export function* SET_QUERY_PARAMS(payload) {
  console.log('SET_QUERY_PARAMS', payload)

  yield put({
    type: 'recordPatients/SET_STATE',
    payload: {
      queryParams: payload.payload.queryParams,
    },
  })

  yield put({
    type: actions.SET_BROWSE,
  })
}

export function* SET_SEARCH_TERM(payload) {
  console.log('SEARCH', payload)

  yield put({
    type: 'recordPatients/SET_STATE',
    payload: {
      searchLoading: true,
    },
  })

  yield put({
    type: 'recordPatients/SET_STATE',
    payload: {
      searchTerm: payload.payload.searchTerm,
      page: payload.payload.page,
    },
  })

  yield put({
    type: actions.SET_BROWSE,
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.TOGGLE_DRAWER, TOGGLE_DRAWER),
    takeEvery(actions.TRIGGER_RELOAD_BROWSE, TRIGGER_RELOAD_BROWSE),
    takeEvery(actions.SET_BROWSE, SET_BROWSE),
    takeEvery(actions.SET_QUERY_PARAMS, SET_QUERY_PARAMS),
    takeEvery(actions.SET_SEARCH_TERM, SET_SEARCH_TERM),
  ])
}
