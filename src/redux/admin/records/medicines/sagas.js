import { all, takeEvery, put, select, call } from 'redux-saga/effects'
import { notification } from 'antd'
import actions from './actions'
import MedicineService from '../../../../services/admin/medicines/index'

export function* TOGGLE_DRAWER() {
  const { drawerState } = yield select(state => state.recordMedicine)

  yield put({
    type: 'recordMedicine/SET_STATE',
    payload: {
      drawerState: !drawerState,
    },
  })
}

export function* TRIGGER_RELOAD_BROWSE(payload) {
  yield put({
    type: 'recordMedicine/SET_STATE',
    payload: {
      reloadListState: payload.payload.reloadListState,
    },
  })
}

export function* SET_BROWSE() {
  try {
    const {
      queryParams: { filters, searchTerm, sorter, page },
      queryParams,
    } = yield select(state => state.recordMedicine)

    let f = ''
    Object.keys(filters).forEach(k => {
      f += `${k}:${filters[k]};`
    })
    const qp = {
      filters: f,
    }

    console.log('search', searchTerm)
    if (searchTerm && searchTerm.length) {
      qp.search = searchTerm
    }

    if (page) {
      qp.page = page.current
      qp.limit = page.pageSize
    }
    console.log('sort', sorter)
    if (sorter.column) {
      qp.orderBy = sorter.field
      qp.sortBy = sorter.order === 'ascend' ? 'asc' : 'desc'
    }

    yield put({
      type: 'recordMedicine/SET_STATE',
      payload: {
        listLoading: true,
      },
    })

    const result = yield call(MedicineService.browse, qp)

    const dataSource1 = result.data
    const pagination = {
      total: result.meta.pagination.total,
      current: result.meta.pagination.current_page,
      pageSize: result.meta.pagination.per_page,
    }
    yield put({
      type: 'recordMedicine/SET_STATE',
      payload: {
        dataSource: dataSource1,
        pagination,
        queryParams,
        reloadListState: false,
        listLoading: false,
        searchLoading: false,
      },
    })
  } catch (e) {
    notification.error({
      message: 'Error',
      description: 'There was a problem loading the list!',
    })
    console.log(e)
    yield put({
      type: 'recordMedicine/SET_STATE',
      payload: {
        reloadListState: false,
      },
    })
  }
}

export function* SET_QUERY_PARAMS(payload) {
  console.log('SET_QUERY_PARAMS', payload)

  yield put({
    type: 'recordMedicine/SET_STATE',
    payload: {
      queryParams: payload.payload.queryParams,
    },
  })

  yield put({
    type: actions.SET_BROWSE,
  })
}

export function* SET_SEARCH_TERM(payload) {
  console.log('SEARCH', payload)

  yield put({
    type: 'recordMedicine/SET_STATE',
    payload: {
      searchLoading: true,
    },
  })

  yield put({
    type: 'recordMedicine/SET_STATE',
    payload: {
      searchTerm: payload.payload.searchTerm,
    },
  })

  yield put({
    type: actions.SET_BROWSE,
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.TOGGLE_DRAWER, TOGGLE_DRAWER),
    takeEvery(actions.TRIGGER_RELOAD_BROWSE, TRIGGER_RELOAD_BROWSE),
    takeEvery(actions.SET_BROWSE, SET_BROWSE),
    takeEvery(actions.SET_QUERY_PARAMS, SET_QUERY_PARAMS),
    takeEvery(actions.SET_SEARCH_TERM, SET_SEARCH_TERM),
  ])
}
