const actions = {
  SET_STATE: 'recordMedicine/SET_STATE',
  GET_DATA: 'recordMedicine/GET_DATA',
  TOGGLE_DRAWER: 'recordMedicine/TOGGLE_DRAWER',
  TRIGGER_RELOAD_BROWSE: 'recordMedicine/TRIGGER_RELOAD_BROWSE',
  SET_BROWSE: 'recordMedicine/SET_BROWSE',
  GET_BROWSE_FAILED: 'recordMedicine/GET_BROWSE_FAILED',
  SET_QUERY_PARAMS: 'recordMedicine/SET_QUERY_PARAMS',
  SET_SEARCH_TERM: 'recordMedicine/SET_SEARCH_TERM',
}

export default actions
