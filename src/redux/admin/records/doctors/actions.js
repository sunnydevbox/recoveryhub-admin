const actions = {
  SET_STATE: 'recordDoctors/SET_STATE',
  GET_DATA: 'recordDoctors/GET_DATA',
  TOGGLE_DRAWER: 'recordDoctors/TOGGLE_DRAWER',
  TRIGGER_RELOAD_BROWSE: 'recordDoctors/TRIGGER_RELOAD_BROWSE',
  SET_BROWSE: 'recordDoctors/SET_BROWSE',
  GET_BROWSE_FAILED: 'recordDoctors/GET_BROWSE_FAILED',
  SET_QUERY_PARAMS: 'recordDoctors/SET_QUERY_PARAMS',
  SET_SEARCH_TERM: 'recordDoctors/SET_SEARCH_TERM',
}

export default actions
