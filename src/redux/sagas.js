import { all } from 'redux-saga/effects'
import user from './user/sagas'
import menu from './menu/sagas'
import settings from './settings/sagas'
import couponCodes from './admin/tools/coupon-codes/sagas'
import recordMedicine from './admin/records/medicines/sagas'
import recordGeneric from './admin/records/medicines-generic/sagas'
import recordFeedback from './admin/records/feedbacks/sagas'
import recordPatients from './admin/records/patients/sagas'
import recordDoctors from './admin/records/doctors/sagas'

import featureAdminToolSchedules from '../pages/admin/records/schedules/redux/sagas'
import featureDoctor from '../pages/admin/records/doctors/redux/sagas'
import featurePatient from '../pages/admin/records/_patients/redux/sagas'
import featureCoupon from '../pages/admin/tools/_coupon-codes/redux/sagas'
import featureMedicine from '../pages/admin/records/_medicines/redux/sagas'
import featureMedGeneric from '../pages/admin/records/medicine_generic/redux/sagas'
import featureInsurance from '../pages/admin/records/insurances/redux/sagas'

export default function* rootSaga() {
  yield all([
    user(),
    menu(),
    settings(),
    couponCodes(),
    recordMedicine(),
    recordGeneric(),
    recordFeedback(),
    recordPatients(),
    recordDoctors(),

    featureDoctor(),
    featurePatient(),
    featureCoupon(),
    featureAdminToolSchedules(),
    featureMedicine(),
    featureMedGeneric(),
    featureInsurance(),
  ])
}
