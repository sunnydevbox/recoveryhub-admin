import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import user from './user/reducers'
import menu from './menu/reducers'
import settings from './settings/reducers'

import recordPatients from './admin/records/patients/reducers'
import recordDoctors from './admin/records/doctors/reducers'
import recordMedicine from './admin/records/medicines/reducers'
import recordGeneric from './admin/records/medicines-generic/reducers'
import recordFeedback from './admin/records/feedbacks/reducers'
import couponCodes from './admin/tools/coupon-codes/reducers'

import featureDoctor from '../pages/admin/records/doctors/redux/reducers'
import featurePatient from '../pages/admin/records/_patients/redux/reducers'
import featureCoupon from '../pages/admin/tools/_coupon-codes/redux/reducers'
import featureAdminToolSchedules from '../pages/admin/records/schedules/redux/reducers'
import featureMedicine from '../pages/admin/records/_medicines/redux/reducers'
import featureMedGeneric from '../pages/admin/records/medicine_generic/redux/reducers'
import featureInsurance from '../pages/admin/records/insurances/redux/reducers'

export default history =>
  combineReducers({
    router: connectRouter(history),
    user,
    menu,
    settings,
    couponCodes,
    recordMedicine,
    recordGeneric,
    recordFeedback,
    recordPatients,
    recordDoctors,

    featureDoctor,
    featurePatient,
    featureCoupon,
    featureAdminToolSchedules,
    featureMedicine,
    featureMedGeneric,
    featureInsurance,
  })
